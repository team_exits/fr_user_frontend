// import logo from './logo.svg'
import './Kerala.css'
import React from 'react'

function Kerala() {
  return (
    <div style={{}}>
      <div>
        <div style={{ textAlign: 'center', fontSize: 25 }}>
          HIGH COURT OF KERALA
        </div>
        <div
          style={{ textAlign: 'center', fontSize: 25, marginBottom: '1rem' }}
        >
          CASE STATUS : SEARCH BY CASE NUMBER
        </div>

        <div>
          <table
            className='table table-bordered'
            style={{ border: '1px solid black' }}
          >
            <tbody style={{ textAlign: 'right' }}>
              <th
                colspan='4'
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  fontSize: 15,
                  backgroundColor: '#00b0f0',
                }}
              >
                CASE DETAILS
              </th>
              <tr>
                <th>Case Type</th>
                <td>WP(C)</td>
                <th>Case Status</th>
                <td>DISPOSED</td>
              </tr>
              <tr>
                <th>Filing Date</th>
                <td>WP(C) 140/2020</td>
                <th>Filing Date</th>
                <td>06-01-2020</td>
              </tr>
              <tr>
                <th>Registration Number</th>
                <td>WP(C) 6/2020 (A)</td>
                <th>Registration Date</th>
                <td>06-01-2020</td>
              </tr>
              <tr>
                <th>CNR Number</th>
                <td>KLHC010001732020</td>
                <th>E-File NO</th>
                <td>Offline</td>
              </tr>
              <tr>
                <th>Disposed date</th>
                <td colSpan='3'>09-10-2020</td>
              </tr>
            </tbody>
          </table>
        </div>
        <br />
        <div>
          <table className='table table-bordered'>
            <tbody>
              <th
                colspan='5'
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  fontSize: 15,
                  backgroundColor: '#00b0f0',
                }}
              >
                CASE STATUS
              </th>
              <tr>
                <th>First Hearing Date</th>
                <td colSpan='4'>Monday, the 06th day of January 2020</td>
              </tr>
              <tr>
                <th>Decision Date</th>
                <td colSpan='4'>Friday, the 09th day of October 2020</td>
              </tr>
              <tr>
                <th>Nature of Disposal</th>
                <td colSpan='4'>DISPOSED OF</td>
              </tr>
              <tr>
                <th>Coram</th>
                <td colSpan='4'>
                  4286-HONOURABLE MR. JUSTICE ANIL K.NARENDRAN
                </td>
              </tr>

              <tr>
                <th>Bench</th>
                <td colSpan='4'>Single</td>
              </tr>
              <tr>
                <th>Last listed Details</th>
                <td>Date : 09-10-2020</td>
                <td>Bench: 4286 - HONOURABLE MR. JUSTICE ANIL K.NARENDRAN</td>
                <td>List : Part Two</td>
                <td>Item : 24</td>
              </tr>
            </tbody>
          </table>
          <br />
          <div>
            <table class='table table-striped table-bordered table-hover'>
              <tbody>
                <tr>
                  <td
                    colspan='2'
                    style={{
                      textAlign: 'center',
                      fontWeight: 'bold',
                      background: '#00b0f0',
                    }}
                  >
                    PETITIONER AND ADVOCATE
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td rowspan='2'>
                    Petitioner : ARAVINDAKSHAN PILLAI , Age : 52 Years <br />
                    Petitioner Advocate : SOORANAD S.SREEKUMAR,SMT.JIJI M.
                    VARKEY,SMT.G.SAVITHA,SRI.M.M.SHAJAHAN{' '}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  RESPONDENT AND ADVOCATE
                </th>
                <tr>
                  <td>1</td>
                  <td style={{ fontWeight: 'bold' }}>
                    Respondent : STATE OF KERALA
                  </td>
                </tr>

                <tr>
                  <td>2</td>
                  <td>THE DISTRICT SUPERINTENDENT OF POLICE</td>
                </tr>

                <tr>
                  <td>3</td>
                  <td>THE CIRCLE INSPECTOR OF POLICE</td>
                </tr>

                <tr>
                  <td>4</td>
                  <td>
                    RAJU K. (55 Years) Advocate : SRI.MANU RAMACHANDRAN (18244)
                    Extra Advocate : SRI.M.KIRANLAL (16623) , SRI.T.S.SARATH
                    (24641) , SRI.R.RAJESH (VARKALA) (21132) , SHRI.SAMEER M
                    NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>5</td>
                  <td colSpan='2'>
                    SHAJI S. (54 Years) Advocate : SRI.MANU RAMACHANDRAN (18244)
                    Extra Advocate : SRI.M.KIRANLAL (16623) , SRI.T.S.SARATH
                    (24641) , SRI.R.RAJESH (VARKALA) (21132) , SHRI.SAMEER M
                    NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>6</td>
                  <td colSpan='2'>
                    VIJAYAN P. (57 Years) Advocate : SRI.MANU RAMACHANDRAN
                    (18244) Extra Advocate : SRI.M.KIRANLAL (16623) ,
                    SRI.T.S.SARATH (24641) , SRI.R.RAJESH (VARKALA) (21132) ,
                    SHRI.SAMEER M NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>7</td>
                  <td colSpan='2'>
                    ADDL.R7.THE CHIEF EXECUTIVE OFFICER Advocate : SRI.SIJU K.,
                    SC, KHWWB (7438)
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  RESPONDENT AND ADVOCATE
                </th>
                <tr>
                  <td>1</td>
                  <td style={{ fontWeight: 'bold' }}>
                    Respondent : STATE OF KERALA
                  </td>
                </tr>

                <tr>
                  <td>2</td>
                  <td>THE DISTRICT SUPERINTENDENT OF POLICE</td>
                </tr>

                <tr>
                  <td>3</td>
                  <td>THE CIRCLE INSPECTOR OF POLICE</td>
                </tr>

                <tr>
                  <td>4</td>
                  <td>
                    RAJU K. (55 Years) Advocate : SRI.MANU RAMACHANDRAN (18244)
                    Extra Advocate : SRI.M.KIRANLAL (16623) , SRI.T.S.SARATH
                    (24641) , SRI.R.RAJESH (VARKALA) (21132) , SHRI.SAMEER M
                    NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>5</td>
                  <td colSpan='2'>
                    SHAJI S. (54 Years) Advocate : SRI.MANU RAMACHANDRAN (18244)
                    Extra Advocate : SRI.M.KIRANLAL (16623) , SRI.T.S.SARATH
                    (24641) , SRI.R.RAJESH (VARKALA) (21132) , SHRI.SAMEER M
                    NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>6</td>
                  <td colSpan='2'>
                    VIJAYAN P. (57 Years) Advocate : SRI.MANU RAMACHANDRAN
                    (18244) Extra Advocate : SRI.M.KIRANLAL (16623) ,
                    SRI.T.S.SARATH (24641) , SRI.R.RAJESH (VARKALA) (21132) ,
                    SHRI.SAMEER M NAIR (27671)
                  </td>
                </tr>

                <tr>
                  <td>7</td>
                  <td colSpan='2'>
                    ADDL.R7.THE CHIEF EXECUTIVE OFFICER Advocate : SRI.SIJU K.,
                    SC, KHWWB (7438)
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  SERVED ON
                </th>
              </tbody>
              <td>GOVERNMENT PLEADER- SERVED ON</td>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  ACTS
                </th>
              </tbody>
              <tr>
                <td>Under Act(s)</td>
                <td>Under Section(s)</td>
              </tr>
              <tr>
                <td>CONSTITUTION OF INDIA</td>
                <td>226</td>
              </tr>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='7'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  IA DETAILS
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <th>#</th>
                  <th>IA Number</th>
                  <th>Date of Filing</th>
                  <th>Status</th>
                  <th>Classification</th>
                  <th>Party</th>
                  <th>Date of Disposal</th>
                </tr>
                <td>1</td>
                <td>IA/1/2020</td>
                <td>13-01-2020</td>
                <td>PENDING</td>
                <td>
                  IMPLEADMENT/DELETION/SUBSTITUTION/TRANSPOSITION OF
                  PARTIES/LRS/REMOVING
                </td>
                <td>ARAVINDAKSHAN PILLAI</td>
                <td></td>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='7'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  DOCUMENTS
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <th>Document No</th>
                  <th>Date</th>
                  <th>File Type</th>
                  <th>Description </th>
                  <th>Party Name</th>
                  <th>Advocate Name</th>
                  <th>Files</th>
                </tr>
                <td>1/2020</td>
                <td>30-01-2020</td>
                <td>STATEMENT</td>
                <td></td>
                <td>ADDL.R7.THE CHIEF EXECUTIVE OFFICER</td>
                <td>SRI.SIJU K., SC, KHWWB</td>
                <td></td>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='7'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  HISTORY OF CASE HEARING{' '}
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <th>#</th>
                  <th>Cause List Type</th>
                  <th>Hon: Judge Name</th>
                  <th>BusinessDate</th>
                  <th>NextDate</th>
                  <th>Purpose of Hearing</th>
                  <th>Order</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Today List</td>
                  <td>4289-HONOURABLE MR. JUSTICE A.MUHAMED MUSTAQUE</td>
                  <td>06-01-2020</td>
                  <td>13-01-2020</td>
                  <td>ADMISSION</td>
                  <td>
                    PETITIONER TO IMPLEAD KHLWWB NOTICE BY SPL MESSENGER POST ON
                    13/01/20 - Issue Notice
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Separate List 1</td>
                  <td>4289-HONOURABLE MR. JUSTICE A.MUHAMED MUSTAQUE</td>
                  <td>13-01-2020</td>
                  <td>17-01-2020</td>
                  <td>PETITIONS</td>
                  <td>POST ON 17/01/2020</td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='4'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  INTERIM ORDERS
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <td>Business Date</td>
                  <td>Hon: Judge Name</td>
                  <td>Application</td>
                  <td>Order</td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='4'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  ORDER
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <td>Order Number</td>
                  <td>Judge</td>
                  <td>Order Date</td>
                  <td>View</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>4286-HONOURABLE MR. JUSTICE ANIL K.NARENDRAN</td>
                  <td>09-10-2020</td>
                  <td>View</td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  CATEGORY DETAILS
                </th>
              </tbody>
              <td>Category : 115.00 Police Protection (289)</td>
              <td>
                Sub Category : 28.600 POLICE PROTECTION - MISCELLANEOUS (582)
              </td>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  OBJECTION
                </th>
              </tbody>
              <tr>
                <td>#</td>
                <td>Objection</td>
              </tr>
              <tr>
                <td>1</td>
                <td>All Objections are Complied</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Kerala
