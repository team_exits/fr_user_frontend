import './Allahabad.css'
import React from 'react'

function Allahabad() {
  return (
    <div style={{}}>
      <div>
        <div style={{ textAlign: 'center', fontSize: 25 }}>
          HIGH COURT OF ALLAHABAD
        </div>
        <div
          style={{ textAlign: 'center', fontSize: 25, marginBottom: '1rem' }}
        >
          CASE STATUS : SEARCH BY CASE NUMBER
        </div>

        <div>
          <table
            className='table table-bordered'
            style={{ border: '1px solid black' }}
          >
            <tbody style={{ textAlign: 'right' }}>
              <th
                colspan='4'
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  fontSize: 15,
                  backgroundColor: '#00b0f0',
                }}
              >
                CASE DETAILS
              </th>
              <tr>
                <th>Filing No</th>
                <td>WPIL/2766/2020</td>
                <th>Filing Date</th>
                <td>10-01-2020</td>
              </tr>
              <tr>
                <th>Registration Number</th>
                <td>WPIL 52/2020 (A)</td>
                <th>Date of Registration </th>
                <td>13-01-2020</td>
              </tr>
              <tr>
                <th>CNR Number</th>
                <td>UPHC010076692020</td>
              </tr>
            </tbody>
          </table>
        </div>
        <br />
        <div>
          <table className='table table-bordered'>
            <tbody>
              <th
                colspan='5'
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  fontSize: 15,
                  backgroundColor: '#00b0f0',
                }}
              >
                CASE STATUS
              </th>
              <tr>
                <th>First Hearing Date</th>
                <td colSpan='4'>14th January 2020</td>
              </tr>
              <tr>
                <th>Date of Decision </th>
                <td colSpan='4'>14th January 2020</td>
              </tr>
              <tr>
                <th>Case Status</th>
                <td colSpan='4'>Case Disposed</td>
              </tr>
              <tr>
                <th>Nature of Disposal</th>
                <td colSpan='4'>Disposed Off/Decided on merits</td>
              </tr>
              <tr>
                <th>Stage of Case</th>
                <td colSpan='4'>Fresh</td>
              </tr>
              <tr>
                <th>Coram( Hon'ble Mr./Ms./Dr. Justice )</th>
                <td colSpan='4'>ANJANI KUMAR MISHRA ( 5115 )</td>
              </tr>

              <tr>
                <th>Bench Type</th>
                <td colSpan='4'>Single Bench</td>
              </tr>
              <tr>
                <th>Judicial Branch</th>
                <td colSpan='4'>WRITS Civil</td>
              </tr>
              <tr>
                <th>Causelist Type</th>
                <td colSpan='4'>---</td>
              </tr>
              <tr>
                <th>State</th>
                <td colSpan='4'>UTTARPRADESH</td>
              </tr>
              <tr>
                <th>District</th>
                <td colSpan='4'>MAHARAJGANJ</td>
              </tr>
            </tbody>
          </table>
          <br />
          <div>
            <table class='table table-striped table-bordered table-hover'>
              <tbody>
                <tr>
                  <td
                    colspan='2'
                    style={{
                      textAlign: 'center',
                      fontWeight: 'bold',
                      background: '#00b0f0',
                    }}
                  >
                    Petitioner /Respondent and their Advocate(s)
                  </td>
                </tr>
                <tr>
                  <th>Petitioner</th>
                  <th>Respondent</th>
                </tr>
                <tr>
                  <th>
                    Petitioner : AJAY YADAV
                    <br />
                    Advocate : PRAMOD KUMAR MAURYA
                  </th>
                  <td>
                    State of U.P. AND 5 OTHERS <br />
                    Advocate - C.S.C.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table className='table table-bordered'>
              <tbody>
                <th
                  colspan='2'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  Category Details
                </th>
                <tr>
                  <th>Category</th>
                  <td colSpan='4'>THE U.P.Z.A. & L.R.ACT 1950(110100)</td>
                </tr>

                <tr>
                  <th>Sub Category</th>
                  <td colSpan='4'>PIL relating to Gaon Sabha land ( 64 )</td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='7'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  IA DETAILS
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <th>Application Number</th>
                  <th>Party</th>
                  <th>Date of Filing</th>
                  <th>Next/Date of Disposal</th>
                  <th>IA Status</th>
                </tr>
                <td>
                  IA/1/2020 Classification : Misc. Application Bench : 5115
                </td>

                <td>AJAY YADAV Vs State of U.P. AND 5 OTHERS</td>
                <td>13-01-2020</td>
                <td></td>
                <td>Pending</td>
              </tbody>
            </table>
          </div>
          <br />
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody style={{ textAlign: 'right' }}>
                <th
                  colspan='7'
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 15,
                    backgroundColor: '#00b0f0',
                  }}
                >
                  Last Listing Detail{' '}
                </th>
                <tr style={{ textAlign: 'left' }}>
                  <th>Cause List Type</th>
                  <th>Hon'ble Mr./Ms./Dr. Justice</th>
                  <th>Last listing Date</th>
                  <th>Stage of Listing</th>
                  <th>Last Short Order</th>
                </tr>
                <tr>
                  <td>Fresh List</td>
                  <td>B.K. NARAYANA , SHAMIM AHMED ( Bench: 5920 )</td>
                  <td>14-01-2020</td>
                  <td>Fresh</td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
        </div>
      </div>
    </div>
  )
}

export default Allahabad
