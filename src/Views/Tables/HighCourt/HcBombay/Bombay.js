// import logo from './logo.svg'
import './Bombay.css'
import React from 'react'

function Bombay() {
  return (
    <>
      <p style={{ textAlign: 'center', fontSize: 25 }}>Case Details</p>

      <div>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Details
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Bench :- Bombay
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Presentation Date :- 10/01/2020
              </th>
            </tr>
            <tr>
              <th>Stamp No. :-</th>
              <td colSpan='3'>APEALST/57/2020</td>
            </tr>
            <tr>
              <th>Filing Date</th>
              <td> 10/01/2020</td>
            </tr>
            <tr>
              <th>Reg. No. :-</th>
              <td> APEAL/57/2020</td>
            </tr>
            <tr>
              <th>Reg. Date :-</th>
              <td> 10/01/2020</td>
            </tr>
            <tr>
              <th>Petitioner :-</th>

              <select>
                <option>AVINASH KRISHNADEO GAIKWAD</option>
              </select>
            </tr>
            <tr>
              <th>Respondent :-</th>

              <select>
                <option>THE STATE OF MAHARASHTRA-</option>
                <option>THE STATE OF MAHARASHTRA-</option>
              </select>
            </tr>
            <tr>
              <th>Petn.Adv:-</th>

              <select>
                <option>NIKHIL KUNAL CHAUDHARI (5880)</option>
              </select>
            </tr>
          </tbody>
        </table>
      </div>
      <div>
        <table className='table table-bordered'>
          <tbody>
            <tr>
              <th>District :-</th>
              <td colSpan='3'>MUMBAI</td>
              <th>Bench :-</th>
              <td colSpan='3'>SINGLE</td>
            </tr>
            <tr>
              <th>Status :-</th>
              <td colSpan='5'> Admitted(Unready)</td>
            </tr>
            <tr>
              <th>Category :-</th>
              <td colSpan='5'>
                APPEAL-By Persons Convicted(Against Conviction) on Bail
              </td>
            </tr>
            <tr>
              <th>Last Date:-</th>
              <td>21/01/2020</td>
              <th>Stage :-</th>
              <td colSpan='5'>
                FOR ADMISSION - CIRCULATION (CRIMINAL SIDE MATTERS){' '}
              </td>
            </tr>
            <tr>
              <th>Last Coram :-</th>
              <td colSpan='5'> HON'BLE SHRI JUSTICE P. K. CHAVAN</td>
            </tr>
            <tr>
              <th>Act :-</th>
              <td colSpan='5'>Indian Penal Code (I.P.C)</td>
            </tr>
            <tr>
              <th>Under Section :-</th>
              <td colSpan='5'> 306, 376</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody>
            <tr>
              <td colSpan='3'>Office Info.</td>
              <td colSpan='3'>Misc Info.</td>
              <td colSpan='3'>Objections.</td>
              <td colSpan='3'>Paper Index</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td colSpan='3'>Conn Matters.</td>
              <td colSpan='3'>Appl Cases.</td>
              <td colSpan='3'>Listing Dates / Orders.</td>
              <td colSpan='3'>Lower Court Details </td>
            </tr>
          </tbody>
        </table>
      </div>
      {/* *************************Office */}
      {/* information************************************* */}
      <div>
        <p style={{ textAlign: 'center', fontSize: 25 }}>Office Information</p>

        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Office Information
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Stamp No. :- APEAL/57/2020 & Reg. No. :- APEAL/57/2020
              </th>
            </tr>
            <tr>
              <th>Description</th>
              <th>Date</th>
              <th>Remark</th>
            </tr>
            <td>Date of Presentation</td>
            <td>10/01/2020</td>
            <td></td>

            <td>Matter admitted on</td>
            <td>21/01/2020</td>
            <td></td>
          </tbody>
        </table>
      </div>

      {/* *************************Misc */}
      {/* information************************************* */}

      <div>
        <p>Miss info </p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Misc Information
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Subject Category
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Sub. Cat. :- ORDINARY CIVIL - ORDINARY CIVIL
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                VP Entires Not Found
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Affidavit Entires Not Found
              </th>
            </tr>
          </tbody>
        </table>
      </div>

      {/* *************************Objections ************************************* */}

      <div>
        <p>Objections </p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Objections
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                CASE Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <td> OBJECTIONS</td>
              <td> RAISED ON</td>
              <td> REMOVED ON</td>
            </tr>
          </tbody>
        </table>
      </div>

      {/* *************************Case Paper Index ************************************* */}

      <div>
        <p>Case Paper Index</p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Paper Index
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                CASE Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <td> Document</td>
              <td> Filing Date</td>
              <td> Start page</td>
              <td> End Page</td>
              <td> No of Pages</td>
            </tr>
          </tbody>
        </table>
      </div>

      {/* *************************Connected  ************************************* */}

      <div>
        <p>Connected Matters</p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Connected Matters
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
          </tbody>
        </table>
      </div>

      {/* *************************Application Cases ************************************* */}

      <div>
        <p>Application Cases</p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Application Cases
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Case Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <td> Stamp No.</td>
              <td> Reg. No.</td>
              <td> Category</td>
              <td> Status</td>
            </tr>
            <tr>
              <td> CAA/30880/2018</td>
              <td> CAA/7/2019</td>
              <td> INJUCTION</td>
              <td> </td>
            </tr>
          </tbody>
        </table>
      </div>

      {/* *************************Listing Dates ************************************* */}

      <div>
        <p>Listing Dates</p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Listing Dates
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                CASE Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <th> Date</th>
              <th> Coram</th>
              <th> Action</th>
              <th> Order/Judgement </th>
            </tr>
            <tr>
              <td> 26/11/2018 (J-R)</td>
              <td> HON'BLE SHRI JUSTICE A.S. CHANDURKAR</td>
              <td> Disposed</td>
              <td> (J-R)Order/Judg-0</td>
            </tr>
            <tr>
              <th colSpan='6' style={{ textAlign: 'center' }}>
                CMIS GIVEN DATES
              </th>
            </tr>
            <tr>
              <th style={{ textAlign: 'center' }}> Sr. No</th>
              <th colSpan='3' style={{ textAlign: 'center' }}>
                CMIS Date
              </th>
            </tr>
            <tr>
              <td style={{ textAlign: 'center' }}> 1</td>
              <td colSpan='3' style={{ textAlign: 'center' }}>
                26/11/2018*
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      {/* **********************Lower Court*************** */}

      <div>
        <p>Lower Court </p>
        <table
          className='table table-bordered'
          style={{ border: '1px solid black' }}
        >
          <tbody style={{ textAlign: 'right' }}>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Lower Court Details
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                CASE Stamp No. :- AO/30878/2018 & Reg. No. :- AO/5/2019
              </th>
            </tr>
            <tr>
              <th colSpan='5' style={{ textAlign: 'center' }}>
                Previous Case Details
              </th>
            </tr>
            <tr>
              <th>Court :-</th>
              <td colSpan='3'>City Civil Court</td>
            </tr>
            <tr>
              <th>District :-</th>
              <td> MUMBAI</td>
            </tr>
            <tr>
              <th>Case No. :-</th>
              <td> SHORT CAUSE SUIT/02151/2015</td>
            </tr>
            <tr>
              <th>Judge Designation :-</th>
              <td> City Civil Court Judge</td>
            </tr>
            <tr>
              <th>Decision Date :-</th>
              <td>09/10/2018</td>
            </tr>
            <tr>
              <th>Judgement Lang. :-</th>
              <td> English</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}

export default Bombay

//  .table, tr,th{
//     border: 1px solid black;
//   }
