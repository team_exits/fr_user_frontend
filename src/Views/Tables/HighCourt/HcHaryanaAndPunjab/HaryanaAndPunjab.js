import React from 'react'
// import ReactTable from "react-table";

const HaryanaAndPunjab = () => {
  return (
    <div
      className='firstDiv'
      style={{ display: 'grid', justifyContent: 'center' }}
    >
      <table className='table table-sm table-bordered'>
        <thead>
          <tr>
            <th colspan='4' style={{ textAlign: 'center' }}>
              State - Punjab and Haryana <br />
              Case Details For Case CRR-2-2020
            </th>
          </tr>
        </thead>

        <thead style={{ backgroundColor: '#e4f0f5' }}>
          <tr>
            <td scope=''>Diary Number</td>
            <td>3343481</td>
            <td scope=''>District</td>
            <td>KARNAL</td>
          </tr>
        </thead>
        <tbody>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>Category</td>
            <td>37.5-CRIMINAL REVISION (AGAINST ACQUITTAL)</td>
            <td scope='row'>Main Case Detail</td>
            <td>---</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>Party Detail</td>
            <td colspan='3'>JEET RAM V/S STATE OF HARYANA AND ANOTHER</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='col'>Advocate Name</td>
            <td>GAURAV GARG (P-1827-2008)</td>
            <td scope='col'> List Type</td>
            <td>ORDINARY</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='col'>Status</td>
            <td>PENDING</td>
            <td scope='col'>Next date</td>
            <td>05-JAN-2022</td>
          </tr>
        </tbody>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr>
            <th colspan='4' style={{ textAlign: 'center' }}>
              Case Listing Details
            </th>
          </tr>
        </thead>
        <tbody>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>Cause List Date</td>
            <td scope='row'>List Type:Sr. No.</td>
            <td scope='row'>Bench</td>
            <td scope='row'>Order Link</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>03-FEB-2020</td>
            <td scope='row'>ORDINARY:249</td>
            <td scope='row'>HON'BLE MR. JUSTICE LALIT BATRA</td>
            <td scope='row'>---</td>
          </tr>
        </tbody>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr>
            <th colspan='4' style={{ textAlign: 'center' }}>
              FIR Details
            </th>
          </tr>
        </thead>
        <tbody>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>FIR No/FIR Date</td>
            <td scope='row'>Police Station</td>
            <td scope='row'>Sections</td>
            <td scope='row'>Sections</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>110/14-MAY-13</td>
            <td scope='row'>P.S. Taraori</td>
            <td scope='row'>323,324,506 OF IPC</td>
            <td scope='row'>KARNAL</td>
          </tr>
        </tbody>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr>
            <th colspan='4' style={{ textAlign: 'center' }}>
              Lower Court Detail
            </th>
          </tr>
        </thead>
        <tbody>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>LC Case No & Order Date</td>
            <td scope='row'>Court Type</td>
            <td scope='row'>LC Judge</td>
            <td scope='row'>District</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>CASE NO-0000124-2013/03-DEC-15</td>
            <td scope='row'>Trial</td>
            <td scope='row'>PARVEEN JMIC KARNAL</td>
            <td scope='row'>KARNAL</td>
          </tr>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='row'>CRA-0000172-2017/30-AUG-19</td>
            <td scope='row'>Apellate</td>
            <td scope='row'>R K MEHTA ASJ KARNAL</td>
            <td scope='row'>KARNAL</td>
          </tr>
        </tbody>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr>
            <th colspan='4' style={{ textAlign: 'center' }}>
              Judgement Details For Case: CRR-2-2020
              <br />
              Party Detail: JEET RAM V/S STATE OF HARYANA AND ANOTHER
            </th>
          </tr>
        </thead>
        <tbody>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <td scope='col'>Order Date</td>
            <td scope='col'>Order Type</td>
            <td scope='col'>Bench</td>
            <td scope='col'>Judgment Link</td>
          </tr>
        </tbody>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <th colspan='4' style={{ textAlign: 'center' }}>
              No Judgment/Order Found.
            </th>
          </tr>
        </thead>
      </table>

      <table className='table table-sm table-bordered'>
        <thead>
          <tr style={{ backgroundColor: '#e4f0f5' }}>
            <th colspan='4' style={{ textAlign: 'center' }}>
              Designed and Developed by National Informatics Centre
              <br />
              Contents Published and Managed by Punjab & Haryana High Court ,
              Chandigarh.
            </th>
          </tr>
        </thead>
      </table>
    </div>
  )
}

export default HaryanaAndPunjab
