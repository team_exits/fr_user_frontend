import logo from './logo.svg'
import './Karnataka.css'
import { Table } from 'react-bootstrap'

function Karnataka() {
  return (
    <>
      <div
        classNAme='firstDiv'
        style={{ display: 'grid', justifyContent: 'center' }}
      >
        <Table>
          <tbody>
            <tr>
              <td
                colSpan='6'
                style={{
                  backgroundColor: '#00b0f0',
                  textAlign: 'center',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}
              >
                Case Details:
              </td>
            </tr>
            <tr>
              <td>Case Number:</td>
              <td> WP 2/2020 (KAHC010531032019)</td>
              <td>Classification:</td>
              <td>GM KLA</td>
              <td>Date of Filing:</td>
              <td>04/12/2019 11:39:58</td>
            </tr>
            <tr>
              <td>Petitioner:</td>
              <td>RAJASHEKAR BHARATHNUR</td>
              <td>Pet. Advocate:</td>
              <td>KUMARA K G</td>
              <td colSpan='2'></td>
            </tr>
            <tr>
              <td>Respondent:</td>
              <td>STATE OF KARNATAKA</td>
              <td>Resp. Advocate:</td>
              <td></td>
              <td colSpan='2'></td>
            </tr>
            <tr>
              <td>Connected To:</td>
              <td>ARAVIND KUMAR AND E.S.INDIRESH</td>
              <td colSpan='4'></td>
            </tr>
            <tr>
              <td>Last Posted For:</td>
              <td>ORDERS</td>
              <td>Last Date of Action:</td>
              <td>28/01/2020</td>
              <td>Last Action Taken:</td>
              <td>DISPOSED</td>
            </tr>
            <tr>
              <td>Next Hearing Date:</td>
              <td>28/01/2020</td>
              <td colSpan='4'></td>
            </tr>

            <tr>
              <td
                colSpan='6'
                style={{
                  backgroundColor: '#00b0f0',
                  textAlign: 'center',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}
              >
                Trial/Appellate Court Details:
              </td>
            </tr>

            <tr>
              <th colSpan='2'>Case Number</th>
              <th colSpan='2'>Trial/Appellate Court</th>
              <th colSpan='2'>Trial/Appellate Date</th>
            </tr>

            <tr>
              <td colSpan='2'>UPLOK-1/256/2017 0/-</td>
              <td colSpan='2'>Quasi Judicial( )</td>
              <td colSpan='2'></td>
            </tr>

            <tr>
              <td
                colSpan='6'
                style={{
                  backgroundColor: '#00b0f0',
                  textAlign: 'center',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}
              >
                Daily Orders:
              </td>
            </tr>

            <tr>
              <th colSpan='2'>SL.No</th>
              <th colSpan='2'>Hon`ble Judge</th>
              <th colSpan='2'>Date of Order</th>
            </tr>

            <tr>
              <td colSpan='2'>1</td>
              <td colSpan='2'>ARAVIND KUMAR AND E.S.INDIRESH</td>
              <td colSpan='2'>21/01/2020</td>
            </tr>

            <tr>
              <td colSpan='2'>2</td>
              <td colSpan='2'>ARAVIND KUMAR AND N S SANJAY GOWDA</td>
              <td colSpan='2'>07/01/2020</td>
            </tr>
          </tbody>
        </Table>
      </div>
    </>
  )
}

export default Karnataka
