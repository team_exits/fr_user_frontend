import './HcCaseDetails.css'
import Layout from '../../../Component/Layout/Layout'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'

export default function HcCaseDetails() {
  let { qid } = useParams()

  const [caseData, setCaseData] = useState([])

  const fetchData = () => {
    return axios
      .get(`http://localhost:5008/captcha/${qid}`)
      .then((res) => {
        const results = res.data
        console.log('Results : ', results)
        return results
      })
      .catch((err) => {
        console.log(err)
      })
  }
  fetchData().then((apiCase) => {
    setCaseData(apiCase)
  })
  return (
    <>
      {/* {caseData.map((d, i) => (
                <div key={i}>{d.case_no}</div>
              ))} */}
      <Layout>
        <div className='container' style={{ backgroundColor: '#fff' }}>
          <p>
            Case Status - WRIT - PUBLIC INTEREST LITIGATION ( WPIL ) - [ 33/2021
            ]
          </p>
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <td>
                    File Received for Reporting with No. 194 on 23.11.2020
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody>
                <tr>
                  <th> Filing No.</th>
                  <td> WPIL/157081/2020</td>
                  <th>Filing Date : 08-12-2020</th>
                </tr>
                <tr>
                  <th> CNR</th>
                  <td>UPHC011635682020</td>
                  <td> Date of Registration : 11-01-2021</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>Case Status</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody>
                <tr>
                  <th>First Hearing Date</th>
                  <td></td>
                </tr>
                <tr>
                  <th>Date of Decision</th>
                  <td>13th January 2021</td>
                </tr>
                <tr>
                  <th> Case Status</th>
                  <td> Case Disposed</td>
                </tr>
                <tr>
                  <th> Nature of Disposal</th>
                  <td>Dismissed on merits</td>
                </tr>
                <tr>
                  <th> Stage of Case</th>
                  <td>Fresh</td>
                </tr>
                <tr>
                  <th> Coram ( Hon'ble Mr./Ms./Dr. Justice )</th>
                  <td>GOVIND MATHUR , SAURABH SHYAM SHAMSHERY ( 5701 )</td>
                </tr>
                <tr>
                  <th>Bench Type</th>
                  <td>Division Bench</td>
                </tr>
                <tr>
                  <th>Judicial Branch</th>
                  <td>WRITS Civil</td>
                </tr>
                <tr>
                  <th>Causelist Type</th>
                  <td> ---</td>
                </tr>
                <tr>
                  <th>State</th>
                  <td>UTTARPRADESH</td>
                </tr>
                <tr>
                  <th>District</th>
                  <td>ALLAHABAD</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>Petitioner/Respondent and their Advocate(s)</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <th>Petitioner</th>
                  <th>Respondent</th>
                </tr>
                <tr>
                  <td>
                    BASANT LAL TIVARI Advocate - JAGADEESH NARAYAN DWIVEDI
                  </td>
                  <td>STATE OF U P AND 7 OTHERS Advocate - C.S.C. ,</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>Category Details</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <th>Category</th>
                  <td>PUBLIC INTEREST LITIGATION ( 370000 )</td>
                </tr>
                <tr>
                  <th>Sub Category</th>
                  <td>STATE OF U P AND 7 OTHERS Advocate - C.S.C. ,</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>Category Details</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <th>Category</th>
                  <td>PUBLIC INTEREST LITIGATION ( 370000 )</td>
                </tr>
                <tr>
                  <th>Sub Category</th>
                  <td>STATE OF U P AND 7 OTHERS Advocate - C.S.C. ,</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>IA Details</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <th>Application(s) Number</th>
                  <th>Party</th>
                  <th>Date of Filing </th>
                  <th>Next / Disposal Date</th>
                  <th>IA Status</th>
                </tr>
                <tr>
                  <td>IA/1/2021 Classification : Stay Application</td>
                  <td>BASANT LAL TIVARI Vs STATE OF U P AND 7 OTHERS</td>
                  <td> 07-01-2021</td>
                  <td></td>
                  <td>Pending</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <p style={{}}>Last Listing Detail</p>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <th>Cause List Type</th>
                  <th>Hon'ble Mr./Ms./Dr. Justice</th>
                  <th>Last Listing Date</th>
                  <th>Stage of Listing</th>
                  <th>Last Short Order</th>
                </tr>
                <tr>
                  <td>Fresh List</td>
                  <td>
                    {' '}
                    GOVIND MATHUR , SAURABH SHYAM SHAMSHERY ( Bench: 5701 )
                  </td>
                  <td> 13-01-2021</td>
                  <td> Fresh</td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>

          {/* <div>
        <p style={{}}>Case History</p>
        <table className="table table-bordered" style={{border: "1px solid black"}}>
                  <thead>
                    <tr>
                      <th>Registration Number</th>
                      <th>Judge</th>
                      <th>Business on Date</th>
                      <th>Hearing Date</th>
                      <th>Purpose of Hearing</th>

                    </tr>
                  </thead>
                  <tbod>
                    <tr>
                      <td>576/2020</td>
                      <td>LD JUDGE 4th BENCH</td>
                      <td>10-08-2020</td>
                      <td>10-09-2020</td>
                      <td>Requisites</td>
                    </tr>
                 

                 
                    <tr>
                      <td>576/2020</td>
                      <td>LD JUDGE 4th BENCH</td>
                      <td>10-08-2020</td>
                      <td>10-09-2020</td>
                      <td>Requisites</td>
                    </tr>
               

                 
                    <tr>
                      <td>576/2020</td>
                      <td>LD JUDGE 4th BENCH</td>
                      <td>10-08-2020</td>
                      <td>10-09-2020</td>
                      <td>Requisites</td>
                    </tr>
                  </tbody>

                </table>
        </div> */}
        </div>
      </Layout>
    </>
  )
}
