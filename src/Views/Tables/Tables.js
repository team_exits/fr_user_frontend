import './Tables.css'
import Layout from '../../Component/Layout/Layout'
import React, { useEffect, useState } from 'react'
import { useParams, useLocation } from 'react-router-dom'
import axios from 'axios'
import Config from '../../Utils/Axios/Config'

export default function Tables() {
  const location = useLocation()
  const datazz = location.state
  console.log('datazz hai >> ', datazz)
  let { qid } = useParams()

  const [caseData, setCaseData] = useState([])
  const [orderData, setOrderData] = useState([])
  const [hearingData, setHearingData] = useState([])
  const [showDetailCaseHis, setShowDetailCaseHis] = useState(false)
  const [showTable, setShowTable] = useState(false)
  //showDetailCaseHis

  useEffect(async () => {
    if (
      datazz != undefined &&
      datazz.scraperResponse.reqBody.reqType == 'BEGIN_ECOURT_SEARCH'
    ) {
      await axios({
        url: `http://${Config.IP}:${Config.UNIFIED}/search/${qid}`,
        method: 'POST',
        data: datazz,
      })
        .then(async (response) => {
          console.log(`response.data.success>>>  `, response.data.success)
          setShowTable(response.data.success)
          setCaseData(response.data.doc.caseResult)
          setOrderData(response.data.doc.orderResult)
          setHearingData(response.data.doc.hearingResult)
        })
        .catch((err) => {
          console.error('err >>> ', err)
        })
    } else {
      await axios({
        url: `http://${Config.IP}:${Config.UNIFIED}/search/${qid}`,
        method: 'GET',
        data: datazz,
      })
        .then(async (response) => {
          console.log(`response.data.success>>>  `, response.data.success)
          setShowTable(response.data.success)
          setCaseData(response.data.doc.caseResult)
          setOrderData(response.data.doc.orderResult)
          setHearingData(response.data.doc.hearingResult)
        })
        .catch((err) => {
          console.error('err >>> ', err)
        })
    }
  }, [datazz])

  console.log('caseData >>> ', caseData)
  console.log('orderData >>> ', orderData)
  console.log('hearingData >>> ', hearingData)

  return (
    <>
      <Layout>
        <div className='container' style={{ backgroundColor: '#fff' }}>
          {showTable ? (
            <>
              {typeof caseData[0] != 'undefined' &&
                caseData[0] != null &&
                caseData.length > 0 &&
                caseData[0].scrapeIds.length > 0 &&
                caseData.map((d, idx) => {
                  // console.log('d >> ', d.case_details.registrationNumber)
                  return (
                    <>
                      <div>
                        <p>Case Details</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <tbody>
                            <tr>
                              <th>Case Type</th>
                              <td colSpan='3'>
                                {typeof d.case_details.caseType !=
                                  'undefined' &&
                                d.case_details.caseType.length > 0
                                  ? d.case_details.caseType
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th> Filing Number</th>
                              <td>
                                {typeof d.case_details.filingNumber !=
                                  'undefined' &&
                                d.case_details.filingNumber.length > 0
                                  ? d.case_details.filingNumber
                                  : 'null'}
                              </td>
                              <th>Filing Date</th>
                              <td>
                                {typeof d.case_details.filingDate !=
                                  'undefined' &&
                                d.case_details.filingDate.length > 0
                                  ? d.case_details.filingDate
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th> Registration Number</th>
                              <td>
                                {typeof d.case_details.registrationNumber !=
                                  'undefined' &&
                                d.case_details.registrationNumber.length > 0
                                  ? d.case_details.registrationNumber
                                  : 'null'}
                              </td>
                              <th>Registration Date</th>
                              <td>
                                {typeof d.case_details.registrationDate !=
                                  'undefined' &&
                                d.case_details.registrationDate.length > 0
                                  ? d.case_details.registrationDate
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th> CNR Number</th>
                              <td colSpan='3'>
                                {typeof d.case_details.cnrNumber !=
                                  'undefined' &&
                                d.case_details.cnrNumber.length > 0
                                  ? d.case_details.cnrNumber
                                  : 'null'}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div>
                        <p>Case Status</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <tbody>
                            <tr>
                              <th>First Hearing Date</th>
                              <td>
                                {typeof d.case_status.firstHearingDate !=
                                  'undefined' &&
                                d.case_status.firstHearingDate.length > 0
                                  ? d.case_status.firstHearingDate
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th>Next Hearing Date</th>
                              <td>
                                {typeof d.case_status.nextHearingDate !=
                                  'undefined' &&
                                d.case_status.nextHearingDate.length > 0
                                  ? d.case_status.nextHearingDate
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th> Case Stage</th>
                              <td>
                                {typeof d.case_status.caseStage !=
                                  'undefined' &&
                                d.case_status.caseStage.length > 0
                                  ? d.case_status.caseStage
                                  : 'null'}
                              </td>
                            </tr>
                            <tr>
                              <th> Case Number and Judge</th>
                              <td>
                                {typeof d.case_status.courtNumberAndJudge !=
                                  'undefined' &&
                                d.case_status.courtNumberAndJudge.length > 0
                                  ? d.case_status.courtNumberAndJudge
                                  : 'null'}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div>
                        <p>Petitioner and Advocate</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <tbody className='advocateBody'>
                            <tr>
                              <th>Petitioner</th>
                              <td>
                                {typeof d.petitioner != 'undefined' &&
                                d.petitioner.length > 0 ? (
                                  <>
                                    {d.petitioner.map((dd, idx1) => {
                                      return (
                                        <>
                                          {typeof dd.petitionerName !=
                                            'undefined' &&
                                          dd.petitionerName.length > 0 ? (
                                            <>{dd.petitionerName}</>
                                          ) : (
                                            'null'
                                          )}
                                        </>
                                      )
                                    })}
                                  </>
                                ) : (
                                  'null'
                                )}
                              </td>
                            </tr>
                            <tr>
                              <th> Petitioner Advocate </th>
                              <td>
                                {typeof d.petitioner_advocate != 'undefined' &&
                                d.petitioner_advocate.length > 0 ? (
                                  <>
                                    {d.petitioner_advocate.map((dd, idx1) => {
                                      return (
                                        <>
                                          {typeof dd.petitionerAdvocate !=
                                            'undefined' &&
                                          dd.petitionerAdvocate.length > 0 ? (
                                            <>{dd.petitionerAdvocate}</>
                                          ) : (
                                            'null'
                                          )}
                                        </>
                                      )
                                    })}
                                  </>
                                ) : (
                                  'null'
                                )}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div>
                        <p>Respondent and Advocate</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <tbody>
                            <tr>
                              <th> Respondent </th>
                              <td>
                                {typeof d.respondent != 'undefined' &&
                                d.respondent.length > 0 ? (
                                  <>
                                    {d.respondent.map((dd, idx1) => {
                                      return (
                                        <>
                                          {typeof dd.respondentName !=
                                            'undefined' &&
                                          dd.respondentName.length > 0 ? (
                                            <>{dd.respondentName}</>
                                          ) : (
                                            'null'
                                          )}
                                        </>
                                      )
                                    })}
                                  </>
                                ) : (
                                  'null'
                                )}
                              </td>
                            </tr>
                            <tr>
                              <th> Respondent Advocate </th>
                              <td>
                                {typeof d.respondent_advocate != 'undefined' &&
                                d.respondent_advocate.length > 0 ? (
                                  <>
                                    {typeof d.respondent_advocate !=
                                      'undefined' &&
                                      d.respondent_advocate.map((dd, idx1) => {
                                        return (
                                          <>
                                            {dd.respondentAdvocate.length >
                                            0 ? (
                                              <>{dd.respondentAdvocate}</>
                                            ) : (
                                              'null'
                                            )}
                                          </>
                                        )
                                      })}
                                  </>
                                ) : (
                                  'null'
                                )}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div>
                        <p>Acts</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <thead>
                            <tr>
                              <th>UnderAct(s)</th>
                              <th>Under Section(s)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              {typeof d.laws != 'undefined' &&
                              d.laws.length > 0 ? (
                                <>
                                  {typeof d.laws != 'undefined' &&
                                    d.laws.map((d, idx) => {
                                      console.log('d 000', d)
                                      return (
                                        <>
                                          <td>
                                            {typeof d.act_Name != 'undefined' &&
                                            d.act_Name.length > 0
                                              ? d.act_Name
                                              : null}
                                          </td>
                                          <td>
                                            {typeof d.sectionNo !=
                                              'undefined' &&
                                            d.sectionNo.length > 0
                                              ? d.sectionNo
                                              : null}
                                          </td>
                                        </>
                                      )
                                    })}
                                </>
                              ) : (
                                <span>null</span>
                              )}
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      {/* FIR TABLE - in FIR search only */}
                      {typeof d.fir.firNo != 'undefined' && d.fir.firNo != '' && (
                        <div>
                          <p>FIR Details</p>
                          <table
                            className='table table-bordered'
                            style={{ border: '1px solid black' }}
                          >
                            <thead>
                              <tr>
                                <th>Police Station</th>
                                <th>FIR Number</th>
                                <th>Year</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                {typeof d.fir != 'undefined' &&
                                Object.entries(d.fir).length > 0 ? (
                                  <>
                                    {typeof d.fir != 'undefined' && (
                                      <>
                                        <td>
                                          {typeof d.policeStation != 'undefined'
                                            ? d.fir.policeStation
                                            : null}
                                        </td>
                                        <td>
                                          {typeof d.fir.firNo != 'undefined'
                                            ? d.fir.firNo
                                            : null}
                                        </td>
                                        <td>
                                          {typeof d.fir.firYear != 'undefined'
                                            ? d.fir.firYear
                                            : null}
                                        </td>
                                      </>
                                    )}
                                  </>
                                ) : (
                                  <span>null</span>
                                )}
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      )}
                      {/* CASE TRANSFER - in FIR search only */}
                      {typeof d.caseTransfer != 'undefined' &&
                        d.caseTransfer != null &&
                        d.caseTransfer.length > 0 && (
                          <div>
                            <p>Case Transfer Details within Establishment</p>
                            <table
                              className='table table-bordered'
                              style={{ border: '1px solid black' }}
                            >
                              <thead>
                                <tr>
                                  <th>Registration Number</th>
                                  <th>Transfer Date</th>
                                  <th>From Court Number and Judge</th>
                                  <th>To Court Number and Judge</th>
                                </tr>
                              </thead>
                              <tbody>
                                {typeof d.caseTransfer != 'undefined' &&
                                d.caseTransfer != null &&
                                d.caseTransfer.length > 0 ? (
                                  <>
                                    {typeof d.caseTransfer != 'undefined' &&
                                      d.caseTransfer.map((d, idx) => {
                                        return (
                                          <tr>
                                            <td>
                                              {typeof d.regNo != 'undefined' &&
                                              d.regNo.length > 0
                                                ? d.regNo
                                                : null}
                                            </td>
                                            <td>
                                              {typeof d.transferDate !=
                                                'undefined' &&
                                              d.transferDate.length > 0
                                                ? d.transferDate
                                                : null}
                                            </td>
                                            <td>
                                              {typeof d.fromCourtNoAndJudge !=
                                                'undefined' &&
                                              d.fromCourtNoAndJudge.length > 0
                                                ? d.fromCourtNoAndJudge
                                                : null}
                                            </td>
                                            <td>
                                              {typeof d.toCourtNoAndJudge !=
                                                'undefined' &&
                                              d.toCourtNoAndJudge.length > 0
                                                ? d.toCourtNoAndJudge
                                                : null}
                                            </td>
                                          </tr>
                                        )
                                      })}
                                  </>
                                ) : (
                                  <span>null</span>
                                )}
                              </tbody>
                            </table>
                          </div>
                        )}
                    </>
                  )
                })}
              {typeof hearingData[0] != 'undefined' && hearingData[0] != null && (
                <>
                  <div>
                    <p>Case History</p>
                    <table
                      className='table table-bordered'
                      style={{ border: '1px solid black' }}
                    >
                      <thead>
                        <tr>
                          <th>Judge</th>
                          <th>Business on Date</th>
                          <th>Hearing Date</th>
                          <th>Purpose of Hearing</th>
                        </tr>
                      </thead>
                      <tbody>
                        {typeof hearingData[0] != 'undefined' &&
                          hearingData[0] != null &&
                          hearingData.length > 0 &&
                          hearingData.map((d, idx) => {
                            return (
                              <>
                                {d.hearings.length > 0 &&
                                typeof d.hearings != 'undefined' &&
                                d.hearings[0].business.length > 0 ? (
                                  <>
                                    {typeof d.hearings[0].business !=
                                      'undefined' &&
                                      d.hearings[0].business.map((d1, idx1) => {
                                        // console.log('d1 ??? ', d1)
                                        return (
                                          <>
                                            <tr key={idx1}>
                                              <td>
                                                {typeof d1.caseHistoryObj
                                                  .judges != 'undefined' &&
                                                  d1.caseHistoryObj.judges}
                                              </td>
                                              <td>
                                                {typeof d1.caseHistoryObj
                                                  .businessDate !=
                                                  'undefined' &&
                                                  d1.caseHistoryObj
                                                    .businessDate}
                                              </td>
                                              <td>
                                                {typeof d1.caseHistoryObj
                                                  .nextdate1 != 'undefined' &&
                                                  d1.caseHistoryObj.nextdate1}
                                              </td>
                                              <td>
                                                {typeof d1.caseHistoryObj
                                                  .purposeOfHearing !=
                                                  'undefined' &&
                                                  d1.caseHistoryObj
                                                    .purposeOfHearing}
                                              </td>
                                            </tr>
                                          </>
                                        )
                                      })}
                                  </>
                                ) : null}
                              </>
                            )
                          })}
                      </tbody>
                    </table>
                  </div>

                  <button
                    onClick={() => setShowDetailCaseHis(!showDetailCaseHis)}
                  >
                    ShowDetailCaseHis
                  </button>
                  {showDetailCaseHis == true ? (
                    <>
                      <div>
                        <p style={{}}>Case History Details</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <thead>
                            <tr>
                              <th>courtName</th>
                              <th>caseNumber</th>
                              <th>dateIN</th>
                              <th>inTheCourtOf</th>
                              <th>nextHearingDateIn</th>
                              <th>cnrNumber1</th>
                              <th>nextPurpose</th>
                              <th>petitionerIn</th>
                              <th>respondentIn</th>
                              <th>businessIn</th>
                            </tr>
                          </thead>
                          <tbody>
                            {hearingData[0] != null &&
                              hearingData.length > 0 &&
                              hearingData.map((d, idx) => {
                                return (
                                  <>
                                    {d.hearings.length > 0 &&
                                    typeof d.hearings[0].business !=
                                      'undefined' &&
                                    d.hearings[0].business.length > 0 ? (
                                      <>
                                        {d.hearings[0].business.map(
                                          (d1, idx1) => {
                                            // console.log('d1 ??? ', d1)
                                            return (
                                              <>
                                                <tr key={idx1}>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .courtName
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .caseNumber
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .dateIN
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .inTheCourtOf
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .nextHearingDateIn
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .cnrNumber1
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .nextPurpose
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .petitionerIn
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .respondentIn
                                                    }
                                                  </td>
                                                  <td>
                                                    {
                                                      d1
                                                        .case_history_inside_object
                                                        .businessIn
                                                    }
                                                  </td>
                                                </tr>
                                              </>
                                            )
                                          }
                                        )}
                                      </>
                                    ) : null}
                                  </>
                                )
                              })}
                          </tbody>
                        </table>
                      </div>
                    </>
                  ) : (
                    <>
                      <span> Click to view Case History Detail</span>
                    </>
                  )}
                </>
              )}

              {typeof orderData[0] != 'undefined' &&
                orderData[0] != null &&
                orderData.length > 0 &&
                orderData.map((d, idx) => {
                  // console.log('d order ka >> ', d.interimOrder[0].orderDate)
                  return (
                    <>
                      <div>
                        <p style={{}}>Order</p>
                        <table
                          className='table table-bordered'
                          style={{ border: '1px solid black' }}
                        >
                          <thead>
                            <tr>
                              <th>Order Number</th>
                              <th>Order Date</th>
                              <th>Order Details</th>
                            </tr>
                          </thead>
                          <tbody>
                            {d.interimOrder.map((dd, idx1) => {
                              console.log('dd.orderNo >> ', dd.orderNo)
                              return (
                                <>
                                  <tr>
                                    <td>{dd.orderNo}</td>
                                    <td>{dd.orderDate}</td>
                                    <td>
                                      <a
                                        href={dd.orderDetailsPdfLink}
                                        target='_blank'
                                      >
                                        {dd.orderDetails}
                                      </a>
                                    </td>
                                  </tr>
                                </>
                              )
                            })}
                          </tbody>
                        </table>
                      </div>
                    </>
                  )
                })}
            </>
          ) : (
            <>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <div>
                  <img
                    src='https://hackernoon.com/images/0*4Gzjgh9Y7Gu8KEtZ.gif'
                    alt='loading'
                  />
                </div>
              </div>
            </>
          )}
        </div>
      </Layout>
    </>
  )
}
