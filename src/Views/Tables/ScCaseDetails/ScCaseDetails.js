import React from 'react'
import './ScCaseDetails.css'
import Layout from '../../../Component/Layout/Layout'

export default function ScCaseDetails() {
  return (
    <>
      <Layout>
        <div className='container' style={{ backgroundColor: '#fff' }}>
          <p>Diary No-27714-2019</p>
          <div>
            <p>BABU SINGH vs. THE STATE OF UTTAR PRADESH</p>

            <div>
              <table
                className='table table-bordered'
                style={{ border: '1px solid black' }}
              >
                <tbody className='advocateBody'>
                  <tr>
                    <td>Case Details</td>
                  </tr>
                </tbody>
              </table>
            </div>

            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody>
                <tr>
                  <th>Diary No.</th>
                  <td colSpan='3'>
                    27714/2019 Field on 03-08-2019 12:32 PM [SECTION:XI]
                  </td>
                </tr>
                <tr>
                  <th>Case No.</th>
                  <td colSpan='3'>
                    SLP(C) No.026919-/2019 Registered on 14-11-2019 (Verified On
                    05-11-2019)
                  </td>
                </tr>

                <tr>
                  <th>Present/Last Listed On</th>
                  <td colSpan='3'>
                    18-11-2019[HON'BLE MR. JUSTICE MOHAN M. SHANTANAGOUDAR and
                    HON'BLE MR.JUSTICE SANJIV KHANNA]
                  </td>
                </tr>

                <tr>
                  <th>Status/Stage</th>
                  <td colSpan='3'>
                    DISPOSED(Motion Hearing [FRESH (FOR ADMISSION)-CIVIL CASES])
                    Dismissed-Ord dt 18-11-2019 (Disposal Date:18-11-2019,
                    Month: 11, Year:2019) JUDGES: HON'BLE MR.JUSTICE MOHAN M.
                    SHANTANAGOUDAR, HON'BLE MR. JUSTICE SANJIV KHANNA
                  </td>
                </tr>

                <tr>
                  <th>Disp.Type</th>
                  <td colSpan='3'>Dismissed</td>
                </tr>

                <tr>
                  <th>Category</th>
                  <td colSpan='3'>
                    3505-Land Laws And Agricultural Tenancies: Others
                  </td>
                </tr>

                <tr>
                  <th>Act</th>
                  <td colSpan='3'></td>
                </tr>

                <tr>
                  <th>Petitioner(s)</th>
                  <td colSpan='3'>
                    1 BABU SINGH S/D/W/Thru:-RAGHUVEER SINGH ROOPPUR,
                    DISTRICT:FATEHABAD, AGRA, UTTAR PRADESH
                  </td>
                </tr>

                <tr>
                  <th>Respondent(s)</th>
                  <td colSpan='3'>
                    1 THE STATE OF UTTARAKHAND SECRETARY MEDICAL HEALTH AND
                    FAMILY WELFARE, GOVERNMENT OF UTTARAKHAND , DISTRICT: .
                    ,DEHRADUN , UTTARAKHAND 2 DIRECTOR GENERAL DIRECTOR GENERAL
                    GOVT. OF UTTARAKHAND DEHRADUN , DISTRICT: . ,DEHRADUN ,
                    UTTARAKHAND 3 AHMED NABI S/D/W/Thru:- CHOTE VILLAGE BAZPUR
                    GAON P.O. MAHESHPUR , DISTRICT: BAZPUR ,UDHAM SINGH NAGAR *
                    , UTTARAKHAND 4 CHIEF MEDICAL OFFICER CHIEF MEDICAL OFFICER
                    UDHAM SINGH NAGAR ,UDHAM SINGH NAGAR * , UTTARAKHAND 5
                    DISTRICT MAGISTRATE DISTRICT MAGISTRATE UDHAM SINGH NAGAR
                    ,UDHAM SINGH NAGAR * , UTTARAKHAND 6 UP ZILA ADHIKARI,
                    BAZPUR UP ZILA ADHIKARI, BAZPUR UDHAM SINGH NAGAR ,UDHAM
                    SINGH NAGAR * , UTTARAKHAND 7 B.D. HOSPITAL OWNER MUNNAVAR
                    ALI DORAHA, BAZPUR, DIST. UDHAM SINGH NAGAR , DISTRICT: .
                    ,UDHAM SINGH NAGAR * , UTTARAKHAND 8 PUBLIC HOSPITAL OWNER
                    MUNNAVAR ALI SARKADI ROAD, KELAKHERA , DISTRICT: . ,UDHAM
                    SINGH NAGAR * , UTTARAKHAND 9 INDIAN MEDICAL ASSOCIATION
                    UTTARAKHAND STATE BRANCH DEHRADUN UTTARAKHAND honorary
                    secretary dipak dhar choudhary 182/2 ARYA NAGAR, DEHRADUN,
                    DIST DEHRADUN , DISTRICT: . , , UTTARAKHAND
                  </td>
                </tr>

                <tr>
                  <th>Pet. Advocate(s)</th>
                  <td colSpan='3'>ANU GUPTA</td>
                </tr>

                <tr>
                  <th>Resp. Advocate(s)</th>
                  <td colSpan='3'>SUDARSHAN SINGH RAWAT[R-1]</td>
                </tr>

                <tr>
                  <th>U/Section</th>
                  <td colSpan='3'></td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <tr>
                  <td>Judgement/Orders</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <table
              className='table table-bordered'
              style={{ border: '1px solid black' }}
            >
              <tbody className='advocateBody'>
                <th>Pet. Advocate(s)</th>
                <tr>18-11-2019 [ROP]- Main Case</tr>
                <tr>13-11-2019 [ROP]- Main Case</tr>
              </tbody>
            </table>
          </div>
        </div>
      </Layout>
    </>
  )
}
