import React, { useEffect, useState, setState } from 'react'
import { useParams } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'
// import ReactHtmlParser from 'react-html-parser'
import { Link, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
// import { deleteDataById, getAllData } from '../../Utils/Axios/CommonAxios'
import Config from '../../Utils/Axios/Config'
import MeterialTable from 'material-table'
import Modal from 'react-modal'
import axios from 'axios'
import Layout from '../../Component/Layout/Layout'

export default function CaseProcessings(props) {
  let { qid } = useParams()
  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()
  const history = useHistory()
  const stateData = useSelector((state) => state.login.login)
  let role = stateData.roles

  console.log('Role ==', role)
  const { name, roles, email, token } = useSelector(
    (state) => state.login.login
  )

  const [modalIsOpen, setIsOpen] = React.useState(false)
  const [modalPreviewData, setModalPreviewData] = React.useState({})
  const [showError, setshowError] = useState(false)
  const searchByEmailId = email
  const status = 'In-Progress'

  function openModal() {
    setIsOpen(true)
  }

  function afterOpenModal() {
    // subtitle.style.color = 'black';
  }

  function closeModal() {
    setIsOpen(false)
  }

  const [assiningEmailId, setassiningEmailId] = useState('')
  let changeStatusValue = {
    status,
  }
  let updateEmailId = {
    assiningEmailId,
    status,
  }

  const updateAssignEmailId = (e, data) => {
    e.preventDefault()

    if (Object.entries(data).length > 0) {
      qid = data.UUId
      // alert(assiningEmailId + ' ' + data.UUId)
      setshowError(true)
      if (assiningEmailId !== '') {
        axios({
          url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/caseProcessing/updateEmailWithStatus/${qid}`,
          method: 'PUT',
          data: updateEmailId,
        })
          .then((response) => {
            alert('Sucessfully Updated  ')
            // window.location.reload()
          })
          .catch((error) => {
            alert('Error: ' + error)
          })
      } else {
        alert('Please fill all the fields  ')
      }
    }
  }

  const [myAssignmentTaskData, setmyAssignmentTaskData] = useState([])
  const [currentClkUUID, setCurrentClkUUID] = useState('')
  const [UUId, setUUId] = useState('')
  const [emailDropdown, setEmailDropdown] = useState([])

  const LoadTable = (d) => {
    setmyAssignmentTaskData((myAssignmentTaskData) => [
      ...myAssignmentTaskData,
      {
        // _id: d._id,
        UUId: d.UUId,
        cl_name: d.cl_name,
        cl_email: d.cl_email,
        cl_phno: d.cl_phno,
        cl_address_ln1: d.cl_address_ln1,
        cl_address_ln2: d.cl_address_ln2,
        cl_district: d.cl_district,
        cl_pin_code: d.cl_pin_code,
        cl_police_stn: d.cl_police_stn,
        cl_state: d.cl_state,

        tenant_name: d.tenant_name,
        tenant_email: d.tenant_email,
        tenant_phno: d.tenant_phno,
        tenant_address_ln1: d.tenant_address_ln1,
        tenant_address_ln2: d.tenant_address_ln2,
        tenant_district: d.tenant_district,
        tenant_pin_code: d.tenant_pin_code,
        tenant_police_stn: d.tenant_police_stn,
        tenant_state: d.tenant_state,

        prpty_address_ln1: d.prpty_address_ln1,
        prpty_address_ln2: d.prpty_address_ln2,
        prpty_descp: d.prpty_descp,
        prpty_district: d.prpty_district,
        prpty_police_stn: d.prpty_police_stn,
        prpty_state: d.prpty_state,

        act_additional_payable: d.act_additional_payable,
        act_agreement: d.act_agreement,
        act_apply: d.act_apply,
        act_bill_amount: d.act_bill_amount,
        act_bill_raised: d.act_bill_raised,
        act_monthly_rent: d.act_monthly_rent,
        act_premises_occupying: d.act_premises_occupying,
        act_recipt_issue_name: d.act_recipt_issue_name,
        act_regis_type: d.act_regis_type,
        act_rent_paid_to: d.act_rent_paid_to,
        act_residential_type: d.act_residential_type,
        act_right_granted: d.act_right_granted,
        act_sub_let: d.act_sub_let,
        act_tenure_date_from: d.act_tenure_date_from,
        act_tenure_date_to: d.act_tenure_date_to,

        // createdAt: d.createdAt,

        // updatedAt: d.updatedAt,
        violation: d.violation,
        violation_date: d.violation_date,
        status: d.status,
        assiningEmailId: d.assiningEmailId,
      },
    ])
  }

  useEffect(() => {
    console.log('rajkamalHello')
    // DataTable Filteration based on Account...
    axios({
      method: 'GET',
      url: `http://${Config.IP}:${Config.CASE_PORT}/api/caseprocessingShowDataMySQL/`,
    }).then((response) => {
      response.data.results.map((d, i) => {
        if (roles === '615aa42d0b95d98afaeacf4b') {
          console.log('anda >> ', d)
          LoadTable(d)
        } else if (d.assiningEmailId === searchByEmailId) {
          LoadTable(d)
        }
      })
    })

    // Load "Assess Account" Email data into Dropdown in table...
    axios({
      method: 'GET',
      url: `${Config.PROTO}://${Config.IP}:${Config.CASE_PORT}/api/admin/user`,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => {
        setEmailDropdown(response.data.results)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }, [])

  const modalHandler = (data) => {
    setModalPreviewData(data)
    openModal()
  }

  const data = myAssignmentTaskData
  //myAssignmentTaskData
  const cols = [
    // {
    //   title: 'CaseId',
    //   field: 'UUId',
    //   render: (data) => (
    //     <Link to={`/case-processing/${data._id}`}>{data.UUId}</Link>
    //   ),
    // },

    {
      title: 'CaseId',
      field: 'UUId',
      render: (data) => (
        <div>
          <button
            style={{
              background: 'transparent',
              border: 'none',
              color: '#03a9f4',
            }}
            onClick={() => modalHandler(data)}
          >
            {data.UUId}
          </button>
          <Modal
            isOpen={modalIsOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            contentLabel='Example Modal'
          >
            {modalPreviewData && (
              <>
                <h5 className='mt-5 mb-2 font-weight-bold'>
                  Client/Claimant Info:
                  <label>{modalPreviewData.UUId}</label>
                </h5>
                <div className='row'>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Client Name :
                      </label>
                      <label className=''> {modalPreviewData.cl_name}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold '>
                        Address Line 1:
                      </label>
                      <label className=' '>
                        {modalPreviewData.cl_address_ln1}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold '>
                        Address Line 2:
                      </label>
                      <label className=' '>
                        {modalPreviewData.cl_address_ln2}
                      </label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'> Email :</label>
                      <label className=''>{modalPreviewData.cl_email}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Phone No :
                      </label>
                      <label className=''>{modalPreviewData.cl_phno}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>State :</label>
                      <label className=''>{modalPreviewData.cl_state}</label>
                    </div>
                  </div>

                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        District :
                      </label>
                      <label className=''>{modalPreviewData.cl_district}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Police Station :
                      </label>
                      <label className=''>
                        {modalPreviewData.cl_police_stn}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Pin Code :
                      </label>
                      <label className=''>{modalPreviewData.cl_pin_code}</label>
                    </div>
                  </div>
                </div>

                <h5 className='mt-4 mb-2 font-weight-bold'>Tenant Info : </h5>
                <div className='row'>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Tenant Name :
                      </label>
                      <label className=''>{modalPreviewData.tenant_name}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Address Line 1 :
                      </label>
                      <label className=''>
                        {modalPreviewData.tenant_address_ln1}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Address Line 2 :
                      </label>
                      <label className=''>
                        {modalPreviewData.tenant_address_ln2}
                      </label>
                    </div>
                  </div>

                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'> Email :</label>
                      <label className=''>
                        {modalPreviewData.tenant_email}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Phone No :
                      </label>
                      <label className=''>{modalPreviewData.tenant_phno}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>State :</label>
                      <label className=''>
                        {modalPreviewData.tenant_state}
                      </label>
                    </div>
                  </div>

                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        District :
                      </label>
                      <label className=''>
                        {modalPreviewData.tenant_district}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Police Station :
                      </label>
                      <label className=''>
                        {modalPreviewData.tenant_police_stn}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Pin Code :
                      </label>
                      <label className=''>
                        {modalPreviewData.tenant_pin_code}
                      </label>
                    </div>
                  </div>
                </div>

                <h5 className='mt-4 mb-2 font-weight-bold'>
                  Property Description :
                </h5>
                <div className='row'>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Description :
                      </label>
                      <label className=''>{modalPreviewData.prpty_descp}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>State :</label>
                      <label className=''>{modalPreviewData.prpty_state}</label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        District :
                      </label>
                      <label className=''>
                        {modalPreviewData.prpty_district}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Police Station :
                      </label>
                      <label className=''>
                        {modalPreviewData.prpty_police_stn}
                      </label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Address Line 1 :
                      </label>
                      <label className=''>
                        {modalPreviewData.prpty_address_ln1}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Address Line 2 :
                      </label>
                      <label className=''>
                        {modalPreviewData.prpty_address_ln2}
                      </label>
                    </div>
                  </div>
                </div>

                <h5 className='mt-4 mb-2 font-weight-bold'>
                  Agreement Details :
                </h5>
                <div className='row'>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Act Applicable :
                      </label>
                      <label className=''>{modalPreviewData.act_apply}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Tenure Date From :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_tenure_date_from}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Commencement of tenancy :
                      </label>
                      <label className=''>Null</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Any Special rights apart from space :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_right_granted}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        occupying the premises :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_premises_occupying}
                      </label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Montly Rent :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_monthly_rent}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Additional Payables :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_additional_payable}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Bills Raised receipted :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_bill_raised}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Bill Amount :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_bill_amount}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Rent Paid To :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_rent_paid_to}
                      </label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Residential/Commercial :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_residential_type}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Sub-let: Yes/No :
                      </label>
                      <label className=''>{modalPreviewData.act_sub_let}</label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        In which name is the rent receipt issued :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_recipt_issue_name}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Agreement, If Any :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_agreement}
                      </label>
                    </div>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        If Yes, then registered/ unregistered :
                      </label>
                      <label className=''>
                        {modalPreviewData.act_regis_type}
                      </label>
                    </div>
                  </div>
                </div>

                <h5 className='mt-4 mb-2 font-weight-bold'>
                  Violation of tenancy section :
                </h5>
                <div className='row'>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Violation :
                      </label>
                      <label className=''> {modalPreviewData.violation}</label>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='form-group m-0'>
                      <label className='pr-2 font-weight-bold'>
                        Violation Date :
                      </label>
                      <label className=''>
                        {modalPreviewData.violation_date}
                      </label>
                    </div>
                  </div>
                </div>
              </>
            )}

            <Link
              className='btn btn-info text-white mr-2'
              to={`/case-processing/${modalPreviewData.UUId}`}
            >
              Edit
            </Link>
            <button className='btn btn-danger text-white' onClick={closeModal}>
              Close
            </button>
          </Modal>
        </div>
      ),
    },

    // Client Section...
    { title: 'ClientName', field: 'cl_name' },
    { title: 'ClientEmail', field: 'cl_email' },
    { title: 'ClientPhno', field: 'cl_phno' },
    { title: 'ClientAddress_ln1', field: 'cl_address_ln1' },
    { title: 'ClientAddress_ln2', field: 'cl_address_ln2' },
    { title: 'ClientDistrict', field: 'cl_district' },
    { title: 'ClientPincode', field: 'cl_pin_code' },
    { title: '(Client) Policestation', field: 'cl_police_stn' },
    { title: 'ClientState', field: 'cl_state' },

    // Tenant Section...
    { title: 'Tenant Name', field: 'tenant_name' },
    { title: 'Tenant Email', field: 'tenant_email' },
    { title: 'Tenant phone-no.', field: 'tenant_phno' },
    { title: 'Tenant Address_ln1', field: 'tenant_address_ln1' },
    { title: 'Tenant Address_ln2', field: 'tenant_address_ln2' },
    { title: 'Tenant District', field: 'tenant_district' },
    { title: 'Tenant Pincode', field: 'tenant_pin_code' },
    { title: 'Tenant Police Station', field: 'tenant_police_stn' },
    { title: 'Tenant state', field: 'tenant_state' },

    //Property Section...
    { title: 'Property Address_ln1', field: 'prpty_address_ln1' },
    { title: 'Property Address_ln2', field: 'prpty_address_ln2' },
    { title: 'Property Description', field: 'prpty_descp' },
    { title: 'Property District', field: 'prpty_district' },
    { title: 'Property PoliceStation', field: 'prpty_police_stn' },
    { title: 'Property State', field: 'prpty_state' },

    //Act Section...
    { title: 'Act Additional payable', field: 'act_additional_payable' },
    { title: 'Act Agreement', field: 'act_agreement' },
    { title: 'Act Apply', field: 'act_apply' },
    { title: 'Act Bill Amount', field: 'act_bill_amount' },
    { title: 'Act Bill Raised', field: 'act_bill_raised' },
    { title: 'Act Monthly Rent', field: 'act_monthly_rent' },
    { title: 'Act Premises Occupying', field: 'act_premises_occupying' },
    { title: 'Act Recipt issue name', field: 'act_recipt_issue_name' },
    { title: 'Act Registeration type', field: 'act_regis_type' },
    { title: 'Act Rent paid-to', field: 'act_rent_paid_to' },
    { title: 'Act Residential type', field: 'act_residential_type' },
    { title: 'Act Right granted', field: 'act_right_granted' },
    { title: 'Act Sub_let', field: 'act_sub_let' },
    { title: 'Act Tenure Date-from', field: 'act_tenure_date_from' },
    { title: 'Act Tenure Date-to', field: 'act_tenure_date_to' },

    //Violation Section...
    { title: 'Violation', field: 'violation' },
    { title: 'Violation Date', field: 'violation_date' },
    {
      title: 'Status',
      field: 'status',
      // render: (data) => (
      //   <div>
      //     <button
      //       type='submit'
      //       className='btn btn-success'
      //       value={data.status}
      //       onclick={(e) => changeStatus(e, data)}
      //     >
      //       {data.status}
      //     </button>
      //   </div>
      // ),
    },
    {
      title: 'Assigninged to by Email_Id',
      field: 'assiningEmailId',
    },
    {
      title: 'Assigning Email',
      field: 'assiningEmailId',
      render: (data) => (
        <div style={{ display: 'flex' }}>
          {roles == '614efb3fe57c5007f9dce95f' && (
            <>
              <select
                className='form-control'
                style={{ width: 200 }}
                value={assiningEmailId}
                onChange={(e) => setassiningEmailId(e.target.value)}
              >
                <option>--Select--</option>
                {emailDropdown &&
                  emailDropdown.map((d, i) => {
                    return <option value={d.email}>{d.email}</option>
                  })}
              </select>
              <button
                type='submit'
                className='btn btn-primary ml-1 mr-1'
                onClick={(e) => updateAssignEmailId(e, data)}
              >
                Submit
              </button>
            </>
          )}
        </div>
      ),
    },
  ]

  // const actionHandler = (event, rowData) => {
  //   setCurrentClkUUID(rowData.UUId)
  //   let path = `/case-processing/${rowData._id}`
  //   history.push(path)
  // }

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <lable value={data.UUId} />
          <MeterialTable
            title='User List'
            data={data}
            columns={cols}
            // other props
            options={{
              selection: true,
              rowStyle: (data, index) =>
                index % 2 === 0 ? { backgroundColor: '#f5f5f5' } : null,
              headerStyle: {
                zIndex: 0,
              },
            }}
          />
        </div>
      </div>
    </Layout>
  )
}
