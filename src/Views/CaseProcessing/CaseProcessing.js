import React, { useEffect, useState } from "react";
import FormGenerator from "../../Component/Layout/Formake/FormGenerator";
import Formake from "../../Component/Layout/Formake/Formake";
import { useParams } from "react-router-dom";
import Modal from "react-modal";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";
import "../../Component/Layout/Formake/Formake.css";
import Layout from "../../Component/Layout/Layout";
import Config from "../../Utils/Axios/Config";
import { useHistory } from "react-router-dom";

export default function CaseProcessing(props) {
  const navigation = props;
  let history = useHistory();

  let { qid } = useParams();
  const [oldQID, setOldQID] = useState(qid);
  const [modalIsOpen, setIsOpen] = React.useState(false);

  console.log("myprops", props?.location?.pathname, qid, oldQID);
  useEffect(() => {
    console.log("myprops", qid, oldQID);
    if (qid !== undefined && qid !== "") {
      axios({
        method: "GET",
        url: `http://${Config.IP}:${Config.CASE_PORT}/api/caseProcessing/${qid}`,
      }).then((response) => {
        console.log("Response -->", response.data.results);
        setcl_name(response.data.results.cl_name);
        setcl_address_ln1(response.data.results.cl_address_ln1);
        setcl_address_ln2(response.data.results.cl_address_ln2);
        setcl_email(response.data.results.cl_email);
        setcl_phno(response.data.results.cl_phno);
        setcl_state(response.data.results.cl_state);
        setcl_district(response.data.results.cl_district);
        setcl_police_stn(response.data.results.cl_police_stn);
        setcl_pin_code(response.data.results.cl_pin_code);

        // Tenant Section...
        settenant_name(response.data.results.tenant_name);
        settenant_address_ln1(response.data.results.tenant_address_ln1);
        settenant_address_ln2(response.data.results.tenant_address_ln2);
        settenant_email(response.data.results.tenant_email);
        settenant_phno(response.data.results.tenant_phno);
        settenant_state(response.data.results.tenant_state);
        settenant_district(response.data.results.tenant_district);
        settenant_police_stn(response.data.results.tenant_police_stn);
        settenant_pin_code(response.data.results.tenant_pin_code);

        // Property description section...
        setprpty_descp(response.data.results.prpty_descp);
        setprpty_address_ln1(response.data.results.prpty_address_ln1);
        setprpty_address_ln2(response.data.results.prpty_address_ln2);
        setprpty_state(response.data.results.prpty_state);
        setprpty_district(response.data.results.prpty_district);
        setprpty_police_stn(response.data.results.prpty_police_stn);

        // Agreement details section...
        setact_apply(response.data.results.act_apply);
        setact_agreement(response.data.results.act_agreement);
        setact_regis_type(response.data.results.act_regis_type);
        setact_tenure_date_from(response.data.results.act_tenure_date_from);
        setact_tenure_date_to(response.data.results.act_tenure_date_to);
        setact_monthly_rent(response.data.results.act_monthly_rent);
        setact_additional_payable(response.data.results.act_additional_payable);
        setact_bill_raised(response.data.results.act_bill_raised);
        setact_bill_amount(response.data.results.act_bill_amount);
        setact_rent_paid_to(response.data.results.act_rent_paid_to);
        setact_residential_type(response.data.results.act_residential_type);
        setact_sub_let(response.data.results.act_sub_let);
        setact_right_granted(response.data.results.act_right_granted);
        setact_premises_occupying(response.data.results.act_premises_occupying);
        setact_recipt_issue_name(response.data.results.act_recipt_issue_name);

        // Violation Section...
        setviolation(response.data.results.violation);
        setviolation_date(response.data.results.violation_date);

        //Others...
        setStatus(response.data.results.status);
        // setAssiningEmailId(response.data.results.assiningEmailId)
      });
    }
  }, [qid]);

  function afterOpenModal() {
    // subtitle.style.color = 'black';
  }

  function closeModal() {
    setIsOpen(false);
  }

  useEffect(() => {
    // alert(qid)
    axios({
      url: `http://${Config.IP}:${Config.CASE_PORT}/api/mysql`,
      method: "GET",
    }).then((response) => {
      setData(response.data.results);
    });
  }, []);

  const [showError, setshowError] = useState(false);
  const [UUId, setuuid] = useState("");

  const [cl_name, setcl_name] = useState("");
  const [cl_address_ln1, setcl_address_ln1] = useState("");

  const [cl_address_ln2, setcl_address_ln2] = useState("");
  const [cl_email, setcl_email] = useState("");
  const [cl_phno, setcl_phno] = useState("");
  const [cl_state, setcl_state] = useState("");
  const [cl_district, setcl_district] = useState("");
  const [cl_police_stn, setcl_police_stn] = useState("");
  const [cl_pin_code, setcl_pin_code] = useState("");

  // Tenant Section...
  const [tenant_name, settenant_name] = useState("");
  const [tenant_address_ln1, settenant_address_ln1] = useState("");
  const [tenant_address_ln2, settenant_address_ln2] = useState("");
  const [tenant_email, settenant_email] = useState("");
  const [tenant_phno, settenant_phno] = useState("");
  const [tenant_state, settenant_state] = useState("");
  const [tenant_district, settenant_district] = useState("");
  const [tenant_police_stn, settenant_police_stn] = useState("");
  const [tenant_pin_code, settenant_pin_code] = useState("");

  // Property description section...
  const [prpty_descp, setprpty_descp] = useState("");
  const [prpty_address_ln1, setprpty_address_ln1] = useState("");
  const [prpty_address_ln2, setprpty_address_ln2] = useState("");
  const [prpty_state, setprpty_state] = useState("");
  const [prpty_district, setprpty_district] = useState("");
  const [prpty_police_stn, setprpty_police_stn] = useState("");

  // Agreement details section...
  const [act_apply, setact_apply] = useState("");
  const [act_agreement, setact_agreement] = useState("");
  const [act_regis_type, setact_regis_type] = useState("None");
  const [act_tenure_date_from, setact_tenure_date_from] = useState("");
  const [act_tenure_date_to, setact_tenure_date_to] = useState("");
  const [act_monthly_rent, setact_monthly_rent] = useState("");
  const [act_additional_payable, setact_additional_payable] = useState("");
  const [act_bill_raised, setact_bill_raised] = useState("");
  const [act_bill_amount, setact_bill_amount] = useState("");
  const [act_rent_paid_to, setact_rent_paid_to] = useState("");
  const [act_residential_type, setact_residential_type] = useState("");
  const [act_sub_let, setact_sub_let] = useState("");
  const [act_right_granted, setact_right_granted] = useState("");
  const [act_premises_occupying, setact_premises_occupying] = useState("");
  const [act_recipt_issue_name, setact_recipt_issue_name] = useState("");

  // Violation Section...
  const [violation, setviolation] = useState("");
  const [violation_date, setviolation_date] = useState("");

  // Others...
  const [status, setStatus] = useState("Pending");
  const [assiningEmailId, setAssiningEmailId] = useState("None");

  let insertData = {
    UUId, //CA25803195
    cl_name,
    cl_address_ln1,
    cl_address_ln2,
    cl_email,
    cl_phno,
    cl_state,
    cl_district,
    cl_police_stn,
    cl_pin_code,

    // Tenant Section...
    tenant_name,
    tenant_address_ln1,
    tenant_address_ln2,
    tenant_email,
    tenant_phno,
    tenant_state,
    tenant_district,
    tenant_police_stn,
    tenant_pin_code,

    // Property description section...
    prpty_descp,
    prpty_address_ln1,
    prpty_address_ln2,
    prpty_state,
    prpty_district,
    prpty_police_stn,

    // Agreement details section...
    act_apply,
    act_agreement,
    act_regis_type,
    act_tenure_date_from,
    act_tenure_date_to,
    act_monthly_rent,
    act_additional_payable,
    act_bill_raised,
    act_bill_amount,
    act_rent_paid_to,
    act_residential_type,
    act_sub_let,
    act_right_granted,
    act_premises_occupying,
    act_recipt_issue_name,

    // Violation Section...
    violation,
    violation_date,

    //Others...
    status,
    assiningEmailId,
  };
  let updateData = {
    cl_name,
    cl_address_ln1,
    cl_address_ln2,
    cl_email,
    cl_phno,
    cl_state,
    cl_district,
    cl_police_stn,
    cl_pin_code,

    // Tenant Section...
    tenant_name,
    tenant_address_ln1,
    tenant_address_ln2,
    tenant_email,
    tenant_phno,
    tenant_state,
    tenant_district,
    tenant_police_stn,
    tenant_pin_code,

    // Property description section...
    prpty_descp,
    prpty_address_ln1,
    prpty_address_ln2,
    prpty_state,
    prpty_district,
    prpty_police_stn,

    // Agreement details section...
    act_apply,
    act_agreement,
    act_regis_type,
    act_tenure_date_from,
    act_tenure_date_to,
    act_monthly_rent,
    act_additional_payable,
    act_bill_raised,
    act_bill_amount,
    act_rent_paid_to,
    act_residential_type,
    act_sub_let,
    act_right_granted,
    act_premises_occupying,
    act_recipt_issue_name,

    // Violation Section...
    violation,
    violation_date,

    //Others...
    status,
    // assiningEmailId,
  };

  const preview = (e) => {
    // let uuidData = Math.floor(Math.random() * 100000000)
    // setuuid(`CA${uuidData}`)
    // let tmpUUID = uuidv4()
    // tmpUUID = tmpUUID.toUpperCase()
    // console.log(
    //   '`CA${tmpUUID.split(' - ')[0]}` >> ',
    //   `CA${tmpUUID.split('-')[0]}`
    // )
    // setuuid(`CA${tmpUUID.split('-')[0]}`)
    setuuid(`CA${Date.now().toString().slice(4, -1)}`);

    e.preventDefault();
    setshowError(true);

    if (
      cl_name !== "" &&
      cl_address_ln1 !== "" &&
      cl_address_ln2 !== "" &&
      cl_email !== "" &&
      cl_phno !== "" &&
      cl_state !== "" &&
      cl_district !== "" &&
      cl_police_stn !== "" &&
      cl_pin_code !== "" &&
      // Tenant Section...
      tenant_name !== "" &&
      tenant_address_ln1 !== "" &&
      tenant_address_ln2 !== "" &&
      tenant_email !== "" &&
      tenant_phno !== "" &&
      tenant_state !== "" &&
      tenant_district !== "" &&
      tenant_police_stn !== "" &&
      tenant_pin_code !== "" &&
      // Property description section...
      prpty_descp !== "" &&
      prpty_address_ln1 !== "" &&
      prpty_address_ln2 !== "" &&
      prpty_state !== "" &&
      prpty_district !== "" &&
      prpty_police_stn !== "" &&
      // Agreement details section...
      act_apply !== "" &&
      act_agreement !== "" &&
      // act_regis_type !== '' &&
      act_tenure_date_from !== "" &&
      act_tenure_date_to !== "" &&
      act_monthly_rent !== "" &&
      act_additional_payable !== "" &&
      act_bill_raised !== "" &&
      act_bill_amount !== "" &&
      act_rent_paid_to !== "" &&
      act_residential_type !== "" &&
      act_sub_let !== "" &&
      act_right_granted !== "" &&
      act_premises_occupying !== "" &&
      act_recipt_issue_name !== "" &&
      // Violation Section...
      violation !== "" &&
      violation_date !== ""
    ) {
      setIsOpen(true);
    } else {
      alert("Please fill all the fields  ");
    }
  };

  const addDataHandler = (e) => {
    e.preventDefault();
    setshowError(true);

    if (
      cl_name !== "" &&
      cl_address_ln1 !== "" &&
      cl_address_ln2 !== "" &&
      cl_email !== "" &&
      cl_phno !== "" &&
      cl_state !== "" &&
      cl_district !== "" &&
      cl_police_stn !== "" &&
      cl_pin_code !== "" &&
      // Tenant Section...
      tenant_name !== "" &&
      tenant_address_ln1 !== "" &&
      tenant_address_ln2 !== "" &&
      tenant_email !== "" &&
      tenant_phno !== "" &&
      tenant_state !== "" &&
      tenant_district !== "" &&
      tenant_police_stn !== "" &&
      tenant_pin_code !== "" &&
      // Property description section...
      prpty_descp !== "" &&
      prpty_address_ln1 !== "" &&
      prpty_address_ln2 !== "" &&
      prpty_state !== "" &&
      prpty_district !== "" &&
      prpty_police_stn !== "" &&
      // Agreement details section...
      act_apply !== "" &&
      act_agreement !== "" &&
      // act_regis_type !== '' &&
      act_tenure_date_from !== "" &&
      act_tenure_date_to !== "" &&
      act_monthly_rent !== "" &&
      act_additional_payable !== "" &&
      act_bill_raised !== "" &&
      act_bill_amount !== "" &&
      act_rent_paid_to !== "" &&
      act_residential_type !== "" &&
      act_sub_let !== "" &&
      act_right_granted !== "" &&
      act_premises_occupying !== "" &&
      act_recipt_issue_name !== "" &&
      // Violation Section...
      violation !== "" &&
      violation_date !== ""
    ) {
      axios({
        url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/caseprocessingInsertMySQL`,
        method: "POST",
        data: insertData,
      })
        .then((response) => {
          console.log("responce", response);
          alert("Sucessfully Inserted ");
          history.replace("/case-processing/" + UUId);
          // navigation.navigate("/case-processing/" + UUId);
        })
        .catch((error) => {
          alert("Error: " + error);
        });
    } else {
      alert("Please fill all the fields  ");
    }
  };

  const updateDataHandler = (e) => {
    e.preventDefault();

    setshowError(true);
    if (
      cl_name !== "" &&
      cl_address_ln1 !== "" &&
      cl_address_ln2 !== "" &&
      cl_email !== "" &&
      cl_phno !== "" &&
      cl_state !== "" &&
      cl_district !== "" &&
      cl_police_stn !== "" &&
      cl_pin_code !== "" &&
      // Tenant Section...
      tenant_name !== "" &&
      tenant_address_ln1 !== "" &&
      tenant_address_ln2 !== "" &&
      tenant_email !== "" &&
      tenant_phno !== "" &&
      tenant_state !== "" &&
      tenant_district !== "" &&
      tenant_police_stn !== "" &&
      tenant_pin_code !== "" &&
      // Property description section...
      prpty_descp !== "" &&
      prpty_address_ln1 !== "" &&
      prpty_address_ln2 !== "" &&
      prpty_state !== "" &&
      prpty_district !== "" &&
      prpty_police_stn !== "" &&
      // Agreement details section...
      act_apply !== "" &&
      act_agreement !== "" &&
      // act_regis_type !== '' &&
      act_tenure_date_from !== "" &&
      act_tenure_date_to !== "" &&
      act_monthly_rent !== "" &&
      act_additional_payable !== "" &&
      act_bill_raised !== "" &&
      act_bill_amount !== "" &&
      act_rent_paid_to !== "" &&
      act_residential_type !== "" &&
      act_sub_let !== "" &&
      act_right_granted !== "" &&
      act_premises_occupying !== "" &&
      act_recipt_issue_name !== "" &&
      // Violation Section...
      violation !== "" &&
      violation_date !== ""
    ) {
      axios({
        url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/caseProcessing/${qid}`,
        method: "PUT",
        data: updateData,
      })
        .then((response) => {
          alert("Sucessfully Updated  ");
        })
        .catch((error) => {
          alert("Error: " + error);
        });
    } else {
      alert("Please fill all the fields  ");
    }
  };

  //========= DROPDOWN mysql =======
  const [data, setData] = useState([]);
  const [state, setState] = useState([]);

  const [district1, setDistrict1] = useState([]);
  const [district2, setDistrict2] = useState([]);
  const [district3, setDistrict3] = useState([]);

  const [policeSt1, setPoliceSt1] = useState([]);
  const [policeSt2, setPoliceSt2] = useState([]);
  const [policeSt3, setPoliceSt3] = useState([]);

  data.map((d) => {
    if (state.indexOf(d.state) === -1) {
      state.push(d.state);
    }
  });

  function districtFunc(state, type) {
    console.log("[][][] -- DIS FUNC CALLED --[][][]]");
    let uniques;
    switch (type) {
      case "cl_state":
        console.log(`INSIDE ||||| state : ${state}, type : ${type}`);
        uniques = data
          .map((item) => {
            if (item.state === state) {
              return item.district;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);

        setDistrict1(uniques);
        break;
      case "tenant_state":
        uniques = data
          .map((item) => {
            if (item.state === state) {
              return item.district;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);

        setDistrict2(uniques);
        break;
      case "prpty_state":
        uniques = data
          .map((item) => {
            if (item.state === state) {
              return item.district;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);

        setDistrict3(uniques);
        break;
      default:
        break;
    }
  }
  function policeStFunc(state, district, type) {
    console.log("[][][] -- POLICE STATION FUNC CALLED --[][][]]");

    let uniques = data;
    switch (type) {
      case "cl_district":
        uniques = data
          .map((item) => {
            if (item.state === state && item.district === district) {
              return item.police_station;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);
        setPoliceSt1(uniques);
        break;
      case "tenant_district":
        uniques = data
          .map((item) => {
            if (item.state === state && item.district === district) {
              return item.police_station;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);
        setPoliceSt2(uniques);
        break;
      case "prpty_district":
        uniques = data
          .map((item) => {
            if (item.state === state && item.district === district) {
              return item.police_station;
            }
          })
          .filter((value, index, self) => self.indexOf(value) === index);
        setPoliceSt3(uniques);
        break;
      default:
        break;
    }
  }

  const handleStateChange = (e, type) => {
    switch (type) {
      case "cl_state":
        setcl_state(e.target.value);
        districtFunc(e.target.value, type);
        break;
      case "tenant_state":
        settenant_state(e.target.value, type);
        districtFunc(e.target.value, type);
        break;
      case "prpty_state":
        setprpty_state(e.target.value, type);
        districtFunc(e.target.value, type);
        break;
      default:
        break;
    }
  };

  const handleDistrictChange = (e, type) => {
    switch (type) {
      case "cl_district":
        setcl_district(e.target.value);
        policeStFunc(cl_state, e.target.value, "cl_district");
        break;
      case "tenant_district":
        settenant_district(e.target.value);
        policeStFunc(tenant_state, e.target.value, "tenant_district");
        break;
      case "prpty_district":
        setprpty_district(e.target.value);
        policeStFunc(prpty_state, e.target.value, "prpty_district");
        break;
      default:
        break;
    }
  };
  return (
    <>
      <Layout>
        <div className="row">
          <div className="col-md-12">
            <div className="Regular-form" style={{ width: "100%" }}>
              <form>
                <h6 className="mb-4 ">Client/Claimant Info: </h6>
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className=""> Client Name</label>

                      <input
                        type="text"
                        name="cl_name"
                        onChange={(e) => setcl_name(e.target.value)}
                        value={cl_name}
                        // defaultValue={data.cl_name}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true && cl_name === "" && "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Address Line 1</label>
                      <input
                        type="text"
                        name="cl_address_ln1"
                        onChange={(e) => setcl_address_ln1(e.target.value)}
                        value={cl_address_ln1}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          cl_address_ln1 === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Address Line 2</label>
                      <input
                        type="text"
                        name="cl_address_ln2"
                        onChange={(e) => setcl_address_ln2(e.target.value)}
                        value={cl_address_ln2}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          cl_address_ln2 === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Email</label>
                      <input
                        type="email"
                        name="cl_email"
                        onChange={(e) => setcl_email(e.target.value)}
                        value={cl_email}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true && cl_email === "" && "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Phone No.</label>
                      <input
                        type="text"
                        name="cl_phno"
                        onChange={(e) => setcl_phno(e.target.value)}
                        value={cl_phno}
                        className="form-control"
                        maxLength="10"
                      />
                      <label className="text-danger">
                        {showError === true && cl_phno === "" && "required"}
                      </label>
                    </div>
                    {/* ola1 */}
                    <div className="form-group m-0">
                      <label className=""> State</label>
                      <select
                        className="form-control"
                        name="cl_state"
                        onChange={(e) => {
                          handleStateChange(e, "cl_state");
                        }}
                        value={cl_state}
                      >
                        <option>
                          {cl_state === "" ? "--Select--" : cl_state}
                        </option>
                        {data &&
                          state.map((d, i) => {
                            return (
                              <option key={i} value={d}>
                                {d}
                              </option>
                            );
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true && cl_state === "" && "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">District</label>
                      <select
                        name="cl_district"
                        className="form-control"
                        onChange={(e) => handleDistrictChange(e, "cl_district")}
                        value={cl_district}
                      >
                        {/* <option>
                        {cl_district == '' ? '--Select--' : cl_district}
                      </option> */}
                        <option>--Select--</option>
                        {data &&
                          district1.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true && cl_district === "" && "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Police Station</label>
                      <select
                        name="cl_police_stn"
                        className="form-control"
                        onChange={(e) => setcl_police_stn(e.target.value)}
                        value={cl_police_stn}
                      >
                        <option>
                          {cl_police_stn === "" ? "--Select--" : cl_police_stn}
                        </option>
                        {data &&
                          policeSt1.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          cl_police_stn === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=""> Pin Code</label>
                      <input
                        type="text"
                        name="cl_pin_code"
                        onChange={(e) => setcl_pin_code(e.target.value)}
                        value={cl_pin_code}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true && cl_pin_code === "" && "required"}
                      </label>
                    </div>
                  </div>
                </div>
                <h6 className="mb-4 ">Tenant Info: </h6>
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Tenant Name</label>
                      <input
                        type="text"
                        name="tenant_name"
                        onChange={(e) => settenant_name(e.target.value)}
                        value={tenant_name}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true && tenant_name === "" && "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Address Line 1</label>
                      <input
                        type="text"
                        name="tenant_address_ln1"
                        onChange={(e) => settenant_address_ln1(e.target.value)}
                        value={tenant_address_ln1}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          tenant_address_ln1 === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Address Line 2</label>
                      <input
                        type="text"
                        name="tenant_address_ln2"
                        onChange={(e) => settenant_address_ln2(e.target.value)}
                        value={tenant_address_ln2}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          tenant_address_ln2 === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Email</label>
                      <input
                        type="email"
                        name="tenant_email"
                        onChange={(e) => settenant_email(e.target.value)}
                        value={tenant_email}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true &&
                          tenant_email === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Phone No.</label>
                      <input
                        type="text"
                        name="tenant_phno"
                        onChange={(e) => settenant_phno(e.target.value)}
                        value={tenant_phno}
                        className="form-control"
                        maxLength="10"
                      />
                      <label className="text-danger">
                        {showError === true && tenant_phno === "" && "required"}
                      </label>
                    </div>
                    {/* ola2 */}
                    <div className="form-group m-0">
                      <label className=""> State</label>
                      <select
                        className="form-control"
                        name="tenant_state"
                        onChange={(e) => {
                          handleStateChange(e, "tenant_state");
                        }}
                        value={tenant_state}
                      >
                        <option>
                          {tenant_state === "" ? "--Select--" : tenant_state}
                        </option>
                        {data &&
                          state.map((d, i) => {
                            return (
                              <option key={i} value={d}>
                                {d}
                              </option>
                            );
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          tenant_state === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">District</label>
                      <select
                        name="tenant_district"
                        className="form-control"
                        onChange={(e) =>
                          handleDistrictChange(e, "tenant_district")
                        }
                        value={tenant_district}
                      >
                        <option>
                          {tenant_district === ""
                            ? "--Select--"
                            : tenant_district}
                        </option>
                        {data &&
                          district2.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          tenant_district === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Police Station</label>
                      <select
                        name="tenant_police_stn"
                        className="form-control"
                        onChange={(e) => settenant_police_stn(e.target.value)}
                        value={tenant_police_stn}
                      >
                        <option>
                          {tenant_police_stn === ""
                            ? "--Select--"
                            : tenant_police_stn}
                        </option>
                        {data &&
                          policeSt2.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          tenant_police_stn === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=""> Pin Code</label>
                      <input
                        type="text"
                        name="tenant_pin_code"
                        onChange={(e) => settenant_pin_code(e.target.value)}
                        value={tenant_pin_code}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true &&
                          tenant_pin_code === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                </div>
                <h6 className="mb-4 ">Property Description :</h6>
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Description :</label>
                      <input
                        type="text"
                        name="prpty_descp"
                        onChange={(e) => setprpty_descp(e.target.value)}
                        value={prpty_descp}
                        className="form-control"
                      />
                      <label className="text-danger">
                        {showError === true && prpty_descp === "" && "required"}
                      </label>
                    </div>
                    {/* ola3 */}
                    <div className="form-group m-0">
                      <label className=""> State</label>
                      <select
                        className="form-control"
                        name="prpty_state"
                        onChange={(e) => {
                          handleStateChange(e, "prpty_state");
                        }}
                        value={prpty_state}
                      >
                        <option>
                          {prpty_state === "" ? "--Select--" : prpty_state}
                        </option>
                        {data &&
                          state.map((d, i) => {
                            return (
                              <option key={i} value={d}>
                                {d}
                              </option>
                            );
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true && prpty_state === "" && "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">District</label>
                      <select
                        name="prpty_district"
                        className="form-control"
                        onChange={(e) =>
                          handleDistrictChange(e, "prpty_district")
                        }
                        value={prpty_district}
                      >
                        <option>
                          {prpty_district === ""
                            ? "--Select--"
                            : prpty_district}
                        </option>
                        {data &&
                          district3.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          prpty_district === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Police Station</label>
                      <select
                        name="prpty_police_stn"
                        className="form-control"
                        onChange={(e) => setprpty_police_stn(e.target.value)}
                        value={prpty_police_stn}
                      >
                        <option>
                          {prpty_police_stn === ""
                            ? "--Select--"
                            : prpty_police_stn}
                        </option>
                        {data &&
                          policeSt3.map((d, i) => {
                            if (d !== undefined && d !== "") {
                              return (
                                <option key={i} value={d}>
                                  {d}
                                </option>
                              );
                            }
                          })}
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          prpty_police_stn === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className=" ">Address Line 1</label>
                      <input
                        type="text"
                        name="prpty_address_ln1"
                        onChange={(e) => setprpty_address_ln1(e.target.value)}
                        value={prpty_address_ln1}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          prpty_address_ln1 === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Address Line 2</label>
                      <input
                        type="text"
                        name="prpty_address_ln2"
                        onChange={(e) => setprpty_address_ln2(e.target.value)}
                        value={prpty_address_ln2}
                        className="form-control "
                      />
                      <label className="text-danger">
                        {showError === true &&
                          prpty_address_ln2 === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                </div>
                <h6 className="mb-4 ">Agreement Details :</h6>
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Act Applicable</label>
                      <select
                        name="act_apply"
                        className="form-control"
                        onChange={(e) => setact_apply(e.target.value)}
                        value={act_apply}
                      >
                        <option value="">--Select--</option>
                        <option value="Lease">Lease</option>
                        <option value="Tenancy">Tenancy</option>
                        <option value="LEAVE LICENSE">LEAVE LICENSE</option>
                      </select>
                      <label className="text-danger">
                        {showError === true && act_apply === "" && "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Tenure Date From</label>
                      <input
                        type="date"
                        className="form-control"
                        onChange={(e) =>
                          setact_tenure_date_from(e.target.value)
                        }
                        value={act_tenure_date_from}
                        name="act_tenure_date_from"
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_tenure_date_from === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Commencement of tenancy:</label>
                      <input
                        type="date"
                        className="form-control"
                        name="act_tenure_date_to"
                        onChange={(e) => setact_tenure_date_to(e.target.value)}
                        value={act_tenure_date_to}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_tenure_date_to === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">
                        Any special rights apart from space
                      </label>
                      <select
                        name="act_right_granted"
                        className="form-control "
                        onChange={(e) => setact_right_granted(e.target.value)}
                        value={act_right_granted}
                      >
                        <option value="">--Select--</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          act_right_granted === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">
                        Original tenant alive & still in occupation or someone
                        else occupying the premises:
                      </label>
                      <select
                        name="act_premises_occupying"
                        className="form-control "
                        onChange={(e) =>
                          setact_premises_occupying(e.target.value)
                        }
                        value={act_premises_occupying}
                      >
                        <option value="">--Select--</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          act_premises_occupying === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Monthly Rent</label>
                      <input
                        type="text"
                        name="act_monthly_rent"
                        className="form-control"
                        onChange={(e) => setact_monthly_rent(e.target.value)}
                        value={act_monthly_rent}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_monthly_rent === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Additional Payables</label>
                      <input
                        type="text"
                        name="act_additional_payable"
                        className="form-control"
                        onChange={(e) =>
                          setact_additional_payable(e.target.value)
                        }
                        value={act_additional_payable}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_additional_payable === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Bills Raised receipted</label>
                      <input
                        type="text"
                        name="act_bill_raised"
                        className="form-control"
                        onChange={(e) => setact_bill_raised(e.target.value)}
                        value={act_bill_raised}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_bill_raised === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Bill Amount</label>
                      <input
                        type="text"
                        name="act_bill_amount"
                        className="form-control"
                        onChange={(e) => setact_bill_amount(e.target.value)}
                        value={act_bill_amount}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_bill_amount === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="pt-4">Rent Paid To</label>
                      <select
                        name="act_rent_paid_to"
                        className="form-control "
                        onChange={(e) => setact_rent_paid_to(e.target.value)}
                        value={act_rent_paid_to}
                      >
                        <option value="">--Select--</option>
                        <option value="LANDLORD">LANDLORD</option>
                        <option value="RENT CONTROLLER">RENT CONTROLLER</option>
                        <option value="THIRD PARTY">THIRD PARTY</option>
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          act_rent_paid_to === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className=" ">Residential/ Commercial</label>
                      <select
                        name="act_residential_type"
                        className="form-control "
                        onChange={(e) =>
                          setact_residential_type(e.target.value)
                        }
                        value={act_residential_type}
                      >
                        <option value="">--Select--</option>
                        <option value="Residential">Residential</option>
                        <option value="Commercial">Commercial</option>
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          act_residential_type === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className=" ">Sub-let: Yes/No</label>
                      <select
                        name="act_sub_let"
                        className="form-control "
                        onChange={(e) => setact_sub_let(e.target.value)}
                        value={act_sub_let}
                      >
                        <option value="">--Select--</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                      <label className="text-danger">
                        {showError === true && act_sub_let === "" && "required"}
                      </label>
                    </div>

                    <div className="form-group m-0">
                      <label className=" ">
                        In which name is the rent receipt issued
                      </label>
                      <input
                        type="text"
                        name="act_recipt_issue_name"
                        className="form-control "
                        onChange={(e) =>
                          setact_recipt_issue_name(e.target.value)
                        }
                        value={act_recipt_issue_name}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          act_recipt_issue_name === "" &&
                          "required"}
                      </label>
                    </div>
                    <div className="form-group m-0">
                      <label className="">Agreement, If Any</label>
                      <select
                        className="form-control"
                        name="act_agreement"
                        onChange={(e) => setact_agreement(e.target.value)}
                        value={act_agreement}
                      >
                        <option value="">--Select--</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                      <label className="text-danger">
                        {showError === true &&
                          act_agreement === "" &&
                          "required"}
                      </label>
                    </div>
                    {act_agreement === "Yes" && (
                      <div className="form-group m-0">
                        <label className="pt-4">
                          If Yes, then registered/ unregistered:{" "}
                        </label>
                        <select
                          className="form-control"
                          name="act_regis_type"
                          onChange={(e) => setact_regis_type(e.target.value)}
                          value={act_regis_type}
                        >
                          <option value="">--Select--</option>
                          <option value="registered">registered</option>
                          <option value="unregistered">unregistered</option>
                        </select>
                      </div>
                    )}
                  </div>
                </div>
                <h6 className="mb-4 ">Violation of tenancy section: </h6>
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Violation</label>
                      <input
                        type="text"
                        className="form-control"
                        name="violation"
                        onChange={(e) => setviolation(e.target.value)}
                        value={violation}
                      />
                      <label className="text-danger">
                        {showError === true && violation === "" && "required"}
                      </label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group m-0">
                      <label className="">Violation Date</label>
                      <input
                        type="date"
                        className="form-control"
                        name="violation_date"
                        onChange={(e) => setviolation_date(e.target.value)}
                        value={violation_date}
                      />
                      <label className="text-danger">
                        {showError === true &&
                          violation_date === "" &&
                          "required"}
                      </label>
                    </div>
                  </div>
                </div>

                {/*--Modal--  */}

                <button
                  type="submit"
                  className="btn btn-info text-white"
                  onClick={(e) => preview(e)}
                >
                  <i class="fa fa-eye"></i> Preview
                </button>
                <Modal
                  isOpen={modalIsOpen}
                  onAfterOpen={afterOpenModal}
                  onRequestClose={closeModal}
                  contentLabel="Example Modal"
                >
                  <h5 className="mt-5 mb-2 font-weight-bold">
                    Client/Claimant Info:{" "}
                  </h5>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          {" "}
                          Client Name :
                        </label>
                        <label className=""> {cl_name}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold ">
                          Address Line 1:
                        </label>
                        <label className=" ">{cl_address_ln1}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold ">
                          Address Line 2:
                        </label>
                        <label className=" ">{cl_address_ln2}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          {" "}
                          Email :
                        </label>
                        <label className="">{cl_email}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          {" "}
                          Phone No :
                        </label>
                        <label className="">{cl_phno}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">State :</label>
                        <label className="">{cl_state}</label>
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          District :
                        </label>
                        <label className="">{cl_district}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Police Station :
                        </label>
                        <label className="">{cl_police_stn} </label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Pin Code :
                        </label>
                        <label className="">{cl_pin_code}</label>
                      </div>
                    </div>
                  </div>

                  <h5 className="mt-4 mb-2 font-weight-bold">Tenant Info : </h5>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          {" "}
                          Tenant Name :
                        </label>
                        <label className="">{tenant_name}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Address Line 1 :
                        </label>
                        <label className="">{tenant_address_ln1}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Address Line 2 :
                        </label>
                        <label className="">{tenant_address_ln2}</label>
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          {" "}
                          Email :
                        </label>
                        <label className="">{tenant_email}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Phone No :
                        </label>
                        <label className="">{tenant_phno}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">State :</label>
                        <label className="">{tenant_state}</label>
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          District :
                        </label>
                        <label className="">{tenant_district}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Police Station :
                        </label>
                        <label className="">{tenant_police_stn}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Pin Code :
                        </label>
                        <label className="">{tenant_pin_code}</label>
                      </div>
                    </div>
                  </div>

                  <h5 className="mt-4 mb-2 font-weight-bold">
                    Property Description :{" "}
                  </h5>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Description :
                        </label>
                        <label className="">{prpty_descp}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">State :</label>
                        <label className="">{prpty_state}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          District :
                        </label>
                        <label className="">{prpty_district}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Police Station :
                        </label>
                        <label className="">{prpty_police_stn}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Address Line 1 :
                        </label>
                        <label className="">{prpty_address_ln1}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Address Line 2 :
                        </label>
                        <label className="">{prpty_address_ln2}</label>
                      </div>
                    </div>
                  </div>

                  <h5 className="mt-4 mb-2 font-weight-bold">
                    Agreement Details :{" "}
                  </h5>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Act Applicable :
                        </label>
                        <label className="">{act_apply}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Tenure Date From :
                        </label>
                        <label className="">{act_tenure_date_from}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Commencement of tenancy :
                        </label>
                        <label className="">Null</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Any Special rights apart from space :
                        </label>
                        <label className="">{act_right_granted}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          occupying the premises :
                        </label>
                        <label className="">{act_premises_occupying}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Montly Rent :
                        </label>
                        <label className="">{act_monthly_rent}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Additional Payables :
                        </label>
                        <label className="">{act_additional_payable}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Bills Raised receipted :
                        </label>
                        <label className="">{act_bill_raised}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Bill Amount :
                        </label>
                        <label className="">{act_bill_amount}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Rent Paid To :
                        </label>
                        <label className="">{act_rent_paid_to}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Residential/Commercial :
                        </label>
                        <label className="">{act_residential_type}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Sub-let: Yes/No :
                        </label>
                        <label className="">{act_sub_let}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          In which name is the rent receipt issued :
                        </label>
                        <label className="">{act_recipt_issue_name}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Agreement, If Any :
                        </label>
                        <label className="">{act_agreement}</label>
                      </div>
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          If Yes, then registered/ unregistered :
                        </label>
                        <label className="">{act_regis_type}</label>
                      </div>
                    </div>
                  </div>

                  <h5 className="mt-4 mb-2 font-weight-bold">
                    Violation of tenancy section :{" "}
                  </h5>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Violation :
                        </label>
                        <label className=""> {violation}</label>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group m-0">
                        <label className="pr-2 font-weight-bold">
                          Violation Date :
                        </label>
                        <label className="">{violation_date}</label>
                      </div>
                    </div>
                  </div>
                  <div className="mt-4">
                    {qid === undefined ? (
                      <button
                        type="submit"
                        className="btn btn-primary mr-4"
                        onClick={(e) => addDataHandler(e)}
                      >
                        Submit
                      </button>
                    ) : (
                      <button
                        type="submit"
                        className="btn btn-primary mr-4"
                        onClick={(e) => updateDataHandler(e)}
                      >
                        Submit
                      </button>
                    )}
                    <button className="btn btn-danger" onClick={closeModal}>
                      Close
                    </button>
                  </div>
                </Modal>
              </form>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}
