import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteDataById, getAllData } from '../../Utils/Axios/CommonAxios'
import Config from '../../Utils/Axios/Config'
import ExtendedDataTables from '../../Component/ExtendedDataTables/ExtendedDataTables'
import Layout from '../../Component/Layout/Layout'

export default function Cases(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props
  const navigation = props
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const login = useSelector((state) => state.login)
  const { token } = login
  // console.log('TokenHai', token)

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <ExtendedDataTables
            navigation={navigation}
            editRoute='/case'
            // deleteRoute="/delete_FORM_ROUTE"
            cols={{
              // _id: "UID",
              cino: 'CNR Number',
              // case_type: null,
              // category: null,
              // classification: null,
              // court: null,
              // courtType: null,
              // court_code: null,
              // description: null,
              // appear_as: null,
              // appear_model: null,
              // before_judge: null,
              // caseNo: null,
              // casePR: null,
              // caseTNY: null,
              // priority: null,
              // referred_by: null,
              // rgyear: null,
              // search_case_no: null,
              // title: null,
              // year: null,
              caseType: 'CASE TYPE',
              filingNumber: 'FILING NO',
              registrationNumber: 'Reg NO',
              filingDate: 'FILING DATE',
              registrationDate: 'REG DATE',
              firstHearingDate: 'First Hearing Date',
              nextHearingDate: 'Next Hearing Date',
              caseStage: 'Case Stage',
              courtNumberAndJudge: 'Court No and Judge',
            }}
            viewApiType='GET'
            viewApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
            editApiType='PUT'
            editApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
            deleteApiType='DELETE'
            deleteApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
            headers={{
              Authorization: 'Bearer ' + token,
            }}
          />
        </div>
      </div>
    </Layout>
  )
}
