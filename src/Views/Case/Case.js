import React from 'react'
import FormGenerator from '../../Component/Layout/Formake/FormGenerator'
import Formake from '../../Component/Layout/Formake/Formake'
import { useParams } from 'react-router-dom'
import '../../Component/Layout/Formake/Formake.css'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'

export default function Case() {
  let { qid } = useParams()

  return (
    <>
      <Layout>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='Regular-form'>
                <Formake
                  useAxios={true}
                  edit={qid ? true : false}
                  editId={qid}
                  title={qid ? 'Edit Case' : 'Add Case'}
                  createApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
                  createType='POST'
                  editApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
                  editType='GET'
                  updateApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
                  updateType='PUT'
                  deleteApi={`http://${Config.IP}:${Config.CASE_PORT}/api/case`}
                  deleteType='DELETE'
                  auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                  headers={{}}
                  params={{}}
                  initialState={{
                    level: null,
                    questionType: null,
                    question: null,
                    answer: null,
                    points: null,
                  }}
                  // colwidths in number totaling upto 12 e.g. 8+4= 12
                  cols={[4, 4, 4]}
                  spacing={3}
                  isSubmitting={false}
                  //used for default change handler functions having switch case inside the to set proper setState
                  updatedValues={(formValues) =>
                    console.log('formValues UPDATING --> ', formValues)
                  }
                  onChangeHandler={(name, value) => {
                    console.log('formValuesx UPDATING --> ', name, value)
                  }}
                  onSubmit={(values) =>
                    console.log('formValues SUBMITTING', values)
                  }
                  fields={[
                    [
                      // caseType: { type: String },
                      // filingNumber: { type: String },
                      // registrationNumber: { type: String },
                      // cnrNumber: { type: String },
                      // filingDate: { type: String },
                      // registrationDate: { type: String },
                      // firstHearingDate: { type: String },
                      // nextHearingDate: { type: String },
                      // caseStage: { type: String },
                      // courtNumberAndJudge: { type: String },
                      {
                        type: 'text',
                        name: 'cnrNumber',
                        label: 'Cnr Number',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'caseType',
                        label: 'Case Type',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'filingNumber',
                        label: 'Filing Number',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'registrationNumber',
                        label: 'Registration Number',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'filingDate',
                        label: 'Filing Date',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'firstHearingDate',
                        label: 'First Hearing Date',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'nextHearingDate',
                        label: 'Next Hearing Date',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'caseStage',
                        label: 'Case Stage',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'courtNumberAndJudge',
                        label: 'Court Number And Judge',
                        props: {},
                      },
                    ],
                  ]}
                  /*
                  fields={[
                    [
                      {
                        type: 'select',
                        name: 'court',
                        label: 'Court',
                        props: {
                          options: [{ CourtComplex: 'courtcomplex' }],
                        },
                      },
                      {
                        type: "text",
                        name: "petitioner",
                        label: "Petitioner",
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'court_hall',
                        label: 'Court Hall',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'case_no',
                        label: 'Case No',
                        // props: {},
                      },
                      {
                        type: 'select',
                        name: 'year',
                        label: 'Year',
                        props: {
                          options: [{ Year: '2019', Year: '2020' }],
                        },
                      },
                      {
                        type: 'text',
                        name: 'classification',
                        label: 'Classification',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'date_of_filing',
                        label: 'Date Of Filing',
                        // props: {},
                      },
                      {
                        type: 'text',
                        name: 'floor',
                        label: 'Floor',
                        // props: {},
                      },
                    ],
                  ]}
                  */
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
