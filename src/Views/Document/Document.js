import React from 'react'
import FormGenerator from '../../Component/Layout/Formake/FormGenerator'
import Formake from '../../Component/Layout/Formake/Formake'
import { useParams } from 'react-router-dom'
import '../../Component/Layout/Formake/Formake.css'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'

export default function Document() {
  let { qid } = useParams()

  return (
    <>
      <Layout>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='Regular-form'>
                <Formake
                  useAxios={true}
                  edit={qid ? true : false}
                  editId={qid}
                  title={qid ? 'Edit Document' : 'Add Document'}
                  createApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
                  createType='POST'
                  editApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
                  editType='GET'
                  updateApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
                  updateType='PUT'
                  deleteApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
                  deleteType='DELETE'
                  auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                  headers={{}}
                  params={{}}
                  initialState={{
                    level: null,
                    questionType: null,
                    question: null,
                    answer: null,
                    points: null,
                    // levels: '60deacc31c0b9a091c2cb2f4',
                  }}
                  // colwidths in number totaling upto 12 e.g. 8+4= 12
                  cols={[4, 4, 4]}
                  spacing={3}
                  isSubmitting={false}
                  //used for default change handler functions having switch case inside the to set proper setState
                  updatedValues={(formValues) =>
                    console.log('formValues UPDATING --> ', formValues)
                  }
                  onChangeHandler={(name, value) => {
                    console.log('formValuesx UPDATING --> ', name, value)
                  }}
                  onSubmit={(values) =>
                    console.log('formValues SUBMITTING', values)
                  }
                  fields={[
                    [
                      {
                        type: 'select',
                        name: 'document_type_id',
                        label: 'Document Type Id',
                        props: {
                          options: [{ CourtComplex: 'courtcomplex' }],
                        },
                      },
                      {
                        type: 'text',
                        name: 'description',
                        label: 'Description',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'judgement_date',
                        label: 'Judgement Date',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'expiry_date',
                        label: 'Expiry Date',
                        // props: {},
                      },
                      // {
                      //   type: "select",
                      //   name: "year",
                      //   label: "Year",
                      //   props: {
                      //     options: [{ Year: "2019", Year: "2020" }],
                      //   },
                      // },
                      {
                        type: 'text',
                        name: 'purpose',
                        label: 'Purpose',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'headed_by',
                        label: 'Headed By',
                        // props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'first_party',
                        label: 'First Party',
                        // props: {},
                      },
                      {
                        type: 'text',
                        name: 'second_party',
                        label: 'Second Party',
                        // props: {},
                      },
                    ],
                  ]}
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
