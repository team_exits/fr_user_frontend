import { Button, makeStyles } from "@material-ui/core";
import React from "react";
import ReactHtmlParser from "react-html-parser";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { deleteDataById, getAllData } from "../../Utils/Axios/CommonAxios";
import Config from "../../Utils/Axios/Config";
import ExtendedDataTables from "../../Component/ExtendedDataTables/ExtendedDataTables";
import Layout from "../../Component/Layout/Layout";

export default function Documents(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props;
  const navigation = props;
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: "flex",
      overflowY: "scroll",
      width: "100%",
    },
  }));
  const classes = useStyles();

  const login = useSelector((state) => state.login);
  const { token } = login;
  console.log("TokenHai", token);

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <ExtendedDataTables
            navigation={navigation}
            editRoute="/document"
            // deleteRoute="/delete_FORM_ROUTE"
            cols={{
              // _id: "UID",
            //   document_type_id:"Document Type Id",
              description: "court",
              judgement_date: "Judgement Date",
              expiry_date: "Expiry Date",
              purpose: "Purpose",
              first_party: "First Party",
              second_party: "Second Party",
              headed_by: "Headed By", 
            }}
            viewApiType="GET"
            viewApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
            editApiType="PUT"
            editApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
            deleteApiType="DELETE"
            deleteApi={`http://${Config.IP}:${Config.DOC_PORT}/api/document`}
            headers={{
              Authorization: "Bearer " + token,
            }}
          />
        </div>
      </div>
    </Layout>
  );
}
