import React from 'react'
import CaseBox from '../../Component/Home/CaseBox/CaseBox'
import { CircularProgressbar } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import { BsThreeDots } from 'react-icons/bs'
import { HiOutlineSearch } from 'react-icons/hi'
import Layout from '../../Component/Layout/Layout'
import Cal from '../../Component/Calendar/DemoApp'

const Dashboard = () => {
  return (
    <Layout>
      <div className='container'>
        <div className='row'>
          <CaseBox
            percent={25}
            title='TOTAL NUMBER OF CASE DIARY'
            type='Cases'
            typeCount={12}
          />
          <CaseBox
            percent={25}
            title='TOTAL NUMBER OF CASE DIARY'
            type='Cases'
            typeCount={12}
          />
          <CaseBox
            percent={25}
            title='TOTAL NUMBER OF CASE DIARY'
            type='Cases'
            typeCount={12}
          />
        </div>
        <Cal />
      </div>
    </Layout>
  )
}

export default Dashboard
