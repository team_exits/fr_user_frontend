import React, { useState, useEffect, useReducer } from 'react'
import { IoDownloadOutline, IoEllipsisHorizontal } from 'react-icons/io5'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { RiCalendar2Fill } from 'react-icons/ri'
import DatePicker from 'react-datepicker'
import refresh from './refresh.jpg'
import { FcSearch } from 'react-icons/fc'

import './Ecourts.css'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'
import Layout from '../../Component/Layout/Layout'
import Config from '../../../src/Utils/Axios/Config'
import Supreme from './Supreme.js'
import District from './District.js'

export const Ecourts = (props) => {
  const [captcha, setCaptcha] = useState(null)
  const [myUUID, setMyUUID] = React.useState(null)
  const [scraperResponse, setScraperResponse] = React.useState(null)
  const [caseObject, setCaseObject] = React.useState({})

  const [cheerioScrapedData, setCheerioScrapedData] = React.useState(null)
  const [searchStatus, setSearchStatus] = React.useState('IDLE')
  const [captchaStatus, setCaptchaStatus] = React.useState(false)
  const [actDataBtnClick, setActDataBtnClick] = React.useState(false)

  // let tmpUUID = uuidv4()

  const [data, setData] = useState({
    advocate: '',
    barcode: '',
    captcha: '',
    caseListDate: '',
    caseType: '',
    case_no: '',
    courtCode: '',
    city: '-1',
    court: '',
    courtType: '',
    firNumber: '',
    petitioner_respondent: '',
    policeStation: '',
    searchAdvocateBy: '',
    searchBy: '-1',
    state: '-1',
    statecode: '',
    status: '-1',
    year: '',
    case_num: '',
    search_act: '',
    under_sec: '',
    actcode: '',
    karnatakaBenchField: '',
  })
  const resethandle = () => {
    setData({
      advocate: '',
      barcode: '',
      captcha: '',
      caseListDate: '',
      caseType: '',
      case_no: '',
      courtCode: '',
      city: '-1',
      court: '',
      courtType: '',
      firNumber: '',
      petitioner_respondent: '',
      policeStation: '',
      searchAdvocateBy: '',
      searchBy: '-1',
      state: '-1',
      statecode: '',
      status: '-1',
      year: '',
      case_num: '',
      search_act: '',
      under_sec: '',
      actcode: '',
    })
  }

  const [statedata, setstatedata] = useState([])
  const [citydata, setcitydata] = useState([])

  const [firData, setFirData] = useState([])
  const [actData, setActData] = useState([])

  const [f, setf] = useState([])

  const [courtcodedata, setcourtcodedata] = useState([])

  const [casetypedata, setcasetypedata] = useState([])

  const [complexCode, setComplexCode] = useState(null)

  //mulla
  const getActTypeDropdown = () => {
    getData('actType', data.search_act, data)
  }

  async function submit(e) {
    e.preventDefault()

    // for DC and HC(containing capthca)
    if (
      data.court == 'DC' ||
      (data.court == 'HC' && data.courtCode == 'KARHC') ||
      (data.court == 'HC' && data.courtCode == 'ORIHC') ||
      (data.court == 'HC' && data.courtCode == 'CHAHC') ||
      (data.court == 'HC' && data.courtCode == 'UTTHC') ||
      (data.court == 'HC' && data.courtCode == 'TRIHC') ||
      (data.court == 'HC' && data.courtCode == 'MANHC') ||
      (data.court == 'HC' && data.courtCode == 'GUWHC')
    ) {
      console.log(data, 'Data Inserted', Config)
      axios
        .post(
          `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/captcha/` + myUUID,
          {
            uuid: myUUID,
            captcha_response: data.captcha,
          }
        )
        .then(
          (response) => {
            console.log(
              `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/captcha`,
              response.data
            )
          },
          (error) => {
            console.log({ ...error })
          }
        )
    }
    if (data.court == 'SC') {
      console.log(data)
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      }

      try {
        // setLoading(true);

        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_SUPREME_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              caseType: data.caseType,
              case_no: data.case_no,
              year: data.year,
            },
          },
        }).then(
          (response) => {
            console.log('requestSCR', response.data)
            setScraperResponse(response.data)
            setCaseObject(response.data.caseDetails)
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } catch (error) {
        console.log(error)
      }
    }

    // for HC non-captcha only and avoid REST by negation condition
    if (
      (data.court == 'HC' && data.courtCode != 'KARHC') ||
      (data.court == 'HC' && data.courtCode != 'ORIHC') ||
      (data.court == 'HC' && data.courtCode != 'CHAHC') ||
      (data.court == 'HC' && data.courtCode != 'UTTHC') ||
      (data.court == 'HC' && data.courtCode != 'TRIHC') ||
      (data.court == 'HC' && data.courtCode != 'MANHC') ||
      (data.court == 'HC' && data.courtCode != 'GUWHC')
    ) {
      console.log(data)
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      }
      try {
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_HIGHCOURT_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              caseType: data.caseType,
              case_no: data.case_no,
              year: data.year,
              courtCode: data.courtCode,
              karnatakaBenchField: data.karnatakaBenchField,
            },
          },
        }).then(
          (response) => {
            console.log('requestSCR', response.data)
            setScraperResponse(response.data)
            setCaseObject(response.data.caseDetails)
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } catch (error) {
        console.log(error)
      }
    }
  }

  function handle(e) {
    console.log(data)
    const value =
      e.target.type === 'checkbox' ? e.target.checked : e.target.value
    setData({ ...data, [e.target.name]: value })
    // let tmpUUID = uuidv4()
    switch (e.target.name) {
      case 'court':
      case 'state':
      case 'city':
      case 'courtCode':
      case 'actType':
      case 'casetype':
        console.log((e.target.name, value))
        if (value && value != -1 && value != '') {
          getData(e.target.name, value, data)
        }
        break
      case 'searchBy':
        if (value == 'CSFIRNumber') {
          console.log((e.target.name, value))
          if (value && value != -1 && value != '') {
            getData(e.target.name, value, data)
          }
        }
        break

      default:
        break
    }
    console.log('e.target.name ==> ', e.target.name)
    console.log('e.target.text ==> ', value)
  }
  function getData(reqType, reqData, reactStateObject) {
    let requestData = {}
    if (reqType == 'court') {
      requestData = { ...requestData, courtCode: reqData }
    }

    if (reqType == 'state') {
      requestData = {
        ...requestData,
        stateCode: reqData,
      }
    }

    if (reqType == 'city') {
      requestData = {
        ...requestData,
        stateCode: data.state,
        cityCode: reqData,
      }
    }

    if (reqType == 'courtCode' && data.court == 'DC') {
      let courtComplexCode = null
      courtcodedata.map((c) => {
        console.log(c, reqData)
        if (reqData == c.code) {
          courtComplexCode = c.complexCode
          console.log('', c.complexCode)
          setComplexCode(c.complexCode)
        }
      })
      requestData = {
        ...requestData,
        stateCode: data.state,
        cityCode: data.city,
        courtCode: reqData,
        courtComplexCode,
        court: data.court,
      }
    }

    if (reqType == 'searchBy' && data.court == 'DC') {
      console.log(`reqType == 'searchBy'`)

      requestData = {
        ...requestData,
        // stateCode: data.state,
        // cityCode: data.city,
        // courtCode: data.courtCode,
        // courtComplexCode: complexCode,
        stateCode: data.state,
        searchBy: data.searchBy,
        stateCode: data.state,
        cityCode: data.city,
        courtCode: data.courtCode,
        courtComplexCode: complexCode,
        caseType: data.caseType,
        year: data.year,
        petitioner_respondent: data.petitioner_respondent,
        status: data.status,
        case_no: data.case_no,
        policeStation: data.policeStation,
        court: data.court,
        firNumber: data.firNumber,
        case_num: data.case_num,
      }
    }

    if (reqType == 'actType' && data.court == 'DC') {
      console.log(`reqType == 'actType'`)

      requestData = {
        ...requestData,
        // stateCode: data.state,
        // cityCode: data.city,
        // courtCode: data.courtCode,
        // courtComplexCode: complexCode,
        stateCode: data.state,
        searchBy: data.searchBy,
        stateCode: data.state,
        cityCode: data.city,
        courtCode: data.courtCode,
        courtComplexCode: complexCode,
        caseType: data.caseType,
        year: data.year,
        petitioner_respondent: data.petitioner_respondent,
        status: data.status,
        case_no: data.case_no,
        policeStation: data.policeStation,
        court: data.court,
        firNumber: data.firNumber,
        case_num: data.case_num,
        //---
        search_act: data.search_act,
        actcode: data.actcode,
        under_sec: data.under_sec,
      }
    }

    if (reqType == 'courtCode' && data.court == 'HC') {
      requestData = {
        ...requestData,
        court: data.court,
        courtCode: reqData,
      }
    }

    axios
      .post(`${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`, {
        reqType,
        requestData,
        reactStateObject,
        tmpUUID: myUUID,
      })
      .then(
        (response) => {
          console.log('UNIFIED SEARCH', response.data)
          if (reqType == 'court') {
            setstatedata(response.data.states)
            console.log(response)
          }

          if (reqType == 'searchBy') {
            setFirData(response.data.fir)
            console.log(response)
          }

          if (reqType == 'actType') {
            setActData(response.data.actType)
            console.log(response)
          }

          if (reqType == 'state') {
            setcitydata(response.data.cities)
          }

          if (reqType == 'city') {
            setcourtcodedata(response.data.courts)
          }

          console.log('====================================')
          console.log(
            'response.data.caseTypes',
            response.data.caseTypes,
            reqType,
            reqData
          )
          console.log('====================================')
          if (reqType == 'court' && reqData == 'SC') {
            setcasetypedata(response.data.caseTypes)
          }

          if (reqType == 'court' && reqData == 'HC') {
            setcourtcodedata(response.data.courts)
          }
          if (reqType == 'courtCode' && data.court == 'DC') {
            setcasetypedata(response.data.caseTypes)
          }
          if (reqType == 'courtCode' && data.court == 'HC') {
            setcasetypedata(response.data.caseTypes)
          }
        },
        (error) => {
          console.log({ ...error })
        }
      )
  }
  React.useEffect(() => {
    let tmpUUID = uuidv4()
    setMyUUID(tmpUUID)
    setCaptcha()
  }, [])

  //imli
  React.useEffect(async () => {
    if (
      (data.status != '-1' && actData.length > 0) ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'DC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'KARHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'ORIHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'CHAHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'UTTHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'TRIHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'MANHC') ||
      (data.year !== null &&
        data.caseType !== null &&
        data.case_no !== null &&
        data.year.length == 4 &&
        data.court == 'HC' &&
        data.courtCode == 'GUWHC')
    ) {
      if (data.court == 'DC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_ECOURT_SEARCH',
            tmpUUID: myUUID,

            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'KARHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_KARNATAKA_SEARCH',
            tmpUUID: myUUID,

            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'ORIHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_ORISSA_SEARCH',
            tmpUUID: myUUID,

            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'CHAHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_CHATTISGARH_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'UTTHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_UTTARAKHAND_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'TRIHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_TRIPURA_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'MANHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_MANIPUR_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      } else if (data.court == 'HC' && data.courtCode == 'GUWHC') {
        console.log(captcha, 'DISTRICRT courts - captcha value')
        console.log('SEARCH INITIATED  &  CREATE CAPTCHA')
        setCaptcha('captcha_loading')
        setSearchStatus('STAGE_1')
        axios({
          method: 'post',
          url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/search`,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            reqType: 'BEGIN_GUWAHATI_SEARCH',
            tmpUUID: myUUID,
            requestData: {
              stateCode: data.state,
              searchBy: data.searchBy,
              stateCode: data.state,
              cityCode: data.city,
              courtCode: data.courtCode,
              courtComplexCode: complexCode,
              caseType: data.caseType,
              year: data.year,
              petitioner_respondent: data.petitioner_respondent,
              status: data.status,
              case_no: data.case_no,
              policeStation: data.policeStation,
              court: data.court,
              firNumber: data.firNumber,
              case_num: data.case_num,
              search_act: data.search_act,
              actcode: data.actcode,
              under_sec: data.under_sec,
            },
          },
        }).then(
          (response) => {
            console.log('SCRAPED_CASE', response)
            if (response.data.ss !== null) {
              setScraperResponse(response.data)

              setData({
                advocate: '',
                barcode: '',
                captcha: '',
                caseListDate: '',
                caseType: '',
                case_no: '',
                courtCode: '',
                city: '-1',
                court: '',
                courtType: '',
                firNumber: '',
                petitioner_respondent: '',
                policeStation: '',
                searchAdvocateBy: '',
                searchBy: '-1',
                state: '-1',
                statecode: '',
                status: '',
                year: '',
                filingCaseNo: '',
              })
              setMyUUID(myUUID)
              setCaptcha(null)
            }
          },
          (error) => {
            console.log({ ...error })
          }
        )
      }
    }
  }, [data.year, data.case_no, actData, data.status, data.courtCode])
  async function getCaptchaData() {
    console.log('getCaptchaData')
    axios({
      method: 'get',
      url: `${Config.PROTO}://${Config.IP}:${Config.UNIFIED}/captcha/${myUUID}`,
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(
      (response) => {
        console.log('response', response)
        const doc = response.data.doc
        if (doc !== null) {
          setSearchStatus('STAGE_2')
          setCaptcha(doc.captcha_url)
        }
      },
      (error) => {
        console.log({ ...error })
      }
    )
  }
  useEffect(() => {
    console.log('casetypedata', casetypedata, casetypedata.length)
  }, [casetypedata])
  React.useEffect(() => {
    if (searchStatus == 'STAGE_1') {
      var interval = setInterval(() => {
        console.log('inside interval', captchaStatus)
        getCaptchaData()
      }, 1000)

      console.log('inside interval 2', captchaStatus)

      return () => {
        clearInterval(interval)
      }
    }
  }, [searchStatus])

  // ========================================================DC===============================================================

  const StateField = (
    <>
      <select
        name='state'
        class='form-control'
        onChange={handle}
        value={data.state}
      >
        <option value='-1'>Select </option>
        {statedata &&
          statedata.map((st) => {
            return <option value={st.code}>{st.name}</option>
          })}
      </select>
    </>
  )

  const CityField = (
    <>
      <select
        name='city'
        class='form-control'
        onChange={handle}
        value={data.city}
      >
        <option value='-1'>Select </option>

        {citydata &&
          citydata.map((ct) => {
            return <option value={ct.code}>{ct.name}</option>
          })}
      </select>
    </>
  )

  const PoliceStationField = (
    <>
      <select
        name='policeStation'
        class='form-control'
        onChange={handle}
        value={data.policeStation}
      >
        <option value='-1'>Select </option>
        {firData &&
          firData.map((fir) => {
            console.log('fir >> ', fir)
            return <option value={fir.code}>{fir.name}</option>
          })}
      </select>
    </>
  )

  const ActType = (
    <>
      <select
        name='actcode'
        class='form-control'
        onChange={handle}
        value={data.actcode}
      >
        <option value='-1'>Select </option>
        {actData &&
          actData.map((actType) => {
            console.log('actType >> ', actType)
            return <option value={actType.code}>{actType.name}</option>
          })}
      </select>
    </>
  )

  const FirNumberField = (
    <>
      <input
        name='firNumber'
        type='text'
        className='form-control'
        placeholder='FIR Number'
        onChange={handle}
        value={data.firNumber}
      />
    </>
  )
  const SearchByField = (
    <>
      <select
        name='searchBy'
        class='form-control'
        onChange={handle}
        value={data.searchBy}
      >
        <option value='-1'>Select </option>
        <option value='CScaseNumber'>Case Number</option>
        <option value='CSFIRNumber'>FIR Number</option>
        <option value='CSpartyName'>Petitioner/Respondent</option>
        {/* <option value='Advocate'>Advocate</option> */}
        <option value='CSact'>Act</option>
        <option value='CSfilingNumber'>Filing Number</option>
        <option value='CScaseType'>Case Type</option>
      </select>
    </>
  )

  const CourtTypeField = (
    <>
      <select
        name='courtType'
        class='form-control'
        onChange={handle}
        value={data.courtType}
      >
        <option value='-1'>Select </option>
        <option value='Court Complex'>Court Complex</option>
        <option value='Court Establishment'>Court Establishment</option>
      </select>
    </>
  )
  const CourtField = (
    <>
      <select
        name='court'
        class='form-control'
        onChange={handle}
        value={data.court}
      >
        <option value='-1'>Select </option>
        <option value='DC'>District Court</option>
        <option value='HC'>High Court</option>
        <option value='SC'>Supreme Court</option>
      </select>
    </>
  )

  const CourtCodeField = (
    <>
      <select
        name='courtCode'
        class='form-control'
        onChange={handle}
        value={data.courtCode}
      >
        <option value='-1'>Select </option>

        {courtcodedata &&
          courtcodedata.map((cc) => {
            return <option value={cc.code}>{cc.name}</option>
          })}
      </select>
    </>
  )

  const karnatakaBenchField = (
    <>
      <select
        name='karBenchField'
        class='form-control'
        onChange={handle}
        value={data.karnatakaBenchField}
      >
        <option value='-1'>Select </option>
        <option value='B'>Bengaluru Bench</option>
        <option value='D'>Dharwad Bench</option>
        <option value='K'>Kalaburagi Bench</option>
      </select>
    </>
  )

  console.log('casetypedataXX', casetypedata)
  const CaseTypeField = (
    <>
      <select
        name='caseType'
        class='form-control'
        onChange={handle}
        value={data.caseType}
      >
        <option value='-1'>Select case type </option>

        {casetypedata &&
          casetypedata.map((cty) => {
            console.log('cities', cty)
            return <option value={cty.code}>{cty.name}</option>
          })}
      </select>
    </>
  )

  const CaseNumberField = (
    <>
      <input
        name='case_no'
        type='text'
        className='form-control'
        placeholder='Case Number'
        onChange={handle}
        value={data.case_no}
      />
    </>
  )

  const YearField = (
    <>
      <input
        name='year'
        type='text'
        className='form-control'
        placeholder='Year'
        onChange={handle}
        value={data.year}
      />
    </>
  )
  const SearchAct = (
    <>
      <input
        name='search_act'
        type='text'
        className='form-control'
        placeholder='Search Type'
        onChange={handle}
        value={data.search_act}
      />
    </>
  )
  const UnderSection = (
    <>
      <input
        name='under_sec'
        type='text'
        className='form-control'
        placeholder='Under Section'
        onChange={handle}
        value={data.under_sec}
      />
    </>
  )

  const FilingNumber = (
    <>
      <input
        name='case_num'
        type='text'
        className='form-control'
        placeholder='filing Number'
        onChange={handle}
        value={data.case_num}
      />
    </>
  )

  const CaptchaField = (
    <>
      <input
        name='captcha'
        type='text'
        className='form-control'
        placeholder='Enter Captcha'
        onChange={handle}
        value={data.captcha}
      />
    </>
  )

  const StatusField = (
    <>
      <select
        name='status'
        class='form-control'
        onChange={handle}
        value={data.status}
      >
        <option value='-1'>Select </option>
        <option value='Pending'>Pending</option>
        <option value='Disposed'>Disposed</option>
        <option value='Both'>Both</option>
      </select>
    </>
  )

  const PetitionerRespondentField = (
    <>
      <input
        name='petitioner_respondent'
        type='text'
        className='form-control'
        placeholder='Party Name'
        onChange={handle}
        value={data.petitioner_respondent}
      />
    </>
  )

  const RadioSearchAdvocateAdvocateField = (
    <>
      <input
        type='radio'
        name='searchAdvocateBy'
        value='Advocate'
        checked={data.searchAdvocateBy === 'Advocate'}
        onChange={handle}
      />
    </>
  )

  const RadioSearchAdvocateBarCodeField = (
    <>
      <input
        type='radio'
        name='searchAdvocateBy'
        value='Bar Code'
        checked={data.searchAdvocateBy === 'Bar Code'}
        onChange={handle}
      />
    </>
  )

  const RadioSearchAdvocateDateCaseListField = (
    <>
      <input
        type='radio'
        name='searchAdvocateBy'
        value='Date Case List'
        checked={data.searchAdvocateBy === 'Date Case List'}
        onChange={handle}
      />
    </>
  )

  const AdvocateField = (
    <>
      <input
        name='advocate'
        type='text'
        className='form-control'
        placeholder='Advocate'
        onChange={handle}
        value={data.advocate}
      />
    </>
  )

  const StateCodeField = (
    <>
      <input
        name='statecode'
        type='text'
        className='form-control'
        placeholder='State Code'
        onChange={handle}
        value={data.statecode}
      />
    </>
  )

  const BarcodeField = (
    <>
      <input
        name='barcode'
        type='text'
        className='form-control'
        placeholder='Barcode'
        onChange={handle}
        value={data.barcode}
      />
    </>
  )

  const CaseListDateField = (
    <>
      <input
        name='caseListDate'
        type='date'
        className='form-control'
        placeholder='Case List Date'
        onChange={handle}
        value={data.caseListDate}
      />
    </>
  )

  return (
    <>
      <Layout>
        <div className='container'>
          <div className='row'>
            <div className='card'>
              <div className='card-body'>
                <form
                  onSubmit={(e) => submit(e)}
                  style={{ padding: '0rem 5rem' }}
                >
                  <div className='row'>
                    <div className='col-md-6'>
                      <label className='form-label'>Court </label>
                    </div>
                    <div className='col-md-6'>{CourtField}</div>
                    {data.court === 'DC' && (
                      <>
                        <div className='col-md-6'>
                          <label className='form-label'>State </label>
                        </div>
                        <div className='col-md-6'>{StateField}</div>
                        {data.state !== '-1' && (
                          <>
                            <div className='col-md-6'>
                              <label className='form-label'>City </label>
                            </div>
                            <div className='col-md-6'>{CityField}</div>
                            <div className='col-md-6'>
                              <label className='form-label'>Court Code</label>
                            </div>
                            <div className='col-md-6'>{CourtCodeField}</div>
                            {data.city !== '-1' && (
                              <>
                                <div className='col-md-6'>
                                  <label className='form-label'>Sort By </label>
                                </div>
                                <div className='col-md-6'>{SearchByField}</div>
                                {/* ===============================Case Number========================================== */}
                                {data.searchBy === 'CScaseNumber' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Court Type
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CourtTypeField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Case Type
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaseTypeField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Case Number
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaseNumberField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>Year</label>
                                    </div>
                                    <div className='col-md-6'>{YearField}</div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {CaptchaField}
                                    </div>
                                  </>
                                )}
                                {/* =====================================FIR================================================== */}
                                {data.searchBy === 'CSFIRNumber' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Police Station
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {PoliceStationField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        FIR Number
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {FirNumberField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Status
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {StatusField}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>Year</label>
                                    </div>
                                    <div className='col-md-6'>{YearField}</div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {CaptchaField}
                                    </div>
                                  </>
                                )}
                                {/* =======================================Petitioner================================================ */}
                                {data.searchBy === 'CSpartyName' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Party Name
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {PetitionerRespondentField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Status
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {StatusField}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>Year</label>
                                    </div>
                                    <div className='col-md-6'>{YearField}</div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {CaptchaField}
                                    </div>
                                  </>
                                )}
                                {/* ==========================================Advocate============================================= */}
                                {data.searchBy === 'Advocate' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Search By
                                      </label>
                                    </div>
                                    <div
                                      className='col-md-6'
                                      style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'space-evenly',
                                      }}
                                    >
                                      <label>
                                        {RadioSearchAdvocateAdvocateField}
                                        <span style={{ paddingLeft: 5 }}>
                                          Advocate
                                        </span>
                                      </label>
                                      <label>
                                        {RadioSearchAdvocateBarCodeField}
                                        <span style={{ paddingLeft: 5 }}>
                                          Bar Code
                                        </span>
                                      </label>
                                      <label>
                                        {RadioSearchAdvocateDateCaseListField}
                                        <span style={{ paddingLeft: 5 }}>
                                          Date Case List
                                        </span>
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Advocate
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {AdvocateField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Advocate Bar Code
                                      </label>
                                    </div>
                                    <div
                                      className='col-md-6'
                                      style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                      }}
                                    >
                                      {StateCodeField}
                                      {BarcodeField}
                                      {YearField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Case List Date
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaseListDateField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Status
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {StatusField}
                                    </div>
                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaptchaField}
                                    </div>
                                  </>
                                )}
                                {/* ==========================================Act============================================= */}
                                {data.searchBy === 'CSact' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Search Act
                                      </label>
                                    </div>
                                    <span
                                      style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        width: '40%',
                                      }}
                                    >
                                      <div className='col-md-6'>
                                        {SearchAct}
                                      </div>
                                      <span>
                                        <button
                                          onClick={getActTypeDropdown}
                                          style={{
                                            marginTop: '9px',
                                            marginRight: '9rem',
                                            width: '7vh',
                                            height: '5vh',
                                          }}
                                        >
                                          {/* Q */}
                                          <FcSearch
                                            style={{
                                              // color: 'white',
                                              height: '3vh',
                                              width: '5vh',
                                              size: 'auto',
                                            }}
                                          />
                                        </button>
                                      </span>
                                    </span>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Act Type
                                      </label>
                                    </div>
                                    <div className='col-md-6'>{ActType}</div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Under Section
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {UnderSection}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Status
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {StatusField}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaptchaField}
                                      {/* Need to change */}
                                    </div>
                                  </>
                                )}
                                {/* ==========================================Filing Number============================================= */}{' '}
                                {data.searchBy === 'CSfilingNumber' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Filing Number
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {FilingNumber}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>Year</label>
                                    </div>
                                    <div className='col-md-6'>{YearField}</div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>

                                    <div className='col-md-6'>
                                      {CaptchaField}
                                      {/* Need to change */}
                                    </div>
                                  </>
                                )}
                                {/* ==========================================Case Type============================================= */}{' '}
                                {data.searchBy === 'CScaseType' && (
                                  <>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Case Type
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaseTypeField}
                                    </div>

                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Status
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {StatusField}
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>Year</label>
                                    </div>
                                    <div className='col-md-6'>{YearField}</div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Captcha
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {captcha == 'captcha_loading' ? (
                                        <img
                                          src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                          style={{ height: 80, width: 80 }}
                                        />
                                      ) : (
                                        <img
                                          src={captcha}
                                          style={{ height: 100, width: 160 }}
                                        />
                                      )}
                                      <img
                                        // src='Image/Layout/profile.jpeg'
                                        src={refresh}
                                        alt='image not found'
                                        style={{
                                          width: '2.5rem',
                                          height: '2rem',
                                        }}
                                      ></img>
                                    </div>
                                    <div className='col-md-6'>
                                      <label className='form-label'>
                                        Enter Captcha
                                      </label>
                                    </div>
                                    <div className='col-md-6'>
                                      {CaptchaField}
                                    </div>
                                  </>
                                )}
                              </>
                            )}
                          </>
                        )}
                      </>
                    )}

                    {data.court === 'SC' && (
                      <>
                        <div className='col-md-6'>
                          <label className='form-label'>Case Type</label>
                        </div>
                        <div className='col-md-6'>{CaseTypeField}</div>
                        <div className='col-md-6'>
                          <label className='form-label'>Case Number</label>
                        </div>
                        <div className='col-md-6'>{CaseNumberField}</div>

                        <div className='col-md-6'>
                          <label className='form-label'>Year</label>
                        </div>
                        <div className='col-md-6'>{YearField}</div>
                      </>
                    )}
                    {data.court === 'HC' && (
                      <>
                        <div className='col-md-6'>
                          <label className='form-label'>Court Code</label>
                        </div>
                        <div className='col-md-6'>{CourtCodeField}</div>
                        {/* //ola1 */}
                        {data.courtCode == 'KARHC' && (
                          <>
                            <div className='col-md-6'>
                              <label className='form-label'>Select Bench</label>
                            </div>
                            <div className='col-md-6'>
                              {karnatakaBenchField}
                            </div>
                          </>
                        )}

                        <div className='col-md-6'>
                          <label className='form-label'>Case Type</label>
                        </div>
                        <div className='col-md-6'>{CaseTypeField}</div>
                        <div className='col-md-6'>
                          <label className='form-label'>Case Number</label>
                        </div>
                        <div className='col-md-6'>{CaseNumberField}</div>
                        <div className='col-md-6'>
                          <label className='form-label'>Year</label>
                        </div>
                        <div className='col-md-6'>{YearField}</div>
                        {data.courtCode == 'KARHC' ||
                        data.courtCode == 'ORIHC' ||
                        data.courtCode == 'CHAHC' ||
                        data.courtCode == 'UTTHC' ||
                        data.courtCode == 'TRIHC' ||
                        data.courtCode == 'MANHC' ||
                        data.courtCode == 'GUWHC' ? (
                          <>
                            <div className='col-md-6'>
                              <label className='form-label'>Captcha</label>
                            </div>
                            <div className='col-md-6'>
                              {captcha == 'captcha_loading' ? (
                                <img
                                  src='https://facturas.intelisis-solutions.com/assets/imagenes/ajax-loading.gif'
                                  style={{ height: 80, width: 80 }}
                                />
                              ) : (
                                <img
                                  src={captcha}
                                  style={{ height: 100, width: 160 }}
                                />
                              )}
                              <img
                                // src='Image/Layout/profile.jpeg'
                                src={refresh}
                                alt='image not found'
                                style={{
                                  width: '2.5rem',
                                  height: '2rem',
                                }}
                              ></img>
                            </div>
                            <div className='col-md-6'>
                              <label className='form-label'>
                                Enter Captcha
                              </label>
                            </div>
                            <div className='col-md-6'>{CaptchaField}</div>
                          </>
                        ) : (
                          <></>
                        )}
                      </>
                    )}
                  </div>

                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    {/* <div className='col-lg-2'> */}
                    <button
                      className='button '
                      onClick={() => {
                        console.log('hi')
                      }}
                      // style={{ margin: '1rem' }}
                    >
                      Submit
                    </button>
                    {/* </div> */}
                    <div className='col-lg-3'>
                      <button
                        className='button '
                        // style={{ margin: '1rem' }}
                        onClick={() => resethandle()}
                      >
                        Reset
                      </button>
                    </div>
                  </div>
                </form>
              </div>

              {scraperResponse && scraperResponse ? (
                <District
                  scraperResponse={scraperResponse}
                  court={data.court}
                  data={caseObject}
                />
              ) : null}
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
