import React, { useState, useEffect, useReducer } from 'react'
import { IoDownloadOutline, IoEllipsisHorizontal } from 'react-icons/io5'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { RiCalendar2Fill } from 'react-icons/ri'
import DatePicker from 'react-datepicker'
import './Ecourts.css'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'

export const FirstResponse = (props) => {
  const { scraperResponse } = props

  console.log('scraperResponse >> ', scraperResponse)

  return (
    <>
      <div className='container'>
        <div className='row'>
          <div className='card'>
            <table
              class='table table-hover'
              style={{ display: 'block', width: '100%', overflowY: 'auto' }}
            >
              {scraperResponse &&
                scraperResponse.reqBody.reqType == 'BEGIN_ECOURT_SEARCH' && (
                  <>
                    <thead>
                      <tr>
                        <th>Sr No</th>
                        <th>Case Type / Case Number / Case Year</th>
                        <th>Petitioner versus Respondent</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {scraperResponse &&
                        JSON.parse(scraperResponse.caseDetails).map((d, i) => {
                          i = i + 1
                          // console.log('d.case_no :', d.pet_name)
                          return (
                            <tr>
                              <td>{i}</td>
                              <td>
                                {d.type_name}/{d.case_no}/{d.case_year}
                              </td>
                              <td>
                                {typeof d.pet_name != 'undefined' &&
                                d.pet_name == '' ? (
                                  <>{`Extra party : ${
                                    typeof d.extra_party != 'undefined' &&
                                    d.extra_party
                                  }`}</>
                                ) : (
                                  <>
                                    {typeof d.pet_name != 'undefined' &&
                                      d.pet_name}
                                    /{' '}
                                    {typeof d.res_name != 'undefined' &&
                                      d.res_name}
                                  </>
                                )}
                              </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/viewCaseDetails/${d.cino}`,
                                    state: {
                                      scraperResponse,
                                      cino: d.cino,
                                      case_no: d.case_no,
                                      case_year: d.case_year,
                                    },
                                  }}
                                >
                                  <button
                                    style={{
                                      paddingLeft: '35px',
                                      paddingTop: '10px',
                                      paddingBottom: '10px',
                                      paddingRight: '35px',
                                      fontSize: '18px',
                                      margin: '10px',
                                      borderRadius: '10px',
                                      backgroundColor: 'green',
                                      color: 'white',
                                    }}
                                  >
                                    Continue
                                  </button>
                                </Link>
                                <Link>
                                  <button
                                    style={{
                                      paddingLeft: '20px',
                                      paddingTop: '10px',
                                      paddingBottom: '10px',
                                      paddingRight: '20px',
                                      fontSize: '18px',
                                      margin: '10px',
                                      borderRadius: '10px',
                                      backgroundColor: '#8425DA',
                                      color: 'white',
                                    }}
                                  >
                                    Search Again
                                  </button>
                                </Link>
                              </td>
                            </tr>
                          )
                        })}
                    </tbody>
                  </>
                )}

              {scraperResponse &&
                scraperResponse.reqBody.reqType != 'BEGIN_ECOURT_SEARCH' && (
                  <>
                    <thead>
                      <tr>
                        <th>Sr No</th>
                        <th>Case Type / Case Number / Case Year</th>
                        <th>Petitioner versus Respondent</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {scraperResponse && (
                        <>
                          <td>1</td>
                          <td>{scraperResponse.caseDetails.caseTypeNoYear}</td>
                          <td>{scraperResponse.caseDetails.parties}</td>
                          <td>
                            <Link
                              to={`/viewCaseDetails/${scraperResponse.uuid}`}
                            >
                              <button
                                style={{
                                  paddingLeft: '35px',
                                  paddingTop: '10px',
                                  paddingBottom: '10px',
                                  paddingRight: '35px',
                                  fontSize: '18px',
                                  margin: '10px',
                                  borderRadius: '10px',
                                  backgroundColor: 'green',
                                  color: 'white',
                                }}
                              >
                                Continue
                              </button>
                            </Link>
                            <Link>
                              <button
                                style={{
                                  paddingLeft: '20px',
                                  paddingTop: '10px',
                                  paddingBottom: '10px',
                                  paddingRight: '20px',
                                  fontSize: '18px',
                                  margin: '10px',
                                  borderRadius: '10px',
                                  backgroundColor: '#8425DA',
                                  color: 'white',
                                }}
                              >
                                Search Again
                              </button>
                            </Link>
                          </td>
                        </>
                      )}
                    </tbody>
                  </>
                )}
            </table>
          </div>
        </div>
      </div>
    </>
  )
}
export default FirstResponse
