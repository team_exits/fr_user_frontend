import React, { useState, useEffect, useReducer } from 'react'
import { IoDownloadOutline, IoEllipsisHorizontal } from 'react-icons/io5'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { RiCalendar2Fill } from 'react-icons/ri'
import DatePicker from 'react-datepicker'
import './Ecourts.css'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'
import Layout from '../../Component/Layout/Layout'
import Config from '../../../src/Utils/Axios/Config'

export const Supreme = (props) => {
  const { ScData } = props
  console.log('opopo >> ', ScData)
  return (
    <>
      <div className='container'>
        <div className='row'>
          <div className='card'>
            {ScData != null && (
              <>
                <div className='container' style={{ backgroundColor: '#fff' }}>
                  <p></p>
                  <div>
                    <p style={{}}>Case Details</p>
                    <table
                      className='table table-bordered'
                      style={{ border: '1px solid black' }}
                    >
                      <tbody>
                        <tr>
                          <th>Diary No.</th>
                          <td>{ScData.caseDetails.diaryNo}</td>
                        </tr>
                        <tr>
                          <th>Case No.</th>
                          <td>{ScData.caseDetails.caseNo}</td>
                        </tr>
                        <tr>
                          <th> Present/Last Listed On</th>
                          <td> {ScData.caseDetails.presentLastListedOn}</td>
                        </tr>
                        <tr>
                          <th> Status/Stage</th>
                          <td style={{ color: 'red' }}>
                            {ScData.caseDetails.statusStage}
                          </td>
                        </tr>
                        <tr>
                          <th> Disp.Type</th>
                          <td style={{ color: 'red' }}>
                            {ScData.caseDetails.dispType}
                          </td>
                        </tr>
                        <tr>
                          <th> Category</th>
                          <td> {ScData.caseDetails.category}</td>
                        </tr>
                        <tr>
                          <th> Act</th>
                          <td> {ScData.caseDetails.caseNo}</td>
                        </tr>
                        <tr>
                          <th> Petitioner(s)</th>
                          <td> {ScData.caseDetails.petitionerS}</td>
                        </tr>
                        <tr>
                          <th> Respondent(s)</th>
                          <td> {ScData.caseDetails.caseNo}</td>
                        </tr>
                        <tr>
                          <th> Pet. Advocate(s)</th>
                          <td> {ScData.caseDetails.petAdvocate}</td>
                        </tr>
                        <tr>
                          <th> Resp. Advocate(s)</th>
                          <td> {ScData.caseDetails.respAdvocate}</td>
                        </tr>
                        <tr>
                          <th> U/Section</th>
                          <td> {ScData.caseDetails.petAdvocate}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
export default Supreme
