import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteDataById, getAllData } from '../../Utils/Axios/CommonAxios'
import Config from '../../Utils/Axios/Config'
import ExtendedDataTables from '../../Component/ExtendedDataTables/ExtendedDataTables'
import Layout from '../../Component/Layout/Layout'
import { HiOutlineDocumentText, HiOutlineTicket } from 'react-icons/hi'

export default function Orders(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props
  const navigation = props
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const login = useSelector((state) => state.login)
  const { token } = login
  console.log('TokenHai', token)

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <ExtendedDataTables
            navigation={navigation}
            editRoute='/order'
            // deleteRoute="/delete_FORM_ROUTE"
            cols={{
              // _id: "UID",
              cino: 'CNR Number',
              // date: 'Date',
              // content: 'Content',
              // name: 'Name',
              // order_type: 'Order Type',
              // order_no: 'Order No',
              // pdf_path: 'Pdf Path',
              orderDetails: 'ORDER DETAILS',
              orderDetailsPdfLink: (obj) => {
                console.log('obj >> ', obj)
                return {
                  colName: 'VIEW PDF',
                  return: obj.orderDetailsPdfLink
                    ? obj.interimOrder.map((dd, idx) => {
                        return (
                          <div
                            style={{
                              display: 'flex',
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              color: 'black',
                            }}
                          >
                            <div>
                              {/* <HiOutlineDocumentText
                                style={{
                                  color: 'blue',
                                  height: '35px',
                                  width: '35px',
                                }}
                              /> */}
                              <a target='_blank' href={dd.orderDetailsPdfLink}>
                                {dd.orderDate}
                              </a>
                            </div>
                          </div>
                        )
                      })
                    : null,
                }
              },
              // orderDetailsPdfLink: (obj) => {
              //   console.log('obj >> ', obj)
              //   return {
              //     colName: 'VIEW PDF',
              //     return: obj.orderDetailsPdfLink ? (
              //       <div
              //         style={{
              //           display: 'flex',
              //           justifyContent: 'center',
              //           alignItems: 'center',
              //         }}
              //       >
              //         <a target='_blank' href={obj.orderDetailsPdfLink}>
              //           <HiOutlineDocumentText
              //             style={{
              //               color: 'blue',
              //               height: '35px',
              //               width: '35px',
              //             }}
              //           />
              //         </a>
              //       </div>
              //     ) : null,
              //   }
              // },
            }}
            viewApiType='GET'
            viewApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
            editApiType='PUT'
            editApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
            deleteApiType='DELETE'
            deleteApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
            headers={{
              Authorization: 'Bearer ' + token,
            }}
          />
        </div>
      </div>
    </Layout>
  )
}
