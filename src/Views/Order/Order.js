import React from 'react'
import FormGenerator from '../../Component/Layout/Formake/FormGenerator'
import Formake from '../../Component/Layout/Formake/Formake'
import { useParams } from 'react-router-dom'
import '../../Component/Layout/Formake/Formake.css'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'

export default function Order() {
  let { qid } = useParams()

  return (
    <>
      <Layout>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='Regular-form'>
                <Formake
                  useAxios={true}
                  edit={qid ? true : false}
                  editId={qid}
                  title={qid ? 'Edit Order' : 'Add Order'}
                  createApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
                  createType='POST'
                  editApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
                  editType='GET'
                  updateApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
                  updateType='PUT'
                  deleteApi={`http://${Config.IP}:${Config.ORDER_PORT}/api/order`}
                  deleteType='DELETE'
                  auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                  headers={{}}
                  params={{}}
                  initialState={{
                    level: null,
                    questionType: null,
                    question: null,
                    answer: null,
                    points: null,
                    // levels: '60deacc31c0b9a091c2cb2f4',
                  }}
                  // colwidths in number totaling upto 12 e.g. 8+4= 12
                  cols={[6, 6]}
                  spacing={3}
                  isSubmitting={false}
                  //used for default change handler functions having switch case inside the to set proper setState
                  updatedValues={(formValues) =>
                    console.log('formValues UPDATING --> ', formValues)
                  }
                  onChangeHandler={(name, value) => {
                    console.log('formValuesx UPDATING --> ', name, value)
                  }}
                  onSubmit={(values) =>
                    console.log('formValues SUBMITTING', values)
                  }
                  //                 orderNo: { type: String },
                  // orderDate: { type: String },
                  // orderDetails: { type: String },
                  // orderDetailsPdfLink: { type: String },
                  fields={[
                    [
                      {
                        type: 'text',
                        name: 'cino',
                        label: 'Cino',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'orderNo',
                        label: 'Order No',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'orderDate',
                        label: 'Order Date',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'orderDetails',
                        label: 'Order Details',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'orderDetailsPdfLink',
                        label: 'Order Pdf Link',
                        props: {},
                      },
                    ],
                  ]}
                  /*                
                  fields={[
                    [
                      {
                        type: 'select',
                        name: 'court',
                        label: 'Court',
                        props: {
                          options: [{ CourtComplex: 'courtcomplex' }],
                        },
                      },
                      {
                        type: 'text',
                        name: 'name',
                        label: 'Name',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'order_no',
                        label: 'Order No',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'date',
                        label: 'Date',
                        // props: {},
                      },
                      {
                        type: 'select',
                        name: 'order_type',
                        label: 'Order Type',
                        props: {
                          options: [{ Null: 'null', Null: 'null' }],
                        },
                      },
                      {
                        type: 'pdf_path',
                        name: 'pdf_path',
                        label: 'Pdf Path',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'content',
                        label: 'Content',
                        // props: {},
                      },
                      // {
                      //   type: "text",
                      //   name: "floor",
                      //   label: "Floor",
                      //   // props: {},
                      // },
                    ],
                  ]}
                  */
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
