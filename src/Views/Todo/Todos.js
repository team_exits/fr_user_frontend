import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteDataById, getAllData } from '../../Utils/Axios/CommonAxios'
import Config from '../../Utils/Axios/Config'
import ExtendedDataTables from '../../Component/ExtendedDataTables/ExtendedDataTables'
import Layout from '../../Component/Layout/Layout'

export default function Todos(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props
  const navigation = props
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const login = useSelector((state) => state.login)
  const { token } = login
  console.log('TokenHai', token)

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <ExtendedDataTables
            navigation={navigation}
            editRoute='/todo'
            // deleteRoute="/delete_FORM_ROUTE"
            cols={{
              // _id: "UID",
              description: null,
              to: null,
              relate_to: null,
              from: null,
              assign_to: null,
            }}
            viewApiType='GET'
            viewApi={`http://${Config.IP}:${Config.TODO_PORT}/api/todo`}
            editApiType='PUT'
            editApi={`http://${Config.IP}:${Config.TODO_PORT}/api/todo`}
            deleteApiType='DELETE'
            deleteApi={`http://${Config.IP}:${Config.TODO_PORT}/api/todo`}
            headers={{
              Authorization: 'Bearer ' + token,
            }}
          />
        </div>
      </div>
    </Layout>
  )
}
