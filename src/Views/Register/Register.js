import React, { useEffect, useState } from 'react'
import { Dropdown } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { setLogin } from '../../Redux/Slice/loginSlice'
import axios from 'axios'
import Config from '../../Utils/Axios/Config'

export default function Register() {
  const dispatch = useDispatch()

  const [formData, setFormData] = useState({
    name: '',
    phone: '',
    email: '',
    password: '',
    gender: '',
    username: '',
    roles: 'user',
  })

  useEffect(() => {
    setFormData({
      name: '',
      phone: '',
      email: '',
      password: '',
      gender: '',
      username: '',
      roles: 'user',
    })

    dispatch(
      setLogin({
        login: null,
        loading: false,
        isLogged: false,
        token: null,
        user: {},
        error: null,
        selected: 0,
      })
    )
  }, [])
  function handleChange(evt) {
    const value =
      evt.target.type === 'checkbox' ? evt.target.checked : evt.target.value
    setFormData({
      ...formData,
      [evt.target.name]: value,
    })
    console.log(formData)
  }

  const register = async (e) => {
    e.preventDefault()
    console.log(formData)

    if (
      formData.name != '' ||
      formData.email != '' ||
      formData.password != ''
    ) {
      try {
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        }

        const { data } = await axios.post(
          `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/auth/signup`,
          {
            username: formData.username,
            password: formData.password,
            name: formData.name,
            email: formData.email,
            roles: 'user',
          },
          config
        )

        // console.log(data);

        if (data.success) {
          // dispatch(setLogin({ isLogged: true, login: {formData} }))
          alert('Registered Successfully...')
          setFormData({
            name: '',
            phone: '',
            email: '',
            password: '',
            gender: '',
            username: '',
            roles: 'user',
          })
        } else {
          dispatch(setLogin({ isLogged: false, login: formData }))
        }
      } catch (err) {
        console.log(err)
        if (err == 'Error: Request failed with status code 404') {
          alert('User Not Found')
        }
      }
    } else {
      alert('Please fill all fields ')
    }
  }

  return (
    <>
      <div
        style={{
          flex: '1',
          flexDirection: 'column',
          justifyContent: 'center',
          minHeight: '100vh',
          alignItems: 'center',
        }}
      >
        <div className='mask d-flex align-items-center h-100 gradient-custom-3'>
          <div className='container h-100'>
            <div className='row d-flex justify-content-center align-items-center h-100'>
              <div className='col-12 col-md-9 col-lg-7 col-xl-6'>
                <div className='card' style={{ borderRadius: '15px' }}>
                  <div className='card-body p-5'>
                    <h2 className='text-uppercase text-center mb-5'>
                      Create an account
                    </h2>

                    <form onSubmit={(e) => register(e)}>
                      <div className='form-outline mb-4'>
                        <label className='form-label' for='form3Example1cg'>
                          Name
                        </label>
                        <input
                          type='text'
                          className='form-control form-control-lg'
                          name='name'
                          value={formData.name}
                          onChange={handleChange}
                        />
                      </div>
                      <div className='form-outline mb-4'>
                        <label className='form-label' for='form3Example1cg'>
                          Username
                        </label>
                        <input
                          type='text'
                          className='form-control form-control-lg'
                          name='username'
                          value={formData.username}
                          onChange={handleChange}
                        />
                      </div>
                      <div className='form-outline mb-4'>
                        <label className='form-label' for='form3Example3cg'>
                          Phone
                        </label>

                        <input
                          type='text'
                          className='form-control form-control-lg'
                          name='phone'
                          value={formData.phone}
                          onChange={handleChange}
                        />
                      </div>

                      <div className='form-outline mb-4'>
                        <label className='form-label' for='form3Example4cg'>
                          Email
                        </label>

                        <input
                          type='email'
                          className='form-control form-control-lg'
                          name='email'
                          value={formData.email}
                          onChange={handleChange}
                        />
                      </div>

                      <div className='form-outline mb-4'>
                        <label className='form-label' for='form3Example4cdg'>
                          Password
                        </label>
                        <input
                          type='password'
                          className='form-control form-control-lg'
                          name='password'
                          value={formData.password}
                          onChange={handleChange}
                        />
                      </div>

                      <div
                        className='form-outline mb-4'
                        style={{ display: 'flex' }}
                      >
                        <div style={{ flex: '0.2', fontSize: '18px' }}>
                          <label className='form-label' for='form3Example4cdg'>
                            Gender
                          </label>
                        </div>
                        <div
                          class='form-check form-check-inline'
                          style={{ flex: '0.8', fontSize: '18px' }}
                        >
                          <div>
                            <input
                              class='form-check-input'
                              type='radio'
                              name='gender'
                              value='Male'
                              checked={formData.gender === 'Male'}
                              onChange={handleChange}
                            />
                            <label
                              class='form-check-label'
                              for='inlineRadio1'
                              style={{ textAlign: 'center' }}
                            >
                              Male
                            </label>
                          </div>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <div class='form-check form-check-inline'>
                            <input
                              class='form-check-input'
                              type='radio'
                              name='gender'
                              value='Female'
                              checked={formData.gender === 'Female'}
                              onChange={handleChange}
                            />
                            <label
                              class='form-check-label'
                              for='inlineRadio2'
                              style={{ textAlign: 'center' }}
                            >
                              Female
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class='form-group' style={{ display: 'flex' }}>
                        <label
                          className='form-label'
                          for='form3Example4cdg'
                          style={{ fontSize: '18px', flex: '0.2' }}
                        >
                          Roles
                        </label>

                        <div style={{ fontSize: '18px', flex: '0.8' }}>
                          <select
                            class='form-control'
                            name='roles'
                            onChange={handleChange}
                            value={formData.roles}
                          >
                            <option value='Admin'>Admin</option>
                            <option value='User'>User</option>
                          </select>
                        </div>
                      </div>

                      <div className='d-flex justify-content-center'>
                        <button
                          type='submit'
                          className='btn  btn-block btn-lg gradient-custom-4 text-body'
                          style={{ backgroundColor: '#00b0f0', color: 'white' }}
                        >
                          Register
                        </button>
                      </div>

                      <p className='text-center text-muted mt-5 mb-0'>
                        Have already an account?
                        <a href='#!' className='fw-bold text-body'>
                          <li>
                            <Link to='login'>Login here</Link>
                          </li>
                        </a>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
