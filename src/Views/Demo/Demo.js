import "./Demo.css";
import Layout from "../../Component/Layout/Layout";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

export default function Demo() {
  let { qid } = useParams();

  const [caseData, setCaseData] = useState([]);

  const fetchData = () => {
    return axios
      .get(`http://localhost:5008/captcha/${qid}`)
      .then((res) => {
        const results = res.data;
        console.log("Results : ", results);
        return results;
      })
      .catch((err) => {
        console.log(err);
      });
  };
  fetchData().then((apiCase) => {
    setCaseData(apiCase);
  });
  return (
    <>
      {/* {caseData.map((d, i) => (
                <div key={i}>{d.case_no}</div>
              ))} */}
      <Layout>
        <div className="container" style={{ backgroundColor: "#fff" }}>
          <p>Earlier Count Details</p>
          <div>
            <table
              className="table table-bordered"
              style={{ border: "1px solid black" }}
            >
              <tbody>
                <tr>
                  <th>S.No</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>Count</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>Agency State</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>Agency Code</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>Case No</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>Order Date</th>
                  <td>hello</td>
                </tr>
                <tr>
                  <th>CNR No./Designation</th>
                  <td>hello</td>
                  </tr>
                <tr>
                    <th>Judge</th>
                    <td>hello</td>
                 </tr>
                  <tr>
                    <th>Police Station</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Crime No. /Year</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Authority/Organisation/Impugned Order No.</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Judgement Challanged</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Judgement Type</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Judgement Covered In</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Vehicle Number</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Reference count/State/District/No.</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Replied Upon count/State/District/No.</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Transfer To State/District/No.</th>
                    <td>hello</td>
                  </tr>
                  <tr>
                    <th>Government Notification State/No./Date</th>
                    <td>hello</td>
                  </tr>
              
              </tbody>
            </table>
          </div>
        </div>
      </Layout>
    </>
  );
}
