import React from 'react'
import FormGenerator from '../../Component/Layout/Formake/FormGenerator'
import Formake from '../../Component/Layout/Formake/Formake'
import { useParams } from 'react-router-dom'
import '../../Component/Layout/Formake/Formake.css'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'
import Cal from '../../Component/Calendar/DemoApp'

export default function Calendar() {
  return (
    <Layout>
      <Cal />
    </Layout>
  )
}
