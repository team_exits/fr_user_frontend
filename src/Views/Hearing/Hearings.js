import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteDataById, getAllData } from '../../Utils/Axios/CommonAxios'
import Config from '../../Utils/Axios/Config'
import ExtendedDataTables from '../../Component/ExtendedDataTables/ExtendedDataTables'
import Layout from '../../Component/Layout/Layout'

export default function Hearings(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props
  const navigation = props
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const login = useSelector((state) => state.login)
  const { token } = login
  // console.log('TokenHai', token)

  return (
    <Layout>
      <div className={classes.root}>
        <div style={{ flex: 1 }}>
          <ExtendedDataTables
            navigation={navigation}
            editRoute='/hearing'
            // deleteRoute="/delete_FORM_ROUTE"
            cols={{
              // _id: "UID",
              cino: 'CNR Number',
              hearings: (obj) => {
                console.log('DataQSTN ::: ', obj.hearings)
                return {
                  colName: 'Hearings',
                  return: obj.hearings ? (
                    obj.hearings ? (
                      <>
                        <table>
                          <tbody>
                            <tr>
                              <th>Judges</th>
                              <th>BusinessDate</th>
                              <th>NextDate</th>
                              <th>PurposeOfHearing</th>
                            </tr>
                            {typeof obj.hearings != undefined &&
                              obj.hearings.length > 0 &&
                              obj.hearings[0].business.map((d, i) => {
                                return (
                                  <>
                                    {i == 0 ? (
                                      <>
                                        <tr>
                                          <td>{d.caseHistoryObj.judges}</td>
                                          <td>
                                            {d.caseHistoryObj.businessDate}
                                          </td>
                                          <td>{d.caseHistoryObj.nextdate1}</td>
                                          <td>
                                            {d.caseHistoryObj.purposeOfHearing}
                                          </td>
                                        </tr>
                                      </>
                                    ) : null}
                                  </>
                                )
                              })}
                          </tbody>
                        </table>
                      </>
                    ) : null
                  ) : null,
                }
              },
              // date: null,
              // regNo: null,
              // judge: null,
              // nextDate: null,
              // nextPurpose: null,
              // court_id: null,
              // client_id: null,
              // lawyer_id: null,
            }}
            viewApiType='GET'
            viewApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
            editApiType='PUT'
            editApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
            deleteApiType='DELETE'
            deleteApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
            headers={{
              Authorization: 'Bearer ' + token,
            }}
          />
        </div>
      </div>
    </Layout>
  )
}
