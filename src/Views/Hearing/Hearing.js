import React from 'react'
import FormGenerator from '../../Component/Layout/Formake/FormGenerator'
import Formake from '../../Component/Layout/Formake/Formake'
import { useParams } from 'react-router-dom'
import '../../Component/Layout/Formake/Formake.css'
import Layout from '../../Component/Layout/Layout'
import Config from '../../Utils/Axios/Config'

export default function Hearing() {
  let { qid } = useParams()

  return (
    <>
      <Layout>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='Regular-form'>
                <Formake
                  useAxios={true}
                  edit={qid ? true : false}
                  editId={qid}
                  title={qid ? 'Edit Hearing' : 'Add Hearing'}
                  createApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
                  createType='POST'
                  editApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
                  editType='GET'
                  updateApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
                  updateType='PUT'
                  deleteApi={`http://${Config.IP}:${Config.HEARING_PORT}/api/hearing`}
                  deleteType='DELETE'
                  auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                  headers={{}}
                  params={{}}
                  initialState={{
                    level: null,
                    questionType: null,
                    question: null,
                    answer: null,
                    points: null,
                    // levels: '60deacc31c0b9a091c2cb2f4',
                  }}
                  // colwidths in number totaling upto 12 e.g. 8+4= 12
                  cols={[6, 6]}
                  spacing={3}
                  isSubmitting={false}
                  //used for default change handler functions having switch case inside the to set proper setState
                  updatedValues={(formValues) =>
                    console.log('formValues UPDATING --> ', formValues)
                  }
                  onChangeHandler={(name, value) => {
                    console.log('formValuesx UPDATING --> ', name, value)
                  }}
                  onSubmit={(values) =>
                    console.log('formValues SUBMITTING', values)
                  }
                  fields={[
                    [
                      {
                        type: 'text',
                        name: 'cino',
                        label: 'Cino',
                        props: {},
                      },
                    ],
                  ]}
                  /*
                  fields={[
                    [
                      {
                        type: 'text',
                        name: 'date',
                        label: 'Date',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'regNo',
                        label: 'Registration No',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'judge',
                        label: 'Judge',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'court_id',
                        label: 'Court Id',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'nextDate',
                        label: 'Next Date',
                      },
                      {
                        type: 'text',
                        name: 'nextPurpose',
                        label: 'Next Purpose',
                        props: {},
                      },
                    ],
                    [
                      {
                        type: 'text',
                        name: 'client_id',
                        label: 'Client Id',
                        props: {},
                      },
                      {
                        type: 'text',
                        name: 'lawyer_id',
                        label: 'Lawyer Id',
                      },
                    ],
                  ]}
                  */
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
