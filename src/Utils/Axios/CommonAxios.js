import axios from "axios";
import { useDispatch } from "react-redux";
import { setError, setLoading } from "../../Redux/Slice/caseSlice";
import Config from "./Config";

//add case
export const createData = (values, path, port) => async (dispatch) => {
  console.log(values, `http://${Config.IP}:${Config.PORT}/api/${path}`);
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    dispatch(setLoading(true));
    dispatch(setError(null));
    const { data } = await axios.post(
      `http://${Config.IP}:${Config.PORT}/api/${path}`,
      values,
      config
    );

    if (data.success) {
      // dispatch(setCase(data.data));
      // console.log("success");
      // dispatch(setModal(true));
      dispatch(setLoading(false));
      return {
        status: true,
        message: "Case data has been added Successfully !!",
      };
    } else {
      dispatch(setLoading(false));
      // console.log("Failed");
      return {
        status: true,
        message: "Something went wrong please try again !!",
      };
    }
  } catch (error) {
    dispatch(
      setError(
        error.response && error.response.data.message
          ? error.response.data
          : error.message
      )
    );

    dispatch(setLoading(false));
    return {
      status: true,
      message:
        error.response && error.response.data.message
          ? error.response.data
          : error.message,
    };
  }
};

// Get all case data
export const getAllData = (path, port) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  console.log(path, port);
  try {
    dispatch(setLoading(true));
    dispatch(setError(null));
    // dispatch(setCase([]));
    const { data } = await axios.get(
      `http://${Config.IP}:${Config.PORT}/api/${path}`,
      config
    );

    if (data.success) {
      let responseData = data.results.reverse();
      // console.log(responseData);
      //   dispatch(setCase(responseData));
      // console.log(responseData);
      dispatch(setLoading(false));
    } else {
      dispatch(setLoading(false));
    }
  } catch (error) {
    dispatch(
      setError(
        error.response && error.response.data.message
          ? error.response.data
          : error.message
      )
    );
    dispatch(setLoading(false));
  }
};

export const getDataById = (id, path, port) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    dispatch(setLoading(true));
    dispatch(setError(null));
    // dispatch(setCaseById({}));
    // dispatch(setCase([]));
    const { data } = await axios.get(
      `http://${Config.IP}:${Config.PORT}/api/${path}/${id.id}`,
      config
    );

    if (data.success) {
      dispatch(setLoading(false));
      //   dispatch(setCaseById(data.results));
    }

    dispatch(setLoading(false));

    // if (data.success) {
    //   dispatch(setCaseById(data));
    // }
  } catch (error) {
    dispatch(
      setError(
        error.response && error.response.data.message
          ? error.response.data
          : error.message
      )
    );
    dispatch(setLoading(false));
  }
};

export const deleteDataById = (id, path, port) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    dispatch(setLoading(true));
    dispatch(setError(null));
    const { data } = await axios.delete(
      `http://${Config.IP}:${Config.PORT}/api/${path}/${id}`,
      config
    );

    if (data.success) {
      dispatch(getAllData(path, port));
      dispatch(setLoading(false));
    }
  } catch (error) {
    dispatch(
      setError(
        error.response && error.response.data.message
          ? error.response.data
          : error.message
      )
    );
    dispatch(setLoading(false));
  }
};

// export const updateDataById = (id, values, path, port) => async (dispatch) => {
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   try {
//     dispatch(setLoading(true));
//     dispatch(setError(null));

//     const { data } = await axios.put(
//       `http://${Config.IP}:${Config.PORT}/api/${path}/${id}`,
//       values,
//       config
//     );

//     // console.log(data)
//     if (data.success) {
//       dispatch(setLoading(false));
//       return {
//         status: true,
//         message: `${path} Data Updated successfully updated !!`,
//       };
//     }
//   } catch (error) {
//     dispatch(
//       setError(
//         error.response && error.response.data.message
//           ? error.response.data
//           : error.message
//       )
//     );
//     dispatch(setLoading(false));
//   }
// };

// export const deleteAllData = (path, port) => async (dispatch) => {
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   try {
//     dispatch(setLoading(true));
//     dispatch(setError(null));
//     const { data } = await axios.delete(
//       `http://${Config.IP}:${Config.PORT}/api/${path}`,
//       config
//     );

//     if (data.success) {
//       dispatch(getAllData(path, port));
//       dispatch(setLoading(false));
//     }
//   } catch (error) {
//     dispatch(
//       setError(
//         error.response && error.response.data.message
//           ? error.response.data
//           : error.message
//       )
//     );
//     dispatch(setLoading(false));
//   }
// };

// export const getCaseById = (id) => async (dispatch) => {
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };
//   try {
//     dispatch(setLoading(true));
//     dispatch(setError(null));
//     const { data } = await axios.get(
//       process.env.REACT_APP_API_URL + `/search/apid/${id}`,
//       config
//     );

//     if (data.success) {
//       dispatch(setCaseDataById(data.data));
//       dispatch(setLoading(false));
//     } else {
//       // dispatch(setError("Something went wrong"));
//       console.log("error");
//     }
//   } catch (error) {
//     dispatch(
//       setError(
//         error.response && error.response.data.message
//           ? error.response.data
//           : error.message
//       )
//     );
//     dispatch(setLoading(false));
//   }
// };

// // Get all case data

// export const getCateroryData = (path, port) => async (dispatch) => {
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   console.log(path, port);
//   try {
//     dispatch(setLoading(true));
//     const { data } = await axios.get(
//       `http://${Config.IP}:${Config.PORT}/api/${path}`,
//       config
//     );

//     if (data.success) {
//       let responseData = data.results.reverse();
//       console.log(responseData);
//       // dispatch(setCase(responseData))
//       // console.log(responseData);
//       dispatch(setLoading(false));
//       return responseData;
//     } else {
//       dispatch(setLoading(false));
//     }
//   } catch (error) {
//     dispatch(
//       setError(
//         error.response && error.response.data.message
//           ? error.response.data
//           : error.message
//       )
//     );
//     dispatch(setLoading(false));
//   }
// };
