const Config = {
  // IP: 'localhost',
  IP: '3.110.39.230',
  // PROTO: 'https',
  PROTO: 'http',

  // UNIFIED: '5024',
  // CASE_PORT: '5003',
  // DOC_PORT: '5004',
  // TODO_PORT: '5005',
  // SC_PORT: '5010',
  // DS_PORT: '5000',

  //----------------------TESTING----------
  UNIFIED: '5000',
  SC_PORT: '5000',
  DS_PORT: '5000',

  PORT: '5001',
  CASE_PORT: '5001',
  DOC_PORT: '5001',
  ORDER_PORT: '5001',
  HEARING_PORT: '5001',
  TODO_PORT: '5001',
}

export default Config
