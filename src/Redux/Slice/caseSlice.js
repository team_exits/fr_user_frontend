import { createSlice } from '@reduxjs/toolkit'
import Config from '../../Utils/Axios/Config'

export const caseslice = createSlice({
  name: 'cases',
  initialState: {
    loading: false,
    caseData: [],
    caseById: null,
    error: null,
    caseDataById: null,
    modal: false,
  },
  reducers: {
    setCase: (state, action) => {
      state.caseData = action.payload
    },

    setCaseById: (state, action) => {
      state.caseById = action.payload
    },

    setError: (state, action) => {
      state.error = action.payload
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    },
    setCaseDataById: (state, action) => {
      state.caseDataById = action.payload
    },
    setModal: (state, action) => {
      state.modal = action.payload
    },
  },
})

//actions
export const {
  setModal,
  setCase,
  setError,
  setLoading,
  setCaseDataById,
  setCaseById,
} = caseslice.actions
//selectlogin grabs the login data

export default caseslice.reducer
