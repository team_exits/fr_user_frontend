import React, { useState, useEffect } from 'react'
import './Header.css'
import { GrNotification } from 'react-icons/gr'
import { CgProfile } from 'react-icons/cg'
import { Link } from '@material-ui/core'
import SwipeableTemporaryDrawer from '../../SwipableMenu/SwipableMenu'
import { useSelector, useDispatch } from 'react-redux'
import { setLogin } from '../../../Redux/Slice/loginSlice'
import logo from '../../../Image/Layout/logo.png'
import profile from '../../../Image/Layout/profile.jpeg'

const Header = () => {
  const dispatch = useDispatch()
  const [usernm, setUserNm] = useState('')
  const { name, roles } = useSelector((state) => state.login.login)
  console.log('name >> ' + name)
  useEffect(() => {
    setUserNm(name)
  }, [])
  const logout = (e) => {
    e.preventDefault()
    // dispatch(setLogin({ isLogged: false, login: null }))
    dispatch(
      setLogin({
        isLogged: false,
        login: {
          name: '',
          username: '',
          roles: '',
          email: '',
          mobile: '',
          roles: 'user',
        },
      })
    )
    try {
      const serialisedState = localStorage.removeItem('persistantState')
      if (serialisedState === null) return undefined
      return JSON.parse(serialisedState)
    } catch (e) {
      console.warn(e)
      return undefined
    }
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='header'>
          <Link href='/'>
            <img className='header-logo' src={logo} />
          </Link>
          <div className='web-nav'>
            <GrNotification size={30} style={{ marginRight: '18px' }} />
            {/* <CgProfile size={24} />
            <p>Ankit</p> */}
            <img
              // src='Image/Layout/profile.jpeg'
              src={profile}
              alt='image not found'
              style={{ width: '45px', height: '45px', borderRadius: '50%' }}
            ></img>
            <div class='dropdown'>
              <button
                class='dropbtn'
                style={{ display: 'flex', justifyContent: 'space-around' }}
              >
                {typeof usernm != undefined ? usernm : `Nitin Jain`}
              </button>
              <p
                style={{
                  color: '#858585',
                  // position: " relative",
                  paddingLeft: '16px',
                  // top: -17,
                  marginTop: '-15px',
                }}
              >
                {/* Admin account */}
              </p>
              <div class='dropdown-content'>
                <a href='./Profile.js'>Profile</a>
                <a href='#'>Settings</a>
                <Link
                  to='login'
                  style={{ cursor: 'pointer' }}
                  onClick={(e) => logout(e)}
                >
                  Logout
                </Link>
              </div>
            </div>
          </div>
          <div className='mobile-menu'>
            <SwipeableTemporaryDrawer />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header
