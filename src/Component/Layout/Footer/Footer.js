import React, { useState } from 'react'
import { Fab, Action } from 'react-tiny-fab'
import {
  AiOutlineAppstore,
  AiOutlineSound,
  AiFillFileAdd,
  AiFillPlusCircle,
} from 'react-icons/ai'
import { BsBriefcase, BsThreeDots } from 'react-icons/bs'
import { HiOutlineDocumentText, HiOutlineTicket } from 'react-icons/hi'
import { Link } from 'react-router-dom'
import './Footer.css'

const Footer = () => {
  const [hover, setHover] = useState(false)
  const onHover = () => {
    setHover(!hover)
  }
  const onLeave = () => {
    setHover(false)
  }
  return (
    <div
      className='container-fluid'
      // style={{
      //   display: 'flex',
      //   flexDirection: 'row',
      //   padding: '1rem 1rem',
      //   justifyContent: 'flex-end',

      // }}
    >
      <div
        className='row'
        // style={{
        //   display: 'flex',
        //   flexDirection: 'column',
        //   padding: '1rem 1rem',
        // }}
      >
        <Fab
          icon={
            <AiFillPlusCircle
              size={30}
              style={{ color: '#fff',
                       height: '60px',
                      width: '80px',
                      padding: '2px',
                      backgroundColor: '#09b2f0',
                      borderRadius: '55px' }}
            />
          }
          alwaysShowTitle={true}
        >
          {/* <BsBriefcase
            text='Case'
            //  onClick={() => {}}
          /> */}
          <div
            onMouseEnter={onHover}
            onMouseLeave={onLeave}
            role='button'
            tabIndex='-3'
          >
            <Link to={'/case'}>
              <BsBriefcase
                text='Case'
                style={{ color: '#09b2f0', height: '40px', width: '40px' }}
                //  onClick={() => {}}
              />
            </Link>
          </div>
          <Link to={'/document'}>
            <HiOutlineDocumentText
              text='Document'
              // onClick={}
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
          </Link>
          <Link to={'/order'}>
            <HiOutlineTicket
              text='Order'
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}

              // onClick={}
            />
          </Link>
          <Link to={'/hearing'}>
            <AiOutlineSound
              text='Hearing'
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}

              // onClick={}
            >
              <i className='fa fa-help' />
            </AiOutlineSound>
          </Link>

          <Link to={'/todo'}>
            <HiOutlineDocumentText
              text='Todo'
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}

              // onClick={}
            >
              <i className='fa fa-foo-bar-fa-foo' />
            </HiOutlineDocumentText>
          </Link>
        </Fab>
      </div>
    </div>
  )
}

export default Footer
