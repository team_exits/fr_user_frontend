import React from 'react'
import { GrNotification } from 'react-icons/gr'
import { CgProfile } from 'react-icons/cg'
import { AiOutlineAppstore, AiOutlineSound } from 'react-icons/ai'
import { FaRegEdit } from 'react-icons/fa'
import {
  BiCalendar,
  BiSave,
  BiBarChartSquare,
  BiCalendarEvent,
} from 'react-icons/bi'
import {
  IoIosArrowDropright,
  IoIosArrowDropdown,
  IoIosArrowDropup,
} from 'react-icons/io'
import { HiOutlineDocumentText, HiOutlineTicket } from 'react-icons/hi'
import { BsBriefcase, BsThreeDots } from 'react-icons/bs'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import { BsDownload } from 'react-icons/bs'
import { FiFilter } from 'react-icons/fi'
import { HiOutlineSearch } from 'react-icons/hi'
import { Link, useRouteMatch } from 'react-router-dom'
import { Icon } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { setSelected } from '../../../Redux/Slice/loginSlice'
import './Sidebar'

const Sidebar = (props) => {
  let match = useRouteMatch()
  console.log(match.path)
  const login = useSelector((state) => state.login)
  const { selected } = login
  const dispatch = useDispatch()

  const sidebarData = [
    {
      id: 0,
      title: 'Dashboard',
      goto: '/',
      leftIconBlue: (
        <AiOutlineAppstore
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <AiOutlineAppstore
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
    },
    {
      id: 1,
      title: 'Calendar',
      goto: '/calendar',
      leftIconBlue: (
        <BiCalendar
          style={{ color: '#09b2f0', height: '40px', width: '40px' }}
        />
      ),
      leftIconWhite: (
        <BiCalendar style={{ color: 'white', height: '40px', width: '40px' }} />
      ),
      isActive: false,
    },
    {
      id: 2,
      title: 'Case',
      goto: '/cases',
      leftIconBlue: (
        <BsBriefcase
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <BsBriefcase
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Case',
          goto: '/case',
          isActive: false,
        },
        {
          id: 1,
          title: 'View Cases',
          goto: '/cases',
          isActive: false,
        },
      ],
    },
    {
      id: 3,
      title: 'Document',
      goto: '/documents',
      leftIconBlue: (
        <HiOutlineDocumentText
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <HiOutlineDocumentText
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Document',
          goto: '/document',
          isActive: false,
        },
        {
          id: 1,
          title: 'View Documents',
          goto: '/documents',
          isActive: false,
        },
      ],
    },
    {
      id: 4,
      title: 'Order',
      goto: '/orders',
      leftIconBlue: (
        <HiOutlineTicket
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <HiOutlineTicket
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Order',
          goto: '/order',
          isActive: false,
        },
        {
          id: 1,
          title: 'View Order',
          goto: '/orders',
          isActive: false,
        },
      ],
    },
    {
      id: 5,
      title: 'Hearing',
      goto: '/hearings',
      leftIconBlue: (
        <AiOutlineSound
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <AiOutlineSound
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Hearing',
          goto: '/hearing',
          isActive: false,
        },
        {
          id: 1,
          title: 'View Hearing',
          goto: '/hearings',
          isActive: false,
        },
      ],
    },
    {
      id: 6,
      title: 'Todo',
      goto: '/todos',
      leftIconBlue: (
        <HiOutlineDocumentText
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <HiOutlineDocumentText
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Todo',
          goto: '/todo',
          isActive: false,
        },
        {
          id: 1,
          title: 'View Todo',
          goto: '/todos',
          isActive: false,
        },
      ],
    },
    {
      id: 7,
      title: 'Case Processing',
      goto: '/case-processing',
      leftIconBlue: (
        <FaRegEdit
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <FaRegEdit style={{ color: 'white', height: '35px', width: '35px' }} />
      ),
      isActive: false,
      children: [
        {
          id: 0,
          title: 'Add Case Processing',
          goto: '/case-processing',
          isActive: false,
        },
        {
          id: 1,
          title: 'My Assingment Task',
          goto: '/case-processings',
          isActive: false,
        },
      ],
    },

    {
      id: 8,
      title: 'Ecourts',
      goto: '/ecourts',
      leftIconBlue: (
        <BiBarChartSquare
          style={{ color: '#09b2f0', height: '35px', width: '35px' }}
        />
      ),
      leftIconWhite: (
        <BiBarChartSquare
          style={{ color: 'white', height: '35px', width: '35px' }}
        />
      ),
      isActive: false,
    },
    // {
    //   id: 9,
    //   title: 'View Case History',
    //   goto: '/view-case-history',
    //   leftIconBlue: (
    //     <BiSave style={{ color: '#09b2f0', height: '40px', width: '40px' }} />
    //   ),
    //   leftIconWhite: (
    //     <BiSave style={{ color: 'white', height: '40px', width: '40px' }} />
    //   ),
    //   isActive: false,
    // },
  ]

  // const [selected, setSelected] = React.useState(0)

  const toggleMenu = (index) => {
    console.log('selected => ', selected, index)
    if (selected == index) {
      dispatch(setSelected(null))
    } else {
      dispatch(setSelected(index))
    }
  }

  console.log('selected', selected)

  return (
    <div style={{ display: 'flex', flex: 0.1 }}>
      <div
        style={{
          width: '100%',
          backgroundColor: '#fff',
          padding: '8rem 0rem',
          borderRight: '1px solid #e2e2e3',
          height: '100vh',
          overflowX: 'overlay',
        }}
      >
        {sidebarData &&
          sidebarData.map((d, i) => {
            console.log(d, i)
            return (
              <>
                <Link to={d.goto} style={{ textDecoration: 'none' }}>
                  <div
                    onClick={() => toggleMenu(i)}
                    style={{
                      display: 'flex',
                      height: 50,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      padding: '0 2rem',
                      margin: '1rem 0',
                      cursor: 'pointer',
                      backgroundColor:
                        match.path === d.goto ||
                        match.path === d.goto ||
                        selected === i
                          ? '#09b2f0'
                          : 'white',
                      borderTopRightRadius: '50px',
                      borderBottomRightRadius: '50px',
                      marginRight: '1rem',
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      {match.path === d.goto || selected === i
                        ? d.leftIconWhite
                        : d.leftIconBlue}
                      <div
                        style={{
                          marginLeft: '1rem',
                          // marginTop: '10px',
                        }}
                      >
                        <span
                          style={{
                            color:
                              match.path === d.goto || selected === i
                                ? 'white'
                                : '#8594b8',
                          }}
                        >
                          {d.title}
                        </span>
                      </div>
                    </div>
                    <div>
                      {d.children && (
                        <>
                          {match.path === d.goto || selected === i ? (
                            <IoIosArrowDropup
                              size={30}
                              color='white'
                              style={{ marginTop: '5px', cursor: 'pointer' }}
                              // onClick={() => toggleMenu(i)}
                            />
                          ) : (
                            <IoIosArrowDropdown
                              size={30}
                              color='#09b2f0'
                              style={{ marginTop: '5px', cursor: 'pointer' }}
                              // onClick={() => toggleMenu(i)}
                            />
                          )}
                        </>
                      )}
                    </div>
                  </div>
                </Link>

                {selected == i && (
                  <>
                    {' '}
                    {d.children &&
                      d.children.map((c, i) => {
                        return (
                          <>
                            <div
                              style={{
                                marginLeft: '4rem ',
                                // margin: "1rem 0",
                              }}
                            >
                              <ul style={{ listStyleType: 'circle' }}>
                                <li>
                                  <Link to={c.goto}>
                                    <a
                                      style={{
                                        color: '#8594b8',
                                        fontSize: '16px',
                                        textDecoration: 'none',
                                      }}
                                    >
                                      {c.title}
                                    </a>
                                  </Link>
                                </li>
                              </ul>
                            </div>
                          </>
                        )
                      })}
                  </>
                )}
              </>
            )
          })}
      </div>
    </div>
  )
}

export default Sidebar
