import React, { useEffect, useState, useCallback, useRef } from 'react'
import { IconContext } from 'react-icons'
import { GiProgression } from 'react-icons/gi'
import { MdUpdate } from 'react-icons/md'
import AsyncSelect from 'react-select/async'
import moment from 'moment'
import { createMuiTheme } from '@material-ui/core'
import blue from '@material-ui/core/colors/blue'
import { ThemeProvider } from '@material-ui/styles'

import { DatePicker } from '@material-ui/pickers'
import { TimePicker } from '@material-ui/pickers'
import { DateTimePicker } from '@material-ui/pickers'

import FileUpload from './FileUpload'

import {
  EditorState,
  convertToRaw,
  ContentState,
  convertFromRaw,
} from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import MultiSelect from './MultiSelect'

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'

const datePickerTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: blue.A200,
      },
    },
    MuiPickersCalendarHeader: {
      switchHeader: {
        // backgroundColor: blue.A200,
        // color: "white",
      },
    },
    MuiPickersDay: {
      day: {
        color: '#000',
      },
      daySelected: {
        backgroundColor: blue['400'],
      },
      dayDisabled: {
        color: blue['100'],
      },
      current: {
        color: blue['900'],
      },
    },
    MuiPickersModal: {
      dialogAction: {
        color: blue['400'],
      },
    },
  },
})

export default function Input(props) {
  // if (props.type == "radio") {
  //   console.log("isSubmittingxxInputProps", props);
  // }
  const [textInput, setTextInput] = useState(props.value)
  const [field, setField] = useState(<h1>Hi</h1>)
  const [dateSv, setDateSv] = useState(
    props.value ? moment(props.value, props.inputFormat).format('Y-M-D') : null
  )
  const [timeSv, setTimeSv] = useState(
    props.value ? moment(props.value, props.inputFormat) : null
  )
  const [dateTimeSv, setDateTimeSv] = useState(
    props.value ? moment(props.value, props.inputFormat) : null
  )
  const [checkedRadio, setCheckedRadio] = useState()
  const [checked, setChecked] = useState({})
  const [checkedSingle, setCheckedSingle] = useState(false)

  const [editorState, setEditorState] = useState(EditorState.createEmpty())
  const [editorOutput, setEditorOutput] = useState()
  useEffect(() => {
    if (props.type == 'text') {
      setTextInput(props.value)
    }
    if (props.type == 'radio') {
      setCheckedRadio(props.value)
    }
  }, [props.value])
  useEffect(() => {
    if (props.type == 'radio') {
      console.log('formValues Updating', checkedRadio, props)
    }
    // console.log("Input isSubmitting useEffect", props.isSubmitting);
    switch (props.type) {
      case 'empty':
        setField(
          <label
            {...props}
            style={{
              marginTop: 10,
              marginBottom: 10,
              fontWeight: 'bold',
              color: 'white',
              ...props.style,
            }}
          >
            {' '}
            {props.value}
          </label>
        )
        break
      case 'label':
        setField(
          <label
            {...props}
            style={{
              marginTop: 10,
              marginBottom: 10,
              fontWeight: 'bold',

              ...props.style,
            }}
          >
            {' '}
            {props.value}
          </label>
        )
        break
      case 'text':
        setField(
          <input
            {...props}
            type='text'
            name={props.name}
            label={props.label}
            value={textInput}
            className={props.className || 'form-control'}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(event) => {
              setTextInput(event.target.value)
              props.handleChange(props.name, event.target.value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
            required={props.required || false}
          />
        )
        break
      case 'email':
        setField(
          <input
            {...props}
            type='email'
            name={props.name}
            label={props.label}
            value={textInput}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(event) => {
              setTextInput(event.target.value)
              props.handleChange(props.name, event.target.value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
            required
          />
        )
        break
      case 'phone':
        setField(
          <input
            {...props}
            type='text'
            name={props.name}
            label={props.label}
            value={textInput}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(event) => {
              setTextInput(event.target.value)
              props.handleChange(props.name, event.target.value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
            required
            maxLength={props.maxLength || 10}
          />
        )
        break
      case 'editor':
        setField(
          <div className='rdw-storybook-root'>
            <Editor
              editorState={editorState}
              wrapperClassName='demo-wrapper'
              editorClassName='demo-editor'
              // toolbarClassName="toolbarClassName"
              // wrapperClassName="wrapperClassName"
              // editorClassName="editorClassName"
              onChange={(value) => {
                console.log('editor >> onChange', value)
              }}
              onEditorStateChange={(value) => {
                console.log('editor >> onEditorStateChange', value)
                setEditorState(value)
                setEditorOutput(
                  draftToHtml(convertToRaw(editorState.getCurrentContent()))
                )
                props.handleChange(
                  props.name,
                  draftToHtml(convertToRaw(editorState.getCurrentContent()))
                )
              }}
              onContentStateChange={(value) => {
                console.log(
                  'editor >> onContentStateChange',
                  value,
                  convertFromRaw(value)
                )
              }}
              // {...props}
              name={props.name}
              label={props.label}
              value={props.value}
              // className={props.className || "form-control"}
              required={props.required || false}
              title={props.title || ''}
              // placeholder={props.placeholder || ""}
              // onChange={(event) => {
              //   props.handleChange(props.name, event.target.value);
              // }}
              wrapperStyle={{
                marginTop: 10,
                marginBottom: 10,
                minHeight: 300,
                height: '100%',
                width: '100%',
                border: 1,
                borderWidth: 1,
                borderColor: '#d7d7d7',
                borderStyle: 'solid',
                ...props.style,
              }}
              editorStyle={{ padding: '0 5px', width: '100%' }}
            />
            {props.preview ? (
              <textarea
                disabled
                style={{ width: '100%', flex: 1 }}
                value={editorOutput}
              />
            ) : null}
          </div>
        )
        break
      case 'textarea':
        setField(
          <textarea
            {...props}
            rows={5 || props.rows}
            type='text'
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(event) => {
              props.handleChange(props.name, event.target.value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              height: 122,
              ...props.style,
            }}
          />
        )
        break
      case 'paste':
      case 'gluejar':
      case 'screenshot':
      case 'snipping':
      case 'snippet':
        setField(
          <input
            type='text'
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onPaste={(event) => {
              console.log('e.clipboardData', event.clipboardData)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
          />
        )
        break
      case 'file':
        setField(
          <FileUpload
            {...props}
            type={props.type}
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(name, value) => {
              props.handleChange(props.name, value)
            }}
            isSubmitting={props.isSubmitting}
            // onSubmit={props.}
            style={{ marginTop: -25, ...props.style }}
          />
        )
        break
      case 'select':
        setField(
          <select
            {...props}
            type={props.type}
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || 'Select An Option'}
            onChange={(event) => {
              if (event.target.value) {
                props.handleChange(props.name, event.target.value)
              }
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
          >
            <option value=''>{props.placeholder || 'Select An Option'}</option>
            {props.options &&
              props.options.map((opt) => {
                return (
                  <option value={Object.keys(opt)}>{Object.values(opt)}</option>
                )
              })}
          </select>
        )
        break
      case 'select2':
      case 'multiselect':
        setField(
          <MultiSelect
            {...props}
            type={props.type}
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || 'MultiSelect An Option'}
            onChange={(name, value) => {
              console.log('msx', props.name, value)
              props.handleChange(props.name, value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
          />
        )
        break
      case 'date':
        setField(
          // <ThemeProvider theme={datePickerTheme}>
          //   <DatePicker
          //     {...props.inputProps}
          //     autoOk
          //     variant='inline'
          //     format={props.outputFormat ? props.outputFormat : 'D-M-Y'}
          //     // selected={Date.now()}
          //     name={props.name}
          //     value={dateSv}
          //     // value={
          //     //   props.value && dateSv == null
          //     //     ? moment(props.value, props.inputFormat).format("Y-M-D")
          //     //     : moment(dateSv).format("Y-M-D")
          //     // }
          //     required={props.required || false}
          //     title={props.title || ''}
          //     placeholder={props.placeholder || ''}
          //     className=''
          //     onChange={(date) => {
          //       setDateSv(date)
          //       props.handleChange(
          //         props.name,
          //         moment(date).format(props.inputFormat)
          //       )
          //     }}
          //     style={{
          //       padding: '6px 0',
          //       width: '100%',
          //       // margin: "10px 0",
          //       ...props.style,
          //     }}
          //   />
          // </ThemeProvider>
          <input
            type='date'
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onPaste={(event) => {
              console.log('e.clipboardData', event.clipboardData)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
          />
        )
        break
      case 'time':
        setField(
          <ThemeProvider theme={datePickerTheme}>
            <TimePicker
              {...props.inputProps}
              autoOk
              variant='inline'
              format={props.outputFormat ? props.outputFormat : 'hh:mm'}
              name={props.name}
              value={timeSv}
              required={props.required || false}
              title={props.title || ''}
              placeholder={props.placeholder || ''}
              className=''
              onChange={(time) => {
                setTimeSv(time)
                props.handleChange(
                  props.name,
                  moment(time).format(props.inputFormat)
                )
                // console.log("timesv change", time);
                // console.log("timesv change --->", moment(time).isValid(),moment(time));
              }}
              style={{ paddingTop: 0, width: '100%', ...props.style }}
            />
          </ThemeProvider>
        )
        break
      case 'datetime':
        setField(
          <ThemeProvider theme={datePickerTheme}>
            <DateTimePicker
              {...props.inputProps}
              autoOk
              variant='inline'
              format={props.outputFormat ? props.outputFormat : 'hh:mm'}
              name={props.name}
              value={timeSv}
              required={props.required || false}
              title={props.title || ''}
              placeholder={props.placeholder || ''}
              className=''
              onChange={(dateTime) => {
                setDateTimeSv(dateTime)
                props.handleChange(
                  props.name,
                  moment(dateTime).format(props.inputFormat)
                )
                // console.log("timesv change", time);
                // console.log("timesv change --->", moment(time).isValid(),moment(time));
              }}
              style={{ paddingTop: 0, width: '100%', ...props.style }}
            />
          </ThemeProvider>
        )
        break
      case 'radio':
        setField(
          <div>
            {props.options &&
              props.options.map((opt) => {
                return (
                  <div className='input-group'>
                    <label className=''>
                      <input
                        {...props}
                        type='radio'
                        // defaultChecked
                        checked={Object.keys(opt) == checkedRadio}
                        name={props.name}
                        value={Object.keys(opt)}
                        className={props.className || ''}
                        required={props.required || false}
                        title={props.title || ''}
                        placeholder={props.placeholder || ''}
                        onChange={(event) => {
                          // setCheckedRadio(Object.keys(opt));
                          // console.log(
                          //   "formValuesRADIO",
                          //   props.name,
                          //   Object.keys(opt)
                          // );
                          // props.handleChange(props.name, Object.keys(opt));
                        }}
                        onClick={(evt) => {
                          console.log('evt', evt.target.value)
                          setCheckedRadio(Object.keys(opt)[0])
                          props.handleChange(props.name, Object.keys(opt)[0])
                        }}
                        style={{
                          width: 14,
                          margin: 0,
                          marginTop: -5,
                          ...props.style,
                        }}
                      />
                      <span style={{ marginLeft: 5 }}>
                        {Object.values(opt)}
                      </span>
                    </label>
                  </div>
                )
              })}
          </div>
        )
        break
      case 'checkbox':
        setField(
          <div class='input-group'>
            <label class=''>
              <input
                {...props}
                type='checkbox'
                defaultChecked={checkedSingle}
                name={props.name}
                value={props.value}
                className={props.className || ''}
                required={props.required || false}
                title={props.title || ''}
                placeholder={props.placeholder || ''}
                onChange={(event) => {
                  setCheckedSingle(event.target.checked)
                  console.log(
                    'checkedSingle',
                    checkedSingle,
                    event.target.checked
                  )
                  props.handleChange(
                    props.name,
                    event.target.checked ? event.target.value : false,
                    'checkbox',
                    checked ? 'set' : 'unset'
                  )
                }}
                style={{
                  width: 14,
                  margin: 0,
                  marginTop: -5,
                  ...props.style,
                }}
              />
              <span style={{ marginLeft: 5 }}>{props.label}</span>
            </label>
          </div>
        )
        break
      case 'checkboxes':
        setField(
          <div>
            {props.options &&
              props.options.map((opt) => {
                const optKey = Object.keys(opt)
                const optVal = Object.values(opt)
                return (
                  <div class='input-group'>
                    <label class=''>
                      <input
                        {...props}
                        type='checkbox'
                        checked={checked[props.name]}
                        name={props.name}
                        value={checked[optKey]}
                        className={props.className || ''}
                        required={props.required || false}
                        title={props.title || ''}
                        placeholder={props.placeholder || ''}
                        onChange={(event) => {
                          const finalVal = event.target.checked ? true : false
                          setChecked({
                            ...checked,
                            [optKey]: finalVal,
                          })
                          props.handleChange(
                            props.name,
                            checked,
                            'checkboxes',
                            checked ? 'set' : 'unset'
                          )
                        }}
                        style={{
                          width: 14,
                          margin: 0,
                          marginTop: -5,
                          ...props.style,
                        }}
                      />
                      <span style={{ marginLeft: 5 }}>{optVal}</span>
                    </label>
                  </div>
                )
              })}
          </div>
        )
        break
      default:
        setField(
          <input
            {...props}
            type='text'
            name={props.name}
            label={props.label}
            value={props.value}
            className={props.className || 'form-control'}
            required={props.required || false}
            title={props.title || ''}
            placeholder={props.placeholder || ''}
            onChange={(event) => {
              props.handleChange(props.name, event.target.value)
            }}
            style={{
              marginTop: 10,
              marginBottom: 10,
              ...props.style,
            }}
          />
        )
        break
    }
  }, [
    props.isSubmitting,
    props.value,
    textInput,
    dateSv,
    timeSv,
    checked,
    editorState,
    props.response,
    checkedRadio,
  ])
  useEffect(() => {
    props.handleChange(
      props.name,
      checked,
      'checkboxes',
      checked ? 'set' : 'unset'
    )
  }, [checked])
  return (
    <>
      <div
        className={props.mainDivClass}
        style={{ marginTop: 5, ...props.mainDivStyle }}
      >
        <div
          className={props.inputDivClass}
          style={{ marginBottom: 0, marginTop: 0, ...props.inputDivStyle }}
        >
          {field}
        </div>
        <div
          className={props.subtitleDivClass}
          style={{
            marginBottom: props.subTitle ? 10 : 0,
            ...props.subTitleStyle,
          }}
        >
          <span
            style={{
              fontWeight: 'normal',
              fontStyle: 'italic',
              fontSize: '0.8em',
            }}
          >
            {props.subTitle || ''}
          </span>
        </div>
      </div>
    </>
  )
}
/* 
 id: 3,
      title: "Time Picker",
      subTitle: "Time Picker",
      icon: <MdUpdate />,
      inputType:,

*/
