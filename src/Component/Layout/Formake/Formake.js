import React, { Component } from "react";
import axios from "axios";

import FormGenerator from "./FormGenerator";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";

import "../Formake/Formake.css";

class Formake extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    const formFields = props.fields;
  }
  state = {
    apiValues: {
      date: "17-12-1991",
      dates: null,
      timedob: null,
      gender: null,
      group: null,
      fname: null,
      genderXXXX: "male",
      ageroup: null,
      picx: null,
      tnc: "yes",
    },
    savedValues: {},
    initialValues: {},
    // submission: false,
    isSubmitting: false,
    fields: [],
    progress: 0,
    sentFiles: 0,
    serverResponse: {},
    editId: null,
  };
  componentDidMount() {
    console.log("isSubmitting", this.props);
    if (this.props.isSubmitting) {
      this.setState((state) => {
        return {
          ...state,
          isSubmitting: this.props.isSubmitting,
        };
      });
    }
    if (typeof this.props.fields != "undefined" && this.props.fields) {
      const savedValues = this.state.savedValues;
      let editVals = savedValues;
      // if (prevProps !== this.props) {

      let fields = this.props.fields;
      const initState = this.props.initialState;
      if (fields && fields.length > 0) {
        const initialValues = this.state.initialValues;
        let initVals = { ...initState };
        let savedVals = this.state.savedVals;
        if (typeof savedVals === "undefined") {
          savedVals = {};
        }

        // Object.keys(initState).map((key) => {});
        if (this.props.edit == false) {
          fields.map((cols, i) => {
            cols.map((col, k) => {
              if (col.name && initState[col.name]) {
                initVals = {
                  ...initVals,
                  [col.name]: initState[col.name],
                };
                // console.log(
                //   "setter",
                //   initVals,
                //   fields[i][k].value,
                //   initState[col.name]
                // );
                fields[i][k].value = initState[col.name];
              }
            });
          });
        }
        if (typeof this.props.edit === "undefined") {
          fields.map((cols, i) => {
            cols.map((col, k) => {
              if (col.name && initState[col.name]) {
                initVals = {
                  ...initVals,
                  [col.name]: initState[col.name],
                };
                // console.log(
                //   "setter",
                //   initVals,
                //   fields[i][k].value,
                //   initState[col.name]
                // );
                fields[i][k].value = initState[col.name];
              }
            });
          });
        }
        if (this.props.edit == true && this.props.useAxios) {
          this.getEditData();
        }
        if (this.state.edit == true && this.props.useAxios) {
          this.getEditData();
        }

        this.setState((state) => {
          return {
            ...state,
            fields,
            edit: this.props.edit,
            editId: this.props.editId,
            initialValues: initVals,
            savedValues: { ...editVals, ...initVals },
          };
        });
        // console.log("initializer VALS", initVals, savedVals);
        // console.log("initializer STATE", this.state);
      }
      /* 

      if (fields && fields.length > 0) {
        const initialValues = this.state.initialValues;
        let initVals = {};
        let savedVals = this.state.savedVals;
        if (typeof savedVals === "undefined") {
          savedVals = {};
        }
        console.log("/request XXXX", this.props, initialValues, editVals);
        fields.map((cols, i) => {
          cols.map((col) => {
            if (initVals[col.name] !== col.value) {
              initVals = {
                ...initVals,
                [col.name]: col.value ? col.value.toString() : null,
              };
            }
          });
        });
        this.setState((state) => {
          return {
            ...state,
            initialValues: initVals,
            savedValues: { ...editVals, ...initVals },
          };
        });
        console.log("antstack", initVals, savedVals);
      }

      */
      // }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log("CDU", this.state);
    if (prevState.progress !== this.state.progress) {
      if (this.state.progress >= 100) {
        setTimeout(() => {
          this.setState((state) => {
            return {
              ...state,
              progress: 100,
              isSubmitting: false,
            };
          });
        }, 1000);
      }
    }
    if (prevState.savedValues !== this.state.savedValues) {
      this.props.updatedValues(this.state.savedValues);
    }
    if (prevState.edit !== this.props.edit && !this.props.useAxios) {
      this.getEditData();
      console.log("prsx", this.props, this.state);
    }

    if (
      prevState.savedValues !== this.state.initialValues &&
      prevState.initialValues !== this.state.initialValues &&
      this.props.edit == false
    ) {
      this.setState((state) => {
        return {
          ...state,
          savedValues: this.props.initialValues,
        };
      });
    }
    if (
      prevProps.isSubmitting !== this.props.isSubmitting &&
      prevState.isSubmitting !== this.state.isSubmitting &&
      prevProps.isSubmitting !== this.props.isSubmitting
    ) {
      console.log("isSubmitting CDU", this.props.isSubmitting);
      this.setState((state) => {
        return {
          ...state,
          isSubmitting: this.props.isSubmitting,
        };
      });
    }
  }
  getEditData() {
    let initVals = {};
    axios({
      url: this.props.editApi + "/" + this.props.editId,
      method: this.props.editType,
      headers: {
        "Content-Type": "application/json",
        ...this.props.headers,
        ...this.props.auth,
      },
      params: { ...this.props.params },
    }).then(
      (response) => {
        console.log(
          "/request " + this.props.create || this.props.edit,
          response.data
        );
        if (response.data.results == null) {
          this.setState((state) => {
            return {
              ...state,
              editId: null,
              edit: false,
              isSubmitting: false,
              savedValues: {},
            };
          });
        }
        if (response.data.results && response.data.results._id) {
          let valuesToSave = {};
          // console.log("ress", response.data.results);
          // response.data.results.map((ress) => {
          //   // valuesToSave = {...valuesToSave,}
          // });
          this.setState((state) => {
            return {
              ...state,
              editId: response.data.results._id,
              edit: true,
              savedValues: response.data.results,
            };
          });
          let myfields = this.props.fields;

          myfields.map((cols, i) => {
            cols.map((col, k) => {
              if (
                col.name &&
                response.data.results[col.name] &&
                col.type != "file"
              ) {
                initVals = {
                  ...initVals,
                  [col.name]: response.data.results[col.name],
                };
                // console.log(
                //   "setteresssssxxxxs",
                //   initVals,
                //   myfields[i][k].value,
                //   response.data.results[col.name]
                // );
                myfields[i][k].value = response.data.results[col.name];
              }
              if (
                col.name &&
                response.data.results[col.name] &&
                col.type == "file"
              ) {
                initVals = {
                  ...initVals,
                  [col.name]: {
                    ...initVals[col.name],
                    response: {
                      updateData: {
                        // initVals[col.name].response ? initVals[col.name].response.updateData : {},
                        [col.name]: response.data.results[col.name],
                      },
                    },
                  },
                };
                // console.log(
                //   "setteresszzz",
                //   initVals,
                //   myfields[i][k],
                //   response.data.results[col.name]
                // );
                myfields[i][k].props = {
                  ...myfields[i][k].props,
                  response: {
                    [col.name]: response.data.results[col.name],
                  },
                };
              }
            });
          });
          this.setState((state) => {
            return {
              ...state,
              fields: myfields,
            };
          });
        }
      },
      (error) => {
        console.log("/request ", error);
      }
    );
  }
  handleChanges = (name, value, type = "normal", behaviour = "set") => {
    console.log("formvx", name, value);
    const savedValues = this.state.savedValues;
    this.setState((state) => {
      return {
        ...state,
        savedValues: { ...savedValues, [name]: value },
      };
    });
    let propOCH = null;
    let countofFiles = 0;

    this.props.fields.map((propFields) => {
      propFields.map((pF) => {
        if (typeof pF.onChangeHandler == "function") {
          propOCH = pF.onChangeHandler;
        }
        if (pF.type == "file" && pF.name == name && value.filePaths) {
          if (value.filePaths > 0) {
            countofFiles++;
            this.setState((state) => {
              return {
                ...state,
                sentFiles: countofFiles,
              };
            });
          }
        }
      });
    });

    if (typeof propOCH == "function") {
      propOCH(name, value);
    }

    this.props.onChangeHandler(name, value);
  };
  handleSubmit = () => {
    const savedValues = this.state.savedValues;
    this.props.onSubmit(savedValues);
    if (
      (typeof this.props.createApi != "undefined" ||
        typeof this.props.editApi != "undefined") &&
      this.props.useAxios
    ) {
      /*
        create
        createType
        edit
        editType
        auth
        headers
        params
       */
      let finalApiValues = {};
      this.props.fields.map((propFields) => {
        propFields.map((pF) => {
          if (pF.type == "file") {
            if (
              typeof savedValues[pF.name] !== "undefined" &&
              typeof savedValues[pF.name] === "object"
            ) {
            }
          } else {
            finalApiValues = {
              ...finalApiValues,
              [pF.name]: savedValues[pF.name],
            };
          }
        });
      });
      console.log(
        "/request SUBMITTING",
        this.state.savedValues,
        finalApiValues
      );
      console.log("SOMETHING", this.props, {
        url: this.state.edit
          ? this.props.updateApi + "/" + this.state.editId
          : this.props.createApi,
        method: this.state.edit ? this.props.updateType : this.props.createType,
        headers: {
          "Content-Type": "application/json",
          ...this.props.headers,
          ...this.props.auth,
        },
        params: { ...this.props.params },
        data: finalApiValues,
      });
      axios({
        url: this.state.edit
          ? this.props.updateApi + "/" + this.state.editId
          : this.props.createApi,
        method: this.state.edit ? this.props.updateType : this.props.createType,
        headers: {
          "Content-Type": "application/json",
          ...this.props.headers,
          ...this.props.auth,
        },
        params: { ...this.props.params },
        data: finalApiValues,
      }).then(
        (response) => {
          console.log(
            "/request " + this.props.create || this.props.edit,
            response.data
          );
          if (response.data.result && response.data.result.n == 1) {
            this.setState((state) => {
              return {
                ...state,
                isSubmitting: true,
              };
            });
            let countofFiles = 0;
            const incrementer = 100 / (countofFiles + 5);
            setTimeout(() => {
              this.setState((state) => {
                return {
                  ...state,
                  progress: 50,
                  sentFiles: countofFiles,
                };
              });
            }, 200);
            setInterval(() => {
              let incr = +this.state.progress + +incrementer;

              this.setState((state) => {
                return {
                  ...state,
                  progress: incr,
                };
              });
            }, 200);
          }
          if (response.data.results && response.data.results._id) {
            this.setState((state) => {
              return {
                ...state,
                editId: response.data.results._id,
                edit: true,
                savedValues: response.data.results,
              };
            });
            this.props.fields.map((propFields) => {
              propFields.map((pF) => {
                if (pF.type == "file") {
                  if (
                    typeof savedValues[pF.name] !== "undefined" &&
                    typeof savedValues[pF.name] === "object"
                  ) {
                    pF.props = {
                      ...pF.props,
                      model_id: response.data.results._id,
                    };
                  }
                } else {
                  // finalApiValues = {
                  //   ...finalApiValues,
                  //   [pF.name]: savedValues[pF.name],
                  // };
                }
                // console.log("uploadfilez Z", finalApiValues);
              });
            });
            this.setState((state) => {
              return {
                ...state,
                isSubmitting: true,
              };
            });
            let countofFiles = 0;
            this.props.fields.map((propFields) => {
              propFields.map((pF) => {
                if (pF && pF.type == "file") {
                  countofFiles++;
                  pF.model_id = response.data.results._id;
                }
              });
            });
            const incrementer = 100 / (countofFiles + 5);
            setTimeout(() => {
              this.setState((state) => {
                return {
                  ...state,
                  progress: 50,
                  sentFiles: countofFiles,
                };
              });
            }, 200);
            setInterval(() => {
              let incr = +this.state.progress + +incrementer;

              this.setState((state) => {
                return {
                  ...state,
                  progress: incr,
                };
              });
            }, 200);
          }
        },
        (error) => {
          console.log("/request ", error);
        }
      );
    } else {
      // setTimeout(() => {
      //   setSubmitting(true);
      // }, 200);
    }
  };
  handleDelete = () => {
    if (typeof this.props.deleteApi != "undefined" && this.props.useAxios) {
      const delConfirm = window.confirm("Delete the item?");
      console.log("delConfirm", delConfirm);
      if (delConfirm) {
        axios({
          url: this.props.deleteApi + "/" + this.state.editId,
          method: this.props.deleteType,
          headers: {
            "Content-Type": "application/json",
            ...this.props.headers,
            ...this.props.auth,
          },
          params: { ...this.props.params },
          data: {},
        }).then(
          (response) => {
            console.log("/request delConfirm", response.data);
            window.location.reload();
            if (response.data.results && response.data.results._id) {
              this.setState((state) => {
                return {
                  ...state,
                  edit: false,
                  editId: null,
                  savedValues: {},
                };
              });
            }
          },
          (error) => {
            console.log("/request ", error);
          }
        );
      }
    }
  };
  render() {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <FormGenerator
          // colwidths in number totaling upto 12 e.g. 8+4= 12
          cols={this.props.cols}
          spacing={this.props.spacing}
          //used for default change handler functions having switch case inside the to set proper setState
          onChangeHandler={(
            name,
            value,
            type = "normal",
            behaviour = "set"
          ) => {
            this.handleChanges(name, value, type, behaviour);
          }}
          onSubmitHandler={() => {
            this.handleSubmit();
          }}
          onDeleteHandler={() => {
            this.handleDelete();
          }}
          savedValues={this.state.savedValues}
          edit={this.state.edit}
          editId={this.state.editId}
          isSubmitting={this.state.isSubmitting}
          fields={this.state.fields}
          progress={this.state.progress}
          title={this.props.title}
        />
      </MuiPickersUtilsProvider>
    );
  }
}

export default Formake;
