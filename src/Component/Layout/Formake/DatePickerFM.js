import React, { Component } from "react";

import moment from "moment";
import { createMuiTheme } from "@material-ui/core";
import blue from "@material-ui/core/colors/blue";
import { ThemeProvider } from "@material-ui/styles";

import { DatePicker } from "@material-ui/pickers";
import { TimePicker } from "@material-ui/pickers";
import { DateTimePicker } from "@material-ui/pickers";

const datePickerTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: blue.A200,
      },
    },
    MuiPickersCalendarHeader: {
      switchHeader: {
        // backgroundColor: blue.A200,
        // color: "white",
      },
    },
    MuiPickersDay: {
      day: {
        color: "#000",
      },
      daySelected: {
        backgroundColor: blue["400"],
      },
      dayDisabled: {
        color: blue["100"],
      },
      current: {
        color: blue["900"],
      },
    },
    MuiPickersModal: {
      dialogAction: {
        color: blue["400"],
      },
    },
  },
});

class DatePickerFM extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    date: null,
  };
  componentWillMount() {
    // console.log("DatePickerFM --->>>> componentWillMount", this.state);
  }

  componentDidMount() {
    console.log(
      "DatePickerFM --->>>> componentDidMount",
      this.state,
      this.props
    );
    if (this.props.value) {
      if (this.props.value !== this.state.date) {
        this.setState((state) => {
          return {
            ...state,
            date: moment(this.props.value,this.props.inputFormat),
          };
        });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("DatePickerFM --->>>> componentDidUpdate", this.state);
    if (prevProps) {
    }
    // if (prevState !== this.state) {
    //   this.props.updatedValues(this.state.savedValues);
    // }
  }

  componentWillUnmount() {
    // console.log("DatePickerFM --->>>> componentWillUnmount", this.state);
  }
  handleChanges = (name, value, type = "normal", behaviour = "set") => {
    const savedValues = this.state.savedValues;

    console.log("DatePickerFM CDU", this.state.savedValues);

    // this.props.onChangeHandler(name, value);
  };

  render() {
    return (
      <ThemeProvider theme={datePickerTheme}>
        <DatePicker
          autoOk
          variant="inline"
          format={this.props.outputFormat ? this.props.outputFormat : "D-M-Y"}
          name={this.props.name}
          value={this.state.date}
          // value={
          //   this.props.value && dateSv == null
          //     ? moment(this.props.value, this.props.inputFormat).format("Y-M-D")
          //     : moment(dateSv).format("Y-M-D")
          // }
          required={this.props.required || false}
          title={this.props.title || ""}
          placeholder={this.props.placeholder || ""}
          className=""
          onChange={(date) => {
            this.setState((state) => {
              return {
                ...state,
                date,
              };
            });
            this.props.handleChange(this.props.name, date);
            console.log(
              "DXS DatePickerFM change",
              // date,
              moment(date).format("Y-M-D")
            );
          }}
          style={{ paddingTop: 0, width: "100%" }}
        />
      </ThemeProvider>
    );
  }
}

export default DatePickerFM;
