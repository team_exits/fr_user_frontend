import React, { useEffect, useState, useRef } from 'react'
import { IconContext } from 'react-icons'
import { GiProgression } from 'react-icons/gi'
import { MdUpdate } from 'react-icons/md'
import fs from 'fs'
import axios from 'axios'
import * as Icon from 'react-icons/all'
import VideoThumbnail from 'react-thumbnail-player'
import { Modal } from 'react-bootstrap'
import FormGenerator from './FormGenerator'

import '../Formake/Formake.css'

export default function FileUpload(props) {
  console.log('updateDataxFileUpload', props.response)

  const fileUploadField = useRef(null)
  const [selectedFile, setSelectedFile] = useState({})
  const [prevFiles, setPrevFiles] = useState({})
  const [isFilePicked, setIsFilePicked] = useState(false)
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)
  const {
    params = {},
    token = null,
    api = {
      method: 'POST',
      url: 'http://localhost:8080/upload',
      headers: {},
      getUrl: 'http://localhost:8081/uploads',
    },
    model = null,
    model_id = null,
    storage_path = null,
    model_key = null,
  } = props
  let { data = {} } = props
  useEffect(() => {
    console.log('props.isSubmitting uE')
    if (
      props.isSubmitting &&
      typeof selectedFile !== 'undefined' &&
      selectedFile.length > 0
    ) {
      console.log('props.isSubmitting IF')
      const formData = new FormData()

      Object.keys(selectedFile).map((sfKey) => {
        formData.append(props.name, selectedFile[sfKey])
      })
      if (model && model_id) {
        formData.append('model', model)
        formData.append('model_id', model_id)
        if (model_key) {
          formData.append('model_key', model_key)
        }
      }
      if (storage_path) {
        formData.append('storage_path', storage_path)
      }

      data = formData
      axios({
        method: api.method,
        url: api.url,
        headers: {
          Authorization: 'Bearer ' + token,
          Accept: '*/*',
          'Content-Type': 'multipart/form-data',
          //   "Content-Type": "application/json",
          ...api.headers,
        },
        params,
        data,
      }).then(
        (response) => {
          console.log('FileUploadProps /request axios', response.data)
          props.onChange(null, { ...selectedFile, response: response.data })
        },
        (error) => {
          console.log('axios', { ...error })
        }
      )
    }
  }, [props.isSubmitting, selectedFile])
  const fileChangeHandler = (event) => {
    let finalFiles = event.target.files
    if (typeof prevFiles === 'object') {
      if (Object.keys(prevFiles).length > 0) {
        finalFiles = {}
        Object.keys(event.target.files).map((filesKey) => {
          finalFiles = Object.assign({}, finalFiles, {
            [Date.now() + Math.floor(Math.random() * 999999)]:
              event.target.files[filesKey],
          })
        })
        Object.keys(prevFiles).map((oldFilesKey) => {
          finalFiles = Object.assign({}, finalFiles, {
            [Date.now() + Math.floor(Math.random() * 999999)]:
              prevFiles[oldFilesKey],
          })
        })

        setSelectedFile(finalFiles)
        setIsFilePicked(true)
      } else {
        setSelectedFile(finalFiles)
        setIsFilePicked(true)
      }
    } else {
      setSelectedFile(finalFiles)
      setIsFilePicked(true)
    }
    props.onChange(null, finalFiles)
  }

  const handleUpload = () => {
    fileUploadField.current.click()
  }
  const addMore = () => {
    const exf = selectedFile
    setPrevFiles(exf)
    if (Object.keys(exf).length > 0) {
      handleUpload()
    }
  }

  const resetField = () => {
    setSelectedFile({})
    setIsFilePicked(false)
  }
  const showDetails = () => {
    return isFilePicked && selectedFile && selectedFile.size > 0 ? (
      singleFileDetails(selectedFile)
    ) : isFilePicked && typeof selectedFile === 'object' ? (
      <div class='row'>
        {Object.keys(selectedFile).map((key) => {
          const sf = selectedFile[key]
          return singleFileDetails(sf, key)
        })}
      </div>
    ) : null
  }
  const singleFileDetails = (sf, key = 0) => {
    return (
      <>
        <div
          class='col-xs-4 col-sm-4 col-md-4 col-lg-4'
          style={{
            position: 'relative',
            height: 250,
          }}
        >
          <>
            <div
              style={{
                flex: 1,
                flexDirection: 'row',
                flexGrow: 1,
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                maxWidth: '100%',
                color: '#000',
                height: '100%',
                // marginTop: 20,
              }}
            >
              <span
                onClick={() => {
                  removeThisFile(key)
                }}
                style={{
                  position: 'relative',
                  marginBottom: -10,
                  marginRight: -5,
                  zIndex: 1,
                  float: 'right',
                  backgroundColor: '#F00',
                  borderRadius: '50%',
                  // padding: 5,
                  paddingLeft: 1,
                  paddingTop: 1,
                  paddingBottom: 0,
                  width: 22,
                  height: 22,
                  // boxShadow: '10 100 105 1000 #000'
                }}
              >
                <Icon.IoMdClose
                  size='large'
                  style={{
                    width: 20,
                    height: 20,
                    fontWeight: 'bold',
                    color: '#fff',
                  }}
                ></Icon.IoMdClose>
              </span>
              {sf.type.search(/image/i) > -1 ? showThumbnail(sf, 'img') : null}
              {sf.type.search(/pdf/i) > -1 ? showThumbnail(sf, 'pdf') : null}
              {sf.type.search(/doc/i) > -1 ? showThumbnail(sf, 'doc') : null}
              {sf.type.search(/xls/i) > -1 ? showThumbnail(sf, 'xls') : null}
              {sf.type.search(/(rar|tar|gz|bz|archive|zip)/i) > -1
                ? showThumbnail(sf, 'archive')
                : null}
              {sf.type.search(/sql/i) > -1 ? showThumbnail(sf, 'sql') : null}
              {sf.type.search(/json/i) > -1 ? showThumbnail(sf, 'json') : null}
              {sf.type.search(/text/i) > -1 && sf.type.search(/plain/i) > -1
                ? showThumbnail(sf, 'text')
                : null}
              {sf.type.search(
                /text|txt|pdf|doc|xls|rtf|image|jpg|jpeg|png|mp4|video|webm|json/i
              ) < 0
                ? showThumbnail(sf, 'others')
                : null}
              {sf.type.search(/video/i) > -1
                ? showThumbnail(sf, 'video')
                : null}
              <div className='caption'>
                <b
                  style={{
                    flex: 1,
                    flexWrap: 'wrap',
                    flexShrink: 1,
                    overflowWrap: 'break-word',
                  }}
                >
                  {sf.name}
                </b>
                <hr style={{ padding: 0, margin: 0 }} />
                <p>
                  {Math.ceil(sf.size / 1024)} KB |{' '}
                  <span
                    style={{
                      flex: 1,
                      flexWrap: 'wrap',
                      flexShrink: 1,
                      overflowWrap: 'break-word',
                    }}
                  >
                    {sf.type}
                  </span>
                </p>
                <p></p>
              </div>
            </div>
          </>

          {/* {sf.type.search(/(image|png)/i) <= -1
            ? showThumbnail(sf, "img")
            : showThumbnail(sf, null)} */}
        </div>
      </>
    )
  }
  const showThumbnail = (sf, type) => {
    let returnableThumb = <></>
    switch (type) {
      case 'img':
        return (returnableThumb = (
          <img
            data-src='#'
            alt=''
            src={URL.createObjectURL(sf)}
            style={{
              position: 'relative',
              width: '100%',
              maxWidth: 200,
              height: '100%',
              maxHeight: 150,
              zIndex: 0,
            }}
          />
        ))
        break
      case 'others':
        return (returnableThumb = (
          <>
            <Icon.FcQuestions
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcQuestions>
          </>
        ))
        break
      case 'text':
        return (returnableThumb = (
          <>
            <Icon.FcRules
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcRules>
          </>
        ))
        break
      case 'json':
        return (returnableThumb = (
          <>
            <Icon.FcFlowChart
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcFlowChart>
          </>
        ))
        break
      case 'sql':
        return (returnableThumb = (
          <>
            <Icon.FcAcceptDatabase
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcAcceptDatabase>
          </>
        ))
        break
      case 'pdf':
        return (returnableThumb = (
          <>
            <Icon.GrDocumentPdf
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#F00',
              }}
            ></Icon.GrDocumentPdf>
          </>
        ))
        break
      case 'doc':
        return (returnableThumb = (
          <>
            <Icon.FcDocument
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcDocument>
          </>
        ))
        break
      case 'xls':
        return (returnableThumb = (
          <>
            <Icon.FcDataSheet
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcDataSheet>
          </>
        ))
        break
      case 'archive':
        return (returnableThumb = (
          <>
            <Icon.FcFilingCabinet
              size='large'
              style={{
                width: 80,
                height: 80,
                fontWeight: 'bold',
                color: '#fff',
              }}
            ></Icon.FcFilingCabinet>
          </>
        ))
        break
      case 'video':
        return (returnableThumb = (
          <VideoThumbnail
            title='Animated Cartoon'
            message='10M views'
            preview={URL.createObjectURL(sf)}
            width={'100%'}
            height={'100%'}
            maxHeight={200}
            muted={true}
            badge=''
            badgeBg='none'
            // classname="customClassName"
          />
        ))
        break

      default:
        return (returnableThumb = (
          <>
            <span className='' style={{ color: '#000', width: '100%' }}>
              {sf.name}
            </span>
            <ul
              style={{
                listStyle: 'none',
                marginLeft: 10,
                textAlign: 'left',
              }}
            >
              <li style={{ padding: 5, paddingBottom: 0 }}>Name:</li>
              <li style={{ padding: 5, paddingBottom: 0 }}>
                Size: {Math.ceil(sf.size / 1024)}
              </li>
              <li style={{ padding: 5, paddingBottom: 0 }}>Type: {sf.type}</li>
              {/* <li>{sf.lastModifiedDate}</li> */}
            </ul>
          </>
        ))
        break
        return returnableThumb
    }
  }
  const removeThisFile = (key) => {
    const existingFiles = selectedFile
    let tempFiles = []
    Object.keys(existingFiles).map((efk) => {
      if (efk !== key) {
        tempFiles.push(existingFiles[efk])
      }
    })
    setSelectedFile(tempFiles)
  }
  return (
    <>
      <input
        type='file'
        className='form-control'
        name='upload_file'
        onChange={fileChangeHandler}
        style={{ display: 'none' }}
        ref={fileUploadField}
        multiple={true}
      />
      {props.response && props.preview
        ? Object.keys(props.response).map((prs) => {
            const file = props.response[prs]
            return (
              <>
                <br />
                <img
                  src={api.getUrl + '/' + file}
                  alt=''
                  style={{ width: 100, height: 100 }}
                />
              </>
            )
          })
        : null}
      <br />

      {Object.keys(selectedFile).length <= 0 ? (
        <button
          type='button'
          className='btn btn-primary'
          onClick={handleUpload}
          style={{ ...props.style }}
        >
          Upload A File
        </button>
      ) : null}

      {selectedFile && Object.keys(selectedFile).length > 0 ? (
        <div style={{ ...props.style }}>
          <button
            type='button'
            className='btn btn-warning'
            onClick={addMore}
            style={{ marginRight: 7 }}
          >
            Add More
          </button>

          <button
            type='button'
            className='btn btn-danger'
            onClick={resetField}
            style={{ marginRight: 7 }}
          >
            Remove Files
          </button>

          <a
            className='btn btn-primary'
            data-toggle='modal'
            href='#fileUploadModal'
            onClick={handleShow}
            style={{ marginRight: 7 }}
          >
            Show Files
          </a>
        </div>
      ) : null}
      <div>
        <Modal
          show={show}
          style={{ position: 'absolute', marginTop: '30%', height: '100%' }}
          onHide={handleClose}
        >
          <Modal.Header>
            <Modal.Title style={{ paddingTop: '30%' }}>
              Your Files{' '}
              <span
                className='pull-right'
                onClick={handleClose}
                style={{
                  fontSize: 20,
                  marginTop: -10,
                  cursor: 'pointer',
                  backgroundColor: '#F00',
                  borderRadius: '50%',
                  paddingLeft: 7,
                  paddingRight: 7,
                  paddingTop: 5,
                }}
              >
                <Icon.IoMdClose
                  size='large'
                  style={{
                    width: 20,
                    height: 20,
                    fontWeight: 'bold',
                    color: '#fff',
                  }}
                ></Icon.IoMdClose>
              </span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div
              class='well well-lg'
              style={{ height: 400, overflow: 'scroll' }}
            >
              {showDetails()}
            </div>
          </Modal.Body>
          <Modal.Footer>
            {Object.keys(selectedFile).length <= 0 ? (
              <button
                type='button'
                className='btn btn-primary'
                onClick={handleUpload}
                style={{ marginRight: 7 }}
              >
                Upload A File
              </button>
            ) : null}

            {selectedFile && Object.keys(selectedFile).length > 0 ? (
              <>
                <button
                  type='button'
                  className='btn btn-danger'
                  onClick={resetField}
                >
                  Remove Files
                </button>
                <button
                  type='button'
                  className='btn btn-warning'
                  onClick={addMore}
                  style={{ marginRight: 7 }}
                >
                  Add More
                </button>
              </>
            ) : null}
            <span
              className='btn btn-primary'
              variant='secondary'
              onClick={handleClose}
            >
              Close
            </span>
            {/* <span
            className="btn btn-primary"
            variant="primary"
            onClick={handleClose}
          >
            Save Changes
          </span> */}
          </Modal.Footer>
        </Modal>
      </div>
    </>
  )
}
