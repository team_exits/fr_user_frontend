import React, { Component } from "react";

import AsyncSelect from "react-select/async";
import axios from "axios";
import Select from "react-select";

const colourOptions = [
  { value: "ocean", label: "Ocean", color: "#00B8D9", isFixed: true },
  { value: "blue", label: "Blue", color: "#0052CC", isDisabled: true },
  { value: "purple", label: "Purple", color: "#5243AA" },
  { value: "red", label: "Red", color: "#FF5630", isFixed: true },
  { value: "orange", label: "Orange", color: "#FF8B00" },
  { value: "yellow", label: "Yellow", color: "#FFC400" },
  { value: "green", label: "Green", color: "#36B37E" },
  { value: "forest", label: "Forest", color: "#00875A" },
  { value: "slate", label: "Slate", color: "#253858" },
  { value: "silver", label: "Silver", color: "#666666" },
];

export default class WithPromises extends Component {
  state = {
    api: {
      method: "POST",
      url: "http://localhost:8080/upload",
      headers: {},
      getUrl: "http://localhost:8081/uploads",
    },
    options: [{ value: null, label: "Select An Option", color: "#d7d7d7" }],
    selectedOption: null,
    fetchData: false,
    loading: true,
  };
  componentDidMount() {
    const api = this.props.api;
    const options = this.props.options;
    console.log("options", options, typeof options);
    if (api && typeof options == "undefined") {
      this.setState((state) => {
        return {
          ...state,
          api,
          fetchData: true,
        };
      });
    }
    if (typeof options === "object") {
      this.setState((state) => {
        return {
          ...state,
          options: [
            { value: null, label: "Select An Option", color: "#d7d7d7" },
            ...options,
          ],
          fetchData: false,
        };
      });
      if (this.props.value && this.state.selectedOption == null) {
        const optns = this.state.options;
        let selectedOption = optns.filter((optn) => {
          return optn.value == this.props.value;
        });
        if (typeof selectedOption === "object") {
          selectedOption = selectedOption[0];
        } else {
          selectedOption = null;
        }
      console.log('select2STATE selectedOption CDM',selectedOption);

        this.setState((state) => {
          return {
            ...state,
            selectedOption,
          };
        });
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("select2", prevState, this.state, prevProps, this.props);
    if (
      typeof this.props.value !== "undefined" &&
      this.props.value &&
      this.state.selectedOption == null &&
      prevState.options !== this.state.options
    ) {
      const optns = this.state.options;
      let selectedOption = optns.filter((optn) => {
        return optn.value == this.props.value;
      });
      if (typeof selectedOption === "object") {
        selectedOption = selectedOption[0];
      }
      console.log('select2STATE selectedOption',selectedOption);
      this.setState((state) => {
        return {
          ...state,
          selectedOption,
        };
      });
    //   setTimeout(() => {
    //     this.setState((state) => {
    //       return {
    //         ...state,
    //         selectedOption,
    //       };
    //     });
    //   }, 500);
    }
    if (prevState.fetchData !== this.state.fetchData && this.state.fetchData) {
      if (this.state.fetchData) {
        axios({
          method: this.state.api.method,
          url: this.state.api.url,
          headers: {
            Authorization: "Bearer " + this.props.token,
            Accept: "*/*",
            "Content-Type": "multipart/form-data",
            //   "Content-Type": "application/json",
            ...this.state.api.headers,
          },
          params: { ...this.props.params },
          data: { ...this.props.reqData },
        }).then(
          (response) => {
            const picker = this.props.pick;
            console.log(
              "FileUploadProps /request axios",
              response.data,
              this.props
            );
            console.log("response.data.results", typeof response.data.results);
            if (
              response.data.results &&
              typeof response.data.results === "object"
            ) {
              if (response.data.results.length > 0) {
                let finalData = [];
                response.data.results.map((res) => {
                  let label = Object.values(picker)[0];
                  console.log("HELLO",label)
                  if (typeof label === "object") {
                    let tmpLabel = "";
                    label.map((lbl) => {
                      console.log("msx", res[lbl]);
                      if (res[lbl]) {
                        tmpLabel = tmpLabel + res[lbl];
                      } else {
                        tmpLabel = tmpLabel + lbl;
                      }
                    });
                    label = tmpLabel;
                  } else if (typeof label === "function"){
console.log("HELLO",label)

                    label = label(res)
console.log("HELLO",label)
;                  }else {
                    label = res[label];
                  }
                  console.log(
                    "msx LABEL",
                    typeof Object.values(picker)[0],
                    label
                  );
                  finalData.push({
                    value: res[Object.keys(picker)[0]],
                    label: label,
                  });
                });
                this.setState((state) => {
                  return {
                    ...state,
                    options: [
                      {
                        value: null,
                        label: "Select An Option",
                        color: "#d7d7d7",
                      },
                      ...finalData,
                    ],
                  };
                });
              }
            }
            // this.props.onChange(null, { ...selectedFile, response: response.data });
          },
          (error) => {
            console.log("axios", { ...error });
          }
        );
      }
    }
  }
  filterOptionsFromState = (inputValue) =>
    this.state.options.filter((i) => {
      i.label.toLowerCase().includes(inputValue.toLowerCase());
    });
  promiseOptions = (inputValue) =>
    new Promise((resolve) => {
      if (this.state.fetchData) {
      } else {
        setTimeout(() => {
          console.log("select2 OPTS", this.filterOptionsFromState(inputValue));
          resolve(this.filterOptionsFromState(inputValue));
        }, 1000);
      }
    });

  //   promiseOptions = () => {

  //   };
  render() {
    const props = this.props;
    console.log("select2STATE", this.state, this.props);
    return (
      <Select
        className="basic-single"
        classNamePrefix="select"
        defaultValue={this.state.selectedOption}
        value={this.state.selectedOption}
        isDisabled={this.props.isDisabled}
        isLoading={this.state.isLoading}
        isClearable={this.props.isClearable}
        isRtl={this.props.isRtl}
        isSearchable={true}
        name="color"
        options={this.state.options}
        onChange={(selectedOption) => {
          console.log("msx1", props.name, selectedOption);

          console.log(props.name, selectedOption.value, this.state.options);
          props.onChange(props.name, selectedOption.value);
          this.setState((state) => {
            return {
              ...state,
              selectedOption,
            };
          });
        }}
      />
    );
  }
}
