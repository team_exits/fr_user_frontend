import React from 'react'
import './Layout.css'
import Footer from './Footer/Footer'
import Header from './Header/Header'
import Sidebar from './Sidebar/Sidebar'

const Layout = ({ children }) => {
  return (
    <>
      <div className='layout'>
        <Header />
        <div className='main-screen'>
          <div>
            <div className='container-fluid'>
              <div
                className='row'
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  backgroundColor: '#f9f9f9',
                }}
              >
                <div className=' layout-sidebar' style={{ width: '20%' }}>
                  <Sidebar />
                </div>
                <div
                  className=' layout-content'
                  style={{
                    width: '80%',
                    overflowY: 'scroll',
                    // overflow: 'hidden',
                  }}
                >
                  {children}
                </div>
              </div>
              <Footer />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Layout
