import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { CgMenuRight } from "react-icons/cg";
import { RiArrowDropUpLine, RiArrowDropDownLine } from "react-icons/ri";
import Header from "../Layout/Header/Header";
import { Link } from "@material-ui/core";

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
});

export default function SwipeableTemporaryDrawer() {
  const listData = [
    { id: 1, title: "Dashboard", page: "/" },
    { id: 2, title: "Case", page: "/case"  },
    { id: 3, title: "Document", page: "/document" },
    { id: 4, title: "Order", page: "/order" },
    // { id: 5, title: "Hearing", page: "/"  },
    { id: 6, title: "Todo's", page: "/todo" },
    // { id: 7, title: "View Case History", page: "/"  },
    { id: 8, title: "Ecourts", page: "/ecourts" },
  ];

  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <Header />
      </List>
      <List>
        <div style={{ margin: "0rem 1rem 1rem 1rem" }}>
          {listData.map((d, i) => {
            // console.log(i, d);
            return (
              <Link href={d.page}>
                <li style={{ listStyle: "none" }}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      padding: "0.7rem 0rem",
                      color: "#2b2d49",
                    }}
                  >
                    <p>{d.title}</p>
                    <RiArrowDropDownLine />
                  </div>
                </li>
              </Link>
            );
          })}
        </div>
      </List>
    </div>
  );

  return (
    <div>
      {["right"].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}>
            <h1
              style={{
                color: "#08b2f0",
              }}
            >
              <CgMenuRight />
            </h1>
          </Button>

          {/* <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button> */}
          <SwipeableDrawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            onOpen={toggleDrawer(anchor, true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
        </React.Fragment>
      ))}
    </div>
  );
}
