import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from 'react-router-dom'
import { Ecourts } from './Views/Ecourts/Ecourts'
import Case from './Views/Case/Case'
import Cases from './Views/Case/Cases'
import Orders from './Views/Order/Orders'
import Order from './Views/Order/Order'
import Dashboard from './Views/Dashboard/Dashboard'
import Todo from './Views/Todo/Todo'
import Todos from './Views/Todo/Todos'
import Documents from './Views/Document/Documents'
import Document from './Views/Document/Document'
import CaseProcessing from './Views/CaseProcessing/CaseProcessing'
import CaseProcessings from './Views/CaseProcessing/CaseProcessings'
import Tables from './Views/Tables/Tables'
import Hearings from './Views/Hearing/Hearings'
import Hearing from './Views/Hearing/Hearing'
import Login from './Views/Login/Login'
import Register from './Views/Register/Register'
import Calendar from './Views/Calendar/Calendar'
import Demo from './Views/Demo/Demo'
import HcCaseDetails from './Views/Tables/HcCaseDetails/HcCaseDetails'
import { useSelector } from 'react-redux'
import { createBrowserHistory } from 'history'

function App() {
  const login = useSelector((state) => state.login)
  const { isLogged } = login

  return (
    <div className='App'>
      <Router
      // history={createBrowserHistory()}
      // onUpdate={() => window.scrollTo(0, 0)}
      basename={window.location.pathname || ''}
      >
        {/* <Router basename={window.location.pathname || ''}> */}
        <Switch>
          {!isLogged ? (
            <>
              <Redirect to='/' />
              <Route exact path='/' component={Login} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/register' component={Register} />
            </>
          ) : (
            <>
              <Route exact path='/' component={Dashboard} />
              <Route exact path='/calendar' component={Calendar} />
              <Route exact path='/ecourts' component={Ecourts} />
              <Route exact path='/viewCaseDetails/:qid' component={Tables} />
              <Route exact path='/demo' component={Demo} />
              <Route exact path='/hccasedetails' component={HcCaseDetails} />

              {/* Case  */}
              <Route exact path='/case' component={Case} />
              <Route exact path='/cases' component={Cases} />
              <Route exact path='/case/:qid' component={Case} />

              <Route exact path='/document' component={Document} />
              <Route exact path='/documents' component={Documents} />
              <Route exact path='/document/:qid' component={Document} />

              <Route exact path='/order' component={Order} />
              <Route exact path='/orders' component={Orders} />
              <Route exact path='/order/:qid' component={Order} />

              <Route exact path='/hearing' component={Hearing} />
              <Route exact path='/hearings' component={Hearings} />
              <Route exact path='/hearing/:qid' component={Hearing} />

              <Route exact path='/todo' component={Todo} />
              <Route exact path='/todos' component={Todos} />
              <Route exact path='/todo/:qid' component={Todo} />

              {/* <Route exact path='/case-processing' component={CaseProcessing} /> */}
              <Route
                exact
                path='/case-processings'
                component={CaseProcessings}
              />
              <Route
                exact
                path='/case-processing/:qid?'
                component={CaseProcessing}
              />
            </>
          )}
        </Switch>
      </Router>
    </div>
  )
}

export default App
